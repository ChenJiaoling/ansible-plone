# -*- coding: utf-8 -*-
from interaktiv.basetiles.base_functions import tile_installer


def setup_after(context):
    """Miscellanous steps import handle
    """

    tile_installer({
        'id': 'interaktiv_tile_news',
        'label': 'News',
        'portaltype': 'NewsTileCT',
        'active': True,
        'image': '++theme++interaktiv.tile_news/tile-icon.jpg'
    })
