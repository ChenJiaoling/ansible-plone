var initializeSelectFolder = function (modal) {

    var base_url = $('body').data('base-url');
    var $chooser = $('.content-tree-items .chooser.selected');
    var folder_uid = $chooser.data('uid') || '';
    var newstile_uid = modal.find('.interaktivbasetiles-modal-header').data('uid');

    $('#spinner').show();
    // Get data with api view and return results in tempalte with results view
    return $.ajax({
      url: base_url + "/@@update_folder_path_api",
      type: "POST",
      dataType: "json",
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      data: {
        'folder_uid': folder_uid,
        'newstile_uid': newstile_uid
      },
      success: function () {
        var base_url = $('body').data('base-url');
        console.log("SUCCESS");
        window.location = base_url;
        modal.close();
      },
      error: function (error) {
        console.log(error);
      },
      complete: function () {
        $('#spinner').hide();
      },
    });

    modal.close();
};