# coding=utf-8
from interaktiv.basetiles.api.api import ApiBaseView
from interaktiv.basetiles.api.api import json_api_call
from interaktiv.basetiles.api.interfaces import IAPIMethodWeakCaching
from zope.interface import implements
from zope.lifecycleevent import ObjectModifiedEvent
from zope.event import notify
from plone.app.uuid.utils import uuidToObject
from zope.site.hooks import getSite
from plone import api
from interaktiv.framework.reference import IFReference
import transaction


class UpdateFolderPath(ApiBaseView):
    implements(IAPIMethodWeakCaching)

    @json_api_call
    def __call__(self, *kw, **kwargs):
        form = self.request.form
        if 'folder_uid' in form:
            folder_uid = form['folder_uid']
            if 'newstile_uid' in form:
                newstile_uid = form['newstile_uid']
                obj = uuidToObject(newstile_uid)
                IFReference.set_relation(obj, 'folder_path', folder_uid)

                obj.reindexObject()
                notify(ObjectModifiedEvent(obj))
                return True
        return False