# -*- coding: UTF-8 -*-
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from interaktiv.basetiles.views.basetile import BaseTileView
from zope.site.hooks import getSite
from Products.CMFCore.utils import getToolByName


class NewsTileCTView(BaseTileView):
    """ View: Extend Base Tile View with Tile """

    template = ViewPageTemplateFile("templates/view.pt")
    template_edit_mode = ViewPageTemplateFile(
        "templates/view_edit_mode.pt"
    )

    def get_options(self):
        parent_class = super(NewsTileCTView, self)
        options = parent_class.get_options()
        options['class'] += ['tile-news']

        if hasattr(self.context, 'is_valid'):
            if not self.context.is_valid:
                options['class'] += ['invalid-tile']

        return options

    def get_news(self):
        news_list = []
        portal = getSite()
        catalog = getToolByName(portal, 'portal_catalog')
        folder_path = getattr(self.context, 'folder_path', None)
        if folder_path:
            folder_path = folder_path.to_path
            query = {
                'path': {"query": folder_path},
                'portal_type': 'TilesContainerCT',
                'sort_on': 'created',
                'sort_order': 'descending'
            }

            for brain in catalog(query):
                news = brain.getObject()

                if getattr(news, 'is_workingcopy', False):
                    continue

                news_list.append({
                    'title': news.title,
                    'description': news.description,
                    'image': self.get_image(news),
                    'news_url': news.absolute_url()
                })
            news_number = len(news_list)
            if news_number >= 3:
                return news_list[0:3]
            elif 0 < news_number < 3:
                return news_list[0:news_number]
        return None

    @staticmethod
    def get_image(obj):
        image_object = getattr(obj, 'header_image', None)
        if image_object:
            return obj.absolute_url() + '/@@images/header_image'
        return None
