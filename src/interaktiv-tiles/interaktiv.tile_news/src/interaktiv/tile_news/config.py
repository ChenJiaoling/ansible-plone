# -*- coding: UTF-8 -*-

TILE_CONFIG = {
    'portal_type': 'NewsTileCT',
    'title': 'News Tile',
    'actions': [
        'remove',
        'select_folder'
    ]
}
