# -*- coding: UTF-8 -*-
# noinspection PyProtectedMember
from interaktiv.tile_text import _
from interaktiv.basetiles.contenttypes.basetile import BaseTile
from zope import schema
from zope.interface import Interface
from zope.interface import implements
from interaktiv.tile_text.config import TILE_CONFIG
from z3c.relationfield.schema import RelationChoice
from plone.app.vocabularies.catalog import CatalogSource


class INewsTileCT(Interface):
    """ Interface for NewsTileCT """

    title = schema.TextLine(
        title=_(
            u'label_title',
            default=u'Titel'
        ),
        description=u"",
        required=True
    )
    folder_path = RelationChoice(
        title=_(
            u'label_news_tile_folder_path',
            default=u'Ordner Link für News'
        ),
        source=CatalogSource(
            portal_type=[
                'Folder'
            ]
        ),
        required=False
    )


class NewsTileCT(BaseTile):
    """ Add Interface to NewsTileCT Container """
    implements(INewsTileCT)

    # noinspection PyShadowingBuiltins
    def __init__(self, id=None, **kwargs):
        self.tile_config = self.merge_tile_config(TILE_CONFIG)
        self.is_valid = True
        BaseTile.__init__(self, id, **kwargs)

    def get_available_tile_actions(self):
        actions = super(NewsTileCT, self).get_available_tile_actions()
        newstile_uid = self.UID()
        selected_folder_uid = str()
        selected_folder_path = self.folder_path
        if selected_folder_path:
            selected_folder_uid = selected_folder_path.to_object.UID()
        actions = self.append_tile_action(actions=actions, action={
            'action_id': 'select_content',
            'url': "/select_content",
            'data_modal': "/select_content_modal",
            'data_modal_size': 'xs',
            'data_modal_params': '{"tagClass": "notice", "uid": "'
                                 + newstile_uid + '"}',
            'data_modal_request': '{"select-portaltype": "Folder", '
                                  '"item_uid":"' + selected_folder_uid + '"}',
            'data_modal_init': 'initializeSelectContent',
            'attr_title': 'label_select_content',
            'attr_class': 'tool edit interaktiv-icon-basetilestheme-option-row',
            'portal_type': self.portal_type
        })

        return actions

    def validate_tile(self):
        if not self.folder_path:
            self.is_valid = False
            return False
        if not self.folder_path.to_object:
            self.is_valid = False
            return False
        self.is_valid = True
        return True
