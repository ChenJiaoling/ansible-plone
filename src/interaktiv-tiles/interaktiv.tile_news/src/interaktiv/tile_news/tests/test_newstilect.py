# -*- coding: UTF-8 -*-
from interaktiv.tile_news.contenttype import INewsTileCT
from interaktiv.tile_news.testing import \
    INTERAKTIV_TILE_NEWS_INTEGRATION_TESTING

import transaction
import unittest2 as unittest
from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility


class NewsTileCTTest(unittest.TestCase):

    layer = INTERAKTIV_TILE_NEWS_INTEGRATION_TESTING
    portal = None
    request = None

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        self.request['ACTUAL_URL'] = self.portal.absolute_url()
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.portal.invokeFactory(
            "TilesContainerCT",
            id="tilescontainerct",
            title="TilesContainerCT",
        )
        self.portal.tilescontainerct.invokeFactory(
            "TileRowCT",
            id="tilerowct",
            title="TileRowCT",
        )
        transaction.commit()

    def tearDown(self):
        if 'tilescontainerct' in self.portal.objectIds():
            self.portal.manage_delObjects(['tilescontainerct'])
        transaction.commit()

    def test_schema(self):
        fti = queryUtility(
            IDexterityFTI,
            name='NewsTileCT'
        )
        schema = fti.lookupSchema()
        self.assertEquals(INewsTileCT, schema)

    def test_fti(self):
        fti = queryUtility(
            IDexterityFTI,
            name='NewsTileCT'
        )
        self.assertNotEquals(None, fti)

    def test_factory(self):
        fti = queryUtility(
            IDexterityFTI,
            name='NewsTileCT'
        )
        factory = fti.factory
        new_object = createObject(factory)
        self.assertEqual(
            new_object.__module__,
            'interaktiv.tile_news.contenttype'
        )

    def test_news_ct_provides_interface(self):
        fti = queryUtility(
            IDexterityFTI,
            name='NewsTileCT'
        )
        factory = fti.factory
        new_object = createObject(factory)

        self.failUnless(INewsTileCT.providedBy(new_object))

    def test_adding(self):
        self.portal.tilescontainerct.tilerowct.invokeFactory(
            type_name='NewsTileCT',
            id='newstilect'
        )
        self.assertTrue(self.portal.tilescontainerct.tilerowct.newstilect)
