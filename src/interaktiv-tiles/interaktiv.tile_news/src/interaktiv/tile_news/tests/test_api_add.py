from plone import api
from plone.app.testing import TEST_USER_ID, setRoles
from plone.dexterity.utils import createContentInContainer

import json
import transaction

import unittest2 as unittest

from interaktiv.tile_news.testing import \
    INTERAKTIV_TILE_NEWS_FUNCTIONAL_TESTING


class TestApiAdd(unittest.TestCase):

    layer = INTERAKTIV_TILE_NEWS_FUNCTIONAL_TESTING
    news_folder = None
    app = None
    portal = None
    request = None

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        # create Folder that holds news item
        self.news_folder = createContentInContainer(
            self.portal,
            'Folder',
            id='folder_a',
        )
        # create tilescontainerct
        createContentInContainer(
            self.portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )
        # create row
        createContentInContainer(
            self.portal.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )

    def tearDown(self):
        if 'tiles_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['tiles_container_a'])
        transaction.commit()

    def test_add_news_tile(self):
        self.request.form['portal_type'] = 'NewsTileCT'
        self.request.form['fields'] = json.dumps({
            'folder_path': self.news_folder.UID()
        })
        view = api.content.get_view(
            name='add_tile',
            context=self.portal.tiles_container_a.tile_row_a,
            request=self.request,
        )
        result = json.loads(view())
        self.assertEqual(len(self.portal.tiles_container_a.objectIds()), 1)
        tile_row = self.portal.tiles_container_a.objectValues()[0]
        self.assertEqual(result['url'], tile_row.absolute_url())
        tile = tile_row.objectValues()[0]
        self.assertEqual(tile.portal_type, 'NewsTileCT')
        self.assertEqual(tile.folder_path.to_object, self.news_folder)


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestApiAdd))
    return suite
