# -*- coding: UTF-8 -*-
# noinspection PyProtectedMember
from interaktiv.basetiles import _
from interaktiv.basetiles.contenttypes.basetile import BaseTile
from zope import schema
from zope.interface import Interface
from zope.interface import implements
from interaktiv.tile_subheadline.config import TILE_CONFIG
from interaktiv.tile_subheadline.vocabularies.heading_tags import HEADING_TAGS
from interaktiv.tile_subheadline.vocabularies.heading_positions import \
    HEADING_POSITIONS


class ISubheadlineCT(Interface):
    """ Interface for SubheadlineCT """

    title = schema.TextLine(
        title=_(
            u'label_title',
            default=u'Titel'
        ),
        description=u"",
        required=True
    )
    subheadline = schema.TextLine(
        title=_(
            u'label_subheadline',
            default=u'Zwischenüberschrift'
        ),
        default=u'Zwischenüberschrift',
        description=u"",
        required=False
    )

    heading_tag = schema.Choice(
        title=_(
            u'label_heading_tag',
            default=u'Überschrift HTML Tag'
        ),
        description=u"Überschrift HTML Tag",
        source="interaktiv.tile_subheadline.vocabularies.heading_tags",
        default="h2",
        required=False,
    )

    heading_position = schema.Choice(
        title=_(
            u'label_heading_position',
            default=u'Überschrift Position'
        ),
        description=u"Überschrift Position",
        source="interaktiv.tile_subheadline.vocabularies.heading_positions",
        default="center",
        required=False,
    )


class SubheadlineCT(BaseTile):
    """ Add Interface to SubheadlineCT Container """
    implements(ISubheadlineCT)

    # noinspection PyShadowingBuiltins
    def __init__(self, id=None, **kwargs):
        self.tile_config = self.merge_tile_config(TILE_CONFIG)
        self.is_valid = True
        BaseTile.__init__(self, id, **kwargs)

    def get_searchable_text(self):
        return getattr(self, 'subheadline', str())

    def get_available_tile_actions(self):
        actions = super(SubheadlineCT, self).get_available_tile_actions()
        actions = self.append_tile_action(actions=actions, action={
            'action_id': 'select_heading_tag',
            'url': "/select_heading_tag",
            'data_modal': "/select_heading_tag_modal",
            'data_modal_size': 'lg',
            'data_modal_params': '{"tagClass": "notice"}',
            'data_modal_init': 'initializeSelectHeadingTag',
            'attr_title': 'label_select_heading_tag',
            'attr_class': 'tool edit interaktiv-icon-basetilestheme-option-row',
            'portal_type': self.portal_type
        })

        return actions

    @staticmethod
    def get_heading_tags():
        return HEADING_TAGS

    @staticmethod
    def get_heading_positions():
        return HEADING_POSITIONS

    def validate_tile(self):
        if not self.subheadline or self.subheadline == u'Zwischenüberschrift':
            self.is_valid = False
            return False

        self.is_valid = True
        return True
