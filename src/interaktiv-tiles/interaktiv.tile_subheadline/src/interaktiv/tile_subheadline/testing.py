from plone.app.testing import PloneSandboxLayer
from plone.app.testing import applyProfile
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting
import importlib
from plone.testing import z2

from zope.configuration import xmlconfig


class InteraktivTileSubheadlineLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)
    products_to_import = [
        'plone.app.contenttypes',
        'interaktiv.basetiles',
        'interaktiv.tile_subheadline'
    ]

    def setUpZope(self, app, configurationContext):
        for product_name in self.products_to_import:
            module = importlib.import_module(product_name)
            xmlconfig.file(
                'configure.zcml',
                module,
                context=configurationContext
            )

    def setUpPloneSite(self, portal):
        for product_name in self.products_to_import:
            applyProfile(portal, product_name + ':default')


INTERAKTIV_TILE_SUBHEADLINE_FIXTURE = InteraktivTileSubheadlineLayer()
INTERAKTIV_TILE_SUBHEADLINE_INTEGRATION_TESTING = IntegrationTesting(
    bases=(INTERAKTIV_TILE_SUBHEADLINE_FIXTURE,),
    name="InteraktivTileSubheadlineLayer:Integration"
)
INTERAKTIV_TILE_SUBHEADLINE_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(INTERAKTIV_TILE_SUBHEADLINE_FIXTURE, z2.ZSERVER_FIXTURE),
    name="InteraktivTileSubheadlineLayer:Functional"
)
