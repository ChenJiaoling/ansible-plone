from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from interaktiv.basetiles.views.basetile import BaseTileView


class SubheadlineCTView(BaseTileView):
    """ View: Extend Base Tile View with Tile """

    template = ViewPageTemplateFile("templates/view.pt")
    template_edit_mode = ViewPageTemplateFile(
        "templates/view_edit_mode.pt"
    )

    def get_options(self):
        parent_class = super(SubheadlineCTView, self)
        options = parent_class.get_options()
        options['class'] += ['tile-subheadline']

        if hasattr(self.context, 'is_valid'):
            if not self.context.is_valid:
                options['class'] += ['invalid-tile']

        return options

    def get_heading_tag(self):
        return getattr(self.context, 'heading_tag', '')

    def get_heading_position(self):
        return getattr(self.context, 'heading_position', '')

    def get_subheadline(self):
        return getattr(self.context, 'subheadline', '')
