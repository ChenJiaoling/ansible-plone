# -*- coding: UTF-8 -*-
from interaktiv.tile_subheadline.contenttype import ISubheadlineCT
from interaktiv.tile_subheadline.testing import \
    INTERAKTIV_TILE_SUBHEADLINE_INTEGRATION_TESTING

import transaction
import unittest2 as unittest
from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility


class SubheadlineCTTest(unittest.TestCase):

    layer = INTERAKTIV_TILE_SUBHEADLINE_INTEGRATION_TESTING
    portal = None
    request = None

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        self.request['ACTUAL_URL'] = self.portal.absolute_url()
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.portal.invokeFactory(
            "TilesContainerCT",
            id="tilescontainerct",
            title="TilesContainerCT",
        )
        self.portal.tilescontainerct.invokeFactory(
            "TileRowCT",
            id="tilerowct",
            title="TileRowCT",
        )
        transaction.commit()

    def tearDown(self):
        if 'tilescontainerct' in self.portal.objectIds():
            self.portal.manage_delObjects(['tilescontainerct'])
        transaction.commit()

    def test_schema(self):
        fti = queryUtility(
            IDexterityFTI,
            name='SubheadlineCT'
        )
        schema = fti.lookupSchema()
        self.assertEquals(ISubheadlineCT, schema)

    def test_fti(self):
        fti = queryUtility(
            IDexterityFTI,
            name='SubheadlineCT'
        )
        self.assertNotEquals(None, fti)

    def test_factory(self):
        fti = queryUtility(
            IDexterityFTI,
            name='SubheadlineCT'
        )
        factory = fti.factory
        new_object = createObject(factory)
        self.assertEqual(
            new_object.__module__,
            'interaktiv.tile_subheadline.contenttype'
        )

    def test_subheadline_ct_provides_interface(self):
        fti = queryUtility(
            IDexterityFTI,
            name='SubheadlineCT'
        )
        factory = fti.factory
        new_object = createObject(factory)

        from interaktiv.tile_subheadline.contenttype import ISubheadlineCT
        self.failUnless(ISubheadlineCT.providedBy(new_object))

    def test_adding(self):
        self.portal.tilescontainerct.tilerowct.invokeFactory(
            'SubheadlineCT', 'subheadlinect'
        )
        self.assertTrue(self.portal.tilescontainerct.tilerowct.subheadlinect)
