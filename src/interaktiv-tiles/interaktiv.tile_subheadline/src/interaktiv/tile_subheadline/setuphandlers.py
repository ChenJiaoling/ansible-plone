# -*- coding: utf-8 -*-
from plone import api
from plone.api.exc import InvalidParameterError
from interaktiv.basetiles.base_functions import tile_installer


def setup_after(context):
    """Miscellanous steps import handle
    """

    tile_installer({
        'id': 'interaktiv_tile_subheadline',
        'label': 'Zwischenüberschrift',
        'portaltype': 'SubheadlineCT',
        'active': True,
        'image': '++theme++interaktiv.tile_subheadline/tile-icon.jpg'
    })
