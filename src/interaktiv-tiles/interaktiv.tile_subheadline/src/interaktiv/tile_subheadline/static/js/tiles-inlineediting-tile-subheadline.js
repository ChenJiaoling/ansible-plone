$(document).ready(function () {
  if(!$('body').hasClass('is-workingcopy')) return;
  // Inline Editing Subheadline with api call "update_subheadline"
  $(document).on('click', '.tile-subheadline', function () {
    var IS_TINYMCE_ACTIVE = false;
    if (typeof(tinyMCE) != "undefined") {
      IS_TINYMCE_ACTIVE = true;
    }
    var component_url = $(this).data('url');
    // Get parent id of clicked event because
    // the clicked event has no id
    if (IS_TINYMCE_ACTIVE) {
      var selector = $(this).getPath() + ' .heading-tag';
      if($(selector).hasClass('mce-content-body')) return;

      tinyMCE.baseURL = "++theme++suzuki.tiles/css";
      tinymce.init({
        selector: selector,
        inline: true,
        toolbar: 'undo redo',
        menubar: false,
        plugins: "paste",
        paste_as_text: true,
        setup: function (ed) {
          ed.on('change', function () {
            $.ajax({
              url: component_url + '/update_content',
              data: {
                subheadline: ed.getContent()
              },
              type: 'POST',
              dataType: 'json',
              contentType: 'application/x-www-form-urlencoded; charset=utf-8',
              success: function () {
                console.log("Changed subheadline to: " + ed.getContent());
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("Status: " + textStatus + " | " + "Error: " + errorThrown);
              }
            });
          });
        }
      });
    }
  });
});