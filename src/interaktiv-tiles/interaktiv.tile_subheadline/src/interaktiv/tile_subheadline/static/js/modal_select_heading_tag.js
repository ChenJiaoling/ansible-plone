//noinspection JSUnusedGlobalSymbols
function initializeSelectHeadingTag(modal) {
  // CLick Ok button
  modal.$modal.on('click', '.interaktivtilesmodal-save-heading-tag', function () {
    var base_url = $('.interaktivbasetiles-modal-header').data('base_url');

    modal.submitForm(base_url + '/update_content', 'json', function (success, response) {
      if (success) {
        refreshTiles(response.uid, response.render);
        modal.close();
      }
    });
  });
}

