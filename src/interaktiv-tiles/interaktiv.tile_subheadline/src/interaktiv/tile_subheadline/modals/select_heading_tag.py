from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.protect.interfaces import IDisableCSRFProtection
from zope.interface import alsoProvides


class SelectHeadingTagModalView(BrowserView):
    """ Select Content From Content Tree """

    template = ViewPageTemplateFile('templates/select_heading_tag.pt')

    def __init__(self, context, request):
        self.context = context
        self.request = request
        alsoProvides(request, IDisableCSRFProtection)

    def __call__(self):
        return self.template()

    def get_uid(self):
        return self.context.UID()
