# -*- coding: UTF-8 -*-
from interaktiv.framework.vocabulary import IFVocabulary

HEADING_POSITIONS = [
            {'id': 'left', 'title': 'Links'},
            {'id': 'center', 'title': 'Zentrum'},
            {'id': 'right', 'title': 'Rechts'}
        ]


def heading_positions(context):
    return IFVocabulary.create_vocabulary(
        HEADING_POSITIONS
    )
