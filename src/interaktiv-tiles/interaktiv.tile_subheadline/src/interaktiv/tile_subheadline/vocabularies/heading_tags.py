# -*- coding: UTF-8 -*-
from interaktiv.framework.vocabulary import IFVocabulary

HEADING_TAGS = [
            {'id': 'h1', 'title': 'h1'},
            {'id': 'h2', 'title': 'h2'},
            {'id': 'h3', 'title': 'h3'},
            {'id': 'h4', 'title': 'h4'},
            {'id': 'div', 'title': 'div subheadline'}
        ]


def heading_tags(context):
    return IFVocabulary.create_vocabulary(
        HEADING_TAGS
    )
