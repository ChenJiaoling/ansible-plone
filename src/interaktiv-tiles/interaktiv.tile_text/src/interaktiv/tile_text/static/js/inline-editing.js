// Inline Editing Text with api call "update_text"
$(document).on('click', '.tile-text', function () {
  var IS_TINYMCE_ACTIVE = false;
  if (typeof(tinyMCE) != "undefined") {
    IS_TINYMCE_ACTIVE = true;
  }

  var component_url = $(this).data('url');
  var selector = $(this).getPath() + ' > .text-selector';
  var content = "";
  var placeholderText = "<p>Klicken Sie hier, um einen Text einzugeben.</p>";

  if (IS_TINYMCE_ACTIVE) {
    if($(selector).hasClass('mce-content-body')) return;

    tinyMCE.baseURL = "++theme++suzuki.tiles/css";
    tinyMCE.init({
      selector: selector,
      inline  : true,
      relative_urls : false,
      remove_script_host : false,
      convert_urls : true,
      plugins : [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media contextmenu paste table'
      ],
      toolbar : 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | unlink | image | table',
      setup   : function (ed) {
        ed.on('click', function () {
          if (ed.getContent() == placeholderText) {
            console.log("init | has placeholder: ", "#" + ed.targetElm.parentElement.id);
            $("#" + ed.targetElm.parentElement.id + " div.edit-text-tile").empty();
          }
        });
        ed.on('change', function (e) {
          content = ed.getContent();
          // If content is empty
          if (content == "") {
            console.log("change | empty");
            content = placeholderText;
            $("#" + e.target.id).html(placeholderText);
            $("#" + ed.targetElm.parentElement.id + " div.edit-text-tile").html(placeholderText);
          }

          //add class according to style guide
          var get_dom = tinyMCE.activeEditor.dom;
          get_dom.addClass(get_dom.select('a'), 'a-theme');
          get_dom.addClass(get_dom.select('ul'), 'ul-theme');
          get_dom.addClass(get_dom.select('ul>li'), 'ul-li-theme');
          get_dom.addClass(get_dom.select('ol'), 'ol-theme');
          get_dom.addClass(get_dom.select('ol>li'), 'ol-li-theme');
          get_dom.addClass(get_dom.select('table'), 'table-theme');
          get_dom.addClass(get_dom.select('td'), 'table-theme');
          get_dom.addClass(get_dom.select('th'), 'table-theme');

          $.ajax({
            url        : component_url + '/update_content',
            data       : {
              text: content
            },
            type       : 'POST',
            dataType   : 'json',
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            success    : function () {
              console.log("Changed text to: " + content);
            },
            error      : function (XMLHttpRequest, textStatus, errorThrown) {
              console.log("Status: " + textStatus + " | " + "Error: " + errorThrown);
            }
          });
        });

      }
    });
  }
});
