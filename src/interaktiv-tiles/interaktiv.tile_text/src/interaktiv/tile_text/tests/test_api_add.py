from plone import api
from plone.app.testing import TEST_USER_ID, setRoles
from plone.dexterity.utils import createContentInContainer
# from plone.app.textfield.value import RichTextValue

import json
import transaction

import unittest2 as unittest

from interaktiv.tile_text.testing import \
    INTERAKTIV_TILE_TEXT_FUNCTIONAL_TESTING


class TestApiAdd(unittest.TestCase):

    layer = INTERAKTIV_TILE_TEXT_FUNCTIONAL_TESTING
    app = None
    portal = None
    request = None

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        createContentInContainer(
            self.portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )

    def tearDown(self):
        if 'tiles_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['tiles_container_a'])
        transaction.commit()

    def test_add_text_tile(self):
        self.request.form['portal_type'] = 'TextCT'
        self.request.form['fields'] = json.dumps({
            'text': '<p>Lorem Ipsum</p>'
        })
        view = api.content.get_view(
            name='add_tile',
            context=self.portal.tiles_container_a,
            request=self.request,
        )
        result = json.loads(view())
        self.assertEqual(len(self.portal.tiles_container_a.objectIds()), 1)
        tile_row = self.portal.tiles_container_a.objectValues()[0]
        self.assertEqual(result['url'], tile_row.absolute_url())
        tile = tile_row.objectValues()[0]
        self.assertEqual(tile.portal_type, 'TextCT')
        self.assertEqual(tile.text.output, '<p>Lorem Ipsum</p>')


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestApiAdd))
    return suite
