import transaction
import unittest2 as unittest
from interaktiv.tile_text.testing import \
    INTERAKTIV_TILE_TEXT_FUNCTIONAL_TESTING
from plone import api
from plone.app.testing import TEST_USER_ID, setRoles
from plone.dexterity.utils import createContentInContainer
from plone.testing.z2 import Browser


class TestApiUpdate(unittest.TestCase):

    layer = INTERAKTIV_TILE_TEXT_FUNCTIONAL_TESTING
    app = None
    portal = None
    request = None
    browser = None

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']

        self.browser = Browser(self.app)
        self.browser.handleErrors = False

        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        createContentInContainer(
            self.portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )
        createContentInContainer(
            self.portal.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        # noinspection PyAttributeOutsideInit
        self.text_tile_a = createContentInContainer(
            self.portal.tiles_container_a.tile_row_a,
            'TextCT',
            id='text_tile_a',
            text='My Text'
        )
        self.text_tile_a.reindexObject()
        transaction.commit()

    def tearDown(self):
        if 'tiles_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['tiles_container_a'])
        transaction.commit()

    def test_update_text(self):
        self.request.form['text'] = '<p>My new Text</p>'
        view = api.content.get_view(
            name='update_content',
            context=self.text_tile_a,
            request=self.request,
        )
        view()
        transaction.commit()
        #
        self.assertEqual(
            self.text_tile_a.text.output,
            '<p>My new Text</p>'
        )


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestApiUpdate))
    return suite
