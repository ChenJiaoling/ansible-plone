# -*- coding: UTF-8 -*-
# noinspection PyProtectedMember
from interaktiv.tile_text import _
from Products.CMFCore.utils import getToolByName
from interaktiv.basetiles.contenttypes.basetile import BaseTile
from plone.app.textfield import RichText
from plone.app.textfield.value import RichTextValue
from zope import schema
from zope.interface import Interface
from zope.interface import implements
from interaktiv.tile_text.config import TILE_CONFIG


class ITextCT(Interface):
    """ Interface for TextCT """

    title = schema.TextLine(
        title=_(
            u'label_title',
            default=u'Titel'
        ),
        description=u"",
        required=True
    )
    text = RichText(
        title=_(
            u'label_text',
            default=u'Text'
        ),
        description=u"",
        default=RichTextValue("Beispieltext"),
        required=False
    )


class TextCT(BaseTile):
    """ Add Interface to TextCT Container """
    implements(ITextCT)

    # noinspection PyShadowingBuiltins
    def __init__(self, id=None, **kwargs):
        self.tile_config = self.merge_tile_config(TILE_CONFIG)
        self.is_valid = True
        BaseTile.__init__(self, id, **kwargs)

    def get_searchable_text(self):
        transforms = getToolByName(self, 'portal_transforms')
        text_obj = getattr(self, 'text', str())
        text = ''
        try:
            html = getattr(text_obj, 'output', str()).encode('utf-8')
            text = transforms.convert('html_to_text', html).getData()
        except Exception as e:
            print "[EXCEPTION] get_searchable_text() for Tile Text: ", e
        return text

    def validate_tile(self):
        if self.text is None:
            self.is_valid = False
            return False
        if not self.text.raw:
            self.is_valid = False
            return False
        if self.text.raw in [
            '<p>Klicken Sie hier, um einen Text einzugeben.</p>',
            'Beispieltext'
        ]:
            self.is_valid = False
            return False
        self.is_valid = True
        return True
