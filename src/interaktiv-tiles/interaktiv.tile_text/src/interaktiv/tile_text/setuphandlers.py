# -*- coding: utf-8 -*-
from interaktiv.basetiles.base_functions import tile_installer


def setup_after(context):
    """Miscellanous steps import handle
    """

    tile_installer({
        'id': 'interaktiv_tile_text',
        'label': 'Text',
        'portaltype': 'TextCT',
        'active': True,
        'image': '++theme++interaktiv.tile_text/tile-icon.jpg'
    })
