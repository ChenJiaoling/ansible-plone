# -*- coding: UTF-8 -*-
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from interaktiv.basetiles.views.basetile import BaseTileView
from plone.app.uuid.utils import uuidToObject
import json


class SliderCTView(BaseTileView):
    """ View: Extend Base Tile View with Tile """

    template = ViewPageTemplateFile("templates/view.pt")
    template_edit_mode = ViewPageTemplateFile(
        "templates/view_edit_mode.pt"
    )
    jsinit = 'setupSlickSlider'

    def get_options(self):
        parent_class = super(SliderCTView, self)
        options = parent_class.get_options()
        options['class'] += ['tile-slider']

        if hasattr(self.context, 'is_valid'):
            if not self.context.is_valid:
                options['class'] += ['invalid-tile']

        return options

    def get_slides(self):
        slides = []
        references = getattr(self.context, 'slides', [])
        settings_raw = getattr(self.context, 'slide_settings', '')
        settings = []

        if settings_raw and isinstance(settings_raw, basestring):
            settings = json.loads(settings_raw)

        for key, reference in enumerate(references):
            obj = reference.to_object
            slide_setting = {}

            if len(settings) > key:
                slide_setting = settings[key]

            link = ''
            if 'link_type' in slide_setting:
                if slide_setting['link_type'] == 'internal_link':
                    link_obj = uuidToObject(slide_setting['internal_link'])
                    if link_obj:
                        link = link_obj.absolute_url()
                if slide_setting['link_type'] == 'external_link':
                    link = slide_setting['external_link']

            title = getattr(obj, 'title', '')
            if 'title' in slide_setting:
                title = slide_setting['title']

            subtitle = getattr(obj, 'description', '')
            if 'subtitle' in slide_setting:
                subtitle = slide_setting['subtitle']

            # apply
            slides.append({
                'src': obj.absolute_url() + '/@@images/image',
                'subtitle': subtitle,
                'title': title,
                'classes': slide_setting['class'],
                'label': slide_setting['label'],
                'link': link,
                'uid': obj.UID() + str(len(slides))
            })

        return slides

    def get_duration(self):
        return getattr(self.context, 'duration', 2000)

    def get_slider_style(self):
        slider_height = getattr(self.context, 'slider_height', 500)

        return 'height: ' + str(slider_height) + 'px'
