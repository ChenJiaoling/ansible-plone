from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
import json
from interaktiv.framework.utilities.api import get_api_utility
from plone.app.uuid.utils import uuidToObject


class ConfigureModalView(BrowserView):
    """ View: Set Image """

    template = ViewPageTemplateFile('templates/configure.pt')
    saving_successful = False

    def __call__(self):
        if 'submit' in self.request.form:
            api_utility = get_api_utility()

            if 'slides' not in self.request.form:
                self.request.form['slides'] = []

            api_utility.set_fields(self.context, self.request.form)
            self.saving_successful = True

        return self.template()

    def get_duration(self):
        return getattr(self.context, 'duration', 2000)

    def get_slider_height(self):
        return getattr(self.context, 'slider_height', 500)

    def get_slides(self):
        slides = []
        references = getattr(self.context, 'slides', [])
        settings_raw = getattr(self.context, 'slide_settings', '')
        settings = []

        if settings_raw and isinstance(settings_raw, basestring):
            settings = json.loads(settings_raw)

        for key, reference in enumerate(references):
            obj = reference.to_object
            slide_setting = {}

            if len(settings) > key:
                slide_setting = settings[key]

            title = getattr(obj, 'title', '')
            if 'title' in slide_setting:
                title = slide_setting['title']

            subtitle = getattr(obj, 'description', '')
            if 'subtitle' in slide_setting:
                subtitle = slide_setting['subtitle']

            slide_data = {
                'src': obj.absolute_url() + '/@@images/image',
                'subtitle': subtitle,
                'title': title,
                'classes': slide_setting['class'],
                'label': slide_setting['label'],
                'id': obj.UID()
            }

            if 'link_type' in slide_setting:
                slide_data['link_type'] = slide_setting['link_type']

            if 'internal_link' in slide_setting:
                link_obj = uuidToObject(slide_setting['internal_link'])
                if link_obj:
                    slide_data['internal_link'] = {
                        'url': link_obj.absolute_url(),
                        'uid': link_obj.UID(),
                        'title': link_obj.title
                    }

            if 'external_link' in slide_setting:
                slide_data['external_link'] = slide_setting['external_link']

            slides.append(slide_data)

        return slides
