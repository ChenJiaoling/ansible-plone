from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.CMFCore.utils import getToolByName


class SelectSlideModalView(BrowserView):
    """ View: Set Image """

    template = ViewPageTemplateFile('templates/select_slide.pt')

    def __call__(self):
        return self.template()

    def get_container(self):
        mediacontainer = []
        catalog = getToolByName(self.context, 'portal_catalog')

        query = {
            "portal_type": ["MediaContainerCT"]
        }

        for brain in catalog(query):
            container_obj = brain.getObject()

            if getattr(container_obj, 'image_type', '') != 'slider_images':
                continue

            mediacontainer.append({
                'url': container_obj.absolute_url(),
                'title': container_obj.title,
                'uid': container_obj.UID(),
                'images': []
            })
            images = mediacontainer[len(mediacontainer)-1]['images']

            for image in container_obj.objectValues():
                if image.portal_type != 'Image':
                    continue

                images.append({
                    'url': image.absolute_url(),
                    'src': image.absolute_url() + '/@@images/image/preview',
                    'subtitle': getattr(image, 'description', ''),
                    'title': getattr(image, 'title', ''),
                    'id': image.UID()
                })

        return mediacontainer
