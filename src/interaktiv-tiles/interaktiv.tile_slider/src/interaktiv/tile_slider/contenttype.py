# -*- coding: UTF-8 -*-
# noinspection PyProtectedMember
from interaktiv.basetiles import _
from interaktiv.basetiles.contenttypes.basetile import BaseTile
from plone.app.vocabularies.catalog import CatalogSource
from z3c.relationfield.schema import RelationList, RelationChoice
from zope import schema
from zope.interface import Interface
from zope.interface import implements
from interaktiv.tile_slider.config import TILE_CONFIG


class ISliderCT(Interface):
    """ Interface for SliderCT """

    title = schema.TextLine(
        title=_(
            u'label_title',
            default=u'Titel'
        ),
        description=u"",
        required=True
    )

    slides = RelationList(
        title=_(
            u"label_slide_objects",
            default=u"Sliderbilder"
        ),
        default=[],
        value_type=RelationChoice(
            title=_(u"Element"),
            source=CatalogSource(portal_type=['Image'])
        ),
        required=False,
    )

    slide_settings = schema.TextLine(
        title=_(
            u"label_slide_settings",
            default=u"Slide Einstellungen"
        ),
        description=u"",
        required=False
    )

    duration = schema.Int(
        title=_(
            u"label_duration",
            default=u"Anzeigedauer"
        ),
        default=2000,
        description=u"Dauer, die eine Slide "
                    u"angezeigt wird bevor gewechselt wird"
    )

    slider_height = schema.Int(
        title=_(
            u"label_slider_height",
            default=u"Anzeigehöhe"
        ),
        default=500,
        description=u"Die Höhe die der Slider insgesamt"
                    u" einnimmt und auf die Bilder anwendet."
    )


class SliderCT(BaseTile):
    """ Add Interface to SliderCT Container """
    implements(ISliderCT)

    # noinspection PyShadowingBuiltins
    def __init__(self, id=None, **kwargs):
        self.tile_config = self.merge_tile_config(TILE_CONFIG)
        self.is_valid = True
        # noinspection PyArgumentList
        BaseTile.__init__(self, id, **kwargs)

    def get_available_tile_actions(self):
        actions = super(SliderCT, self).get_available_tile_actions()
        actions = self.append_tile_action(actions=actions, action={
            'action_id': 'configure',
            'data_modal': "/configure_modal",
            'data_modal_size': 'md',
            'data_modal_params': '{'
                                 ' "tagClass": "configure-slidertile",'
                                 ' "id": "configure-slidertile"'
                                 '}',
            'data_modal_init': 'initializeConfigureModal_SliderTile',
            'attr_title': 'label_configure_tile',
            'attr_class': 'configure tool'
                          ' interaktiv-icon-basetilestheme-option-row',
        })

        return actions

    @property
    def selectable_types(self):
        return ['Image']

    def validate_tile(self):
        if not self.slides:
            self.is_valid = False
            return False
        self.is_valid = True
        return True
