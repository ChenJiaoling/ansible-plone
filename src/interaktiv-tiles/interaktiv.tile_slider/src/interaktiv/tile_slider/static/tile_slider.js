function initializeSelectSlide(selectmodal, configuremodal) {
  var template         = configuremodal.$modal.find('#select_slide_template')
                                       .html();
  var $slideEntryClone = $(template);

  selectmodal.$modal.find('.slide-image').click(function () {
    var $this = $(this);
    var data  = {
      title   : $this.data('title'),
      subtitle: $this.data('subtitle'),
      uid     : $this.data('uid'),
      image   : $this.find('img').attr('src')
    };

    // set fields up
    $slideEntryClone.find('[name="title"]').val(data.title)
                    .attr('id', data.uid + '_title').siblings('label')
                    .attr('for', data.uid + '_subtitle');
    $slideEntryClone.find('[name="subtitle"]').val(data.subtitle)
                    .attr('id', data.uid + '_subtitle').siblings('label')
                    .attr('for', data.uid + '_subtitle');
    $slideEntryClone.find('[name="label"]').val('')
                    .attr('id', data.uid + '_label').siblings('label')
                    .attr('for', data.uid + '_label');
    $slideEntryClone.find('[name="class"]').val('btn btn-default')
                    .attr('id', data.uid + '_class').siblings('label')
                    .attr('for', data.uid + '_class');

    $slideEntryClone.find('img').attr('src', data.image);
    $slideEntryClone.data('uid', data.uid);

    configuremodal.$modal.find('.slide-entries').append($slideEntryClone);
    selectmodal.close();
  });
}

//noinspection JSUnusedGlobalSymbols
function initializeConfigureModal_SliderTile(configuremodal) {
  var base_url = configuremodal.$modal.find('.interaktivbasetiles-modal-header')
                               .data('base_url');
  var tile_uid = configuremodal.$modal.find('.interaktivbasetiles-modal-header')
                               .data('uid');
  var $form    = configuremodal.$modal.find('form');

  configuremodal.$modal.on('click', '[data-action="link_image"]', function () {
    var $slideEntry = $(this).parents('.slide-entry');
    $(this).addClass('active').siblings('button').removeClass('active');
    var $externalLink = $slideEntry.find('.external_link');
    var $internalLink = $slideEntry.find('.internal_link');

    if ($(this).data('type') == 'internal') {
      $externalLink.addClass('hidden');
      $internalLink.removeClass('hidden');
    }
    else {
      $externalLink.removeClass('hidden');
      $internalLink.addClass('hidden');
    }
  });

  configuremodal.$modal.on('click', '[data-action="internallink_image"]', function () {
    var $slideEntry = $(this).parents('.slide-entry');
    var $input      = $slideEntry.find('[name="internal_link"]');

    var selectModal = new ModalClass('selectLink');
    selectModal.openUrl(
      base_url + '/select_content_modal',
      {
        'select-selector'  : $input.getPath(),
        'selectable_types' : ['TilesContainerCT', 'Document'],
        'select-titlefield': $slideEntry.find('.internal_link_label').getPath()
      },
      function () {
        initializeSelectContent(selectModal);
      }
    );
  });

  configuremodal.$modal.on('click', '[data-action="add_image"]', function () {
    var selectmodal = new ModalClass('selectSlide');

    selectmodal.openUrl(base_url + '/select_slide', function () {
      initializeSelectSlide(selectmodal, configuremodal);
    });
  });

  configuremodal.$modal.on('click', '[data-action="remove_image"]', function () {
    $(this).parents('.slide-entry').remove();
  });

  configuremodal.$modal.on('click', '[data-action="move_image"]', function () {
    var $slide    = $(this).parents('.slide-entry');
    var index     = $slide.index();
    var new_index = ($(this).data('dir') == 'up') ? index - 1 : index + 1;
    $slide.parents('.slide-entries').appendAt(new_index, $slide);
  });

  configuremodal.$modal.on('click', '[data-action="save_tile"]', function () {
    $form.find('[name="slides"]').remove();
    $form.find('[name="slide_settings"]').remove();

    var settings = [];
    var slides   = [];

    configuremodal.$modal.find('.slide-entries .slide-entry').each(function () {
      var $this = $(this);

      settings.push({
        label        : $this.find('[name="label"]').val(),
        class        : $this.find('[name="class"]').val(),
        title        : $this.find('[name="title"]').val(),
        subtitle     : $this.find('[name="subtitle"]').val(),
        internal_link: $this.find('[name="internal_link"]').val(),
        external_link: $this.find('[name="external_link"]').val(),
        link_type    : (!$this.find('.internal_link')
                              .hasClass('hidden')) ? 'internal_link' : 'external_link'
      });

      $form.append('<input type="hidden" name="slides" value="' + $this.data('uid') + '" />');
    });

    var $settings = $('<input type="hidden" name="slide_settings" value="" />');
    $settings.val(JSON.stringify(settings));
    $settings.appendTo($form);

    // duration
    $form.find('[name="duration"]')
         .val(configuremodal.$modal.find('#slide_duration').val());

    // slider height
    $form.find('[name="slider_height"]')
         .val(configuremodal.$modal.find('#slider_height').val());

    configuremodal.submitForm(base_url + '/configure_modal', 'html', function (success) {
      if (success) {
        refreshTiles(tile_uid, function () {
          setupSlickSlider($('#' + tile_uid));
        });
        configuremodal.close();
      }
    });
  });
}

function setupSlickSlider($slider) {
  $slider = $($slider);
  var $slideWrapper = $slider.find('.slider-wrapper');
  var $progressBars = $slider.find('.slide-progresses');
  var hovered       = false;

  $slideWrapper
    .slick({
      prevArrow: '<button type="button" class="slick-prev bewo-engineering-skip-left"></button>',
      nextArrow: '<button type="button" class="slick-next bewo-engineering-skip-right"></button>'
    })
    .on('beforeChange', function (event, slick, currentSlide) {
      var uid = $(slick.$slides[currentSlide]).data('uid');
      $progressBars.find('[data-slide="' + uid + '"]').val(0);
    });

  $slider
    .on('mouseover', function () {
      hovered = true;
    })
    .on('mouseleave', function () {
      hovered = false;
    });

  var duration = parseInt($slider.find('.tile-content').data('duration'));

  var interval = setInterval(function () {
    if($slider.isStale()){
      clearInterval(interval);
      return;
    }
    if (hovered) return;
    var $currentSlide = $slideWrapper.find('.slick-active');
    var slideUID      = $currentSlide.data('uid');
    var $progressBar  = $progressBars.find('[data-slide="' + slideUID + '"]');

    if ($progressBar.val() < $progressBar.attr('max')) {
      $progressBar.val($progressBar.val() + 1000 / (duration + 0.1));
    }
    else {
      $slideWrapper.slick('slickNext');
    }
  }, 10);
}

$(document).ready(function () {
  var $body = $('body');
  if (!$body.hasClass('portaltype-tilescontainerct')) return;

  $('.tiles-container').find('.tile-slider').each(function () {
    setupSlickSlider($(this));
  });
});