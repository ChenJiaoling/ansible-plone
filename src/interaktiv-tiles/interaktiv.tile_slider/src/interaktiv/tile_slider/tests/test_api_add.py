from plone import api
from plone.app.testing import TEST_USER_ID, setRoles
from plone.dexterity.utils import createContentInContainer

import json
import transaction

import unittest2 as unittest

from interaktiv.tile_slider.testing import \
    INTERAKTIV_tile_slider_FUNCTIONAL_TESTING


class TestApiAdd(unittest.TestCase):

    layer = INTERAKTIV_tile_slider_FUNCTIONAL_TESTING
    app = None
    portal = None
    request = None

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        # noinspection PyAttributeOutsideInit
        self.tiles_container_a = createContentInContainer(
            self.portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )
        # noinspection PyAttributeOutsideInit
        self.image = createContentInContainer(
            self.portal,
            'Image',
            id='image_a',
        )

    def test_add_slider_tile_to_tile_row(self):
        tile_row_a = createContentInContainer(
            self.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        self.request.form['portal_type'] = 'SliderCT'
        self.request.form['position'] = '0'
        view = api.content.get_view(
            name='add_tile',
            context=tile_row_a,
            request=self.request,
        )
        result = json.loads(view())
        self.assertEqual(len(self.portal.tiles_container_a.objectIds()), 1)
        tile_row = self.portal.tiles_container_a.objectValues()[0]
        self.assertEqual(result['url'], tile_row.absolute_url())
        tile = tile_row.objectValues()[0]
        self.assertEqual(tile.portal_type, 'SliderCT')

    def test_add_slider_tile_to_tiles_container_no_position(self):
        self.request.form['portal_type'] = 'SliderCT'
        view = api.content.get_view(
            name='add_tile',
            context=self.portal.tiles_container_a,
            request=self.request,
        )
        view()
        self.assertEqual(len(self.portal.tiles_container_a.objectIds()), 1)
        tile_row = self.portal.tiles_container_a.objectValues()[0]
        tile = tile_row.objectValues()[0]
        self.assertEqual(tile.portal_type, 'SliderCT')

    def test_add_slider_tile_to_tile_row_no_position(self):
        tile_row_a = createContentInContainer(
            self.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        createContentInContainer(
            tile_row_a,
            'SliderCT',
            id='image_tile_a',
        )
        self.request.form['portal_type'] = 'SliderCT'
        view = api.content.get_view(
            name='add_tile',
            context=tile_row_a,
            request=self.request,
        )
        view()
        self.assertEqual(len(tile_row_a.objectIds()), 2)
        self.assertEqual(tile_row_a.objectValues()[0].id, 'image_tile_a')

    def test_add_slider_tile_to_tile_row_with_position(self):
        tile_row_a = createContentInContainer(
            self.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        createContentInContainer(
            tile_row_a,
            'SliderCT',
            id='image_tile_a',
        )
        self.request.form['portal_type'] = 'SliderCT'
        self.request.form['position'] = '0'
        view = api.content.get_view(
            name='add_tile',
            context=tile_row_a,
            request=self.request,
        )
        view()
        self.assertEqual(len(tile_row_a.objectIds()), 2)
        self.assertEqual(tile_row_a.objectValues()[1].id, 'image_tile_a')

    def test_add_slider_tile_to_tile_container_with_position(self):
        createContentInContainer(
            self.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        self.request.form['portal_type'] = 'SliderCT'
        self.request.form['position'] = '0'
        view = api.content.get_view(
            name='add_tile',
            context=self.portal.tiles_container_a,
            request=self.request,
        )
        view()
        self.assertEqual(len(self.portal.tiles_container_a.objectIds()), 2)
        self.assertEqual(
            self.portal.tiles_container_a.objectValues()[1].id, 'tile_row_a'
        )

    def tearDown(self):
        if 'tiles_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['tiles_container_a'])
        transaction.commit()


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestApiAdd))
    return suite
