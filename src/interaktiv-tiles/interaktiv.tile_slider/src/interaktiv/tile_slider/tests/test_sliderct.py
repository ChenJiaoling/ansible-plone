# -*- coding: UTF-8 -*-
from interaktiv.tile_slider.contenttype import ISliderCT
from interaktiv.tile_slider.testing import \
    INTERAKTIV_tile_slider_INTEGRATION_TESTING

import transaction
import unittest2 as unittest
from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility


class SliderCTTest(unittest.TestCase):

    layer = INTERAKTIV_tile_slider_INTEGRATION_TESTING
    portal = None
    request = None

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        self.request['ACTUAL_URL'] = self.portal.absolute_url()
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.portal.invokeFactory(
            "TilesContainerCT",
            id="tilescontainerct",
            title="TilesContainerCT",
        )
        self.portal.tilescontainerct.invokeFactory(
            "TileRowCT",
            id="tilerowct",
            title="TileRowCT",
        )
        transaction.commit()

    def tearDown(self):
        if 'tilescontainerct' in self.portal.objectIds():
            self.portal.manage_delObjects(['tilescontainerct'])
        transaction.commit()

    def test_schema(self):
        fti = queryUtility(
            IDexterityFTI,
            name='SliderCT'
        )
        schema = fti.lookupSchema()
        self.assertEquals(ISliderCT, schema)

    def test_fti(self):
        fti = queryUtility(
            IDexterityFTI,
            name='SliderCT'
        )
        self.assertNotEquals(None, fti)

    def test_factory(self):
        fti = queryUtility(
            IDexterityFTI,
            name='SliderCT'
        )
        factory = fti.factory
        new_object = createObject(factory)
        self.assertEqual(
            new_object.__module__,
            'interaktiv.tile_slider.contenttype'
        )

    def test_text_ct_provides_interface(self):
        fti = queryUtility(
            IDexterityFTI,
            name='SliderCT'
        )
        factory = fti.factory
        new_object = createObject(factory)

        from interaktiv.tile_slider.contenttype import ISliderCT
        self.failUnless(ISliderCT.providedBy(new_object))

    def test_adding(self):
        self.portal.tilescontainerct.tilerowct.invokeFactory(
            'SliderCT', 'SliderCT'
        )
        self.assertTrue(self.portal.tilescontainerct.tilerowct.SliderCT)
