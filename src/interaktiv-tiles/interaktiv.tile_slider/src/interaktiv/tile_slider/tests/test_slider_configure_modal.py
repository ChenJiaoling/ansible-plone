import json

import transaction
import unittest2 as unittest
from interaktiv.tile_slider.testing import \
    INTERAKTIV_tile_slider_FUNCTIONAL_TESTING
from plone import api
from plone.app.testing import TEST_USER_ID, setRoles
from plone.dexterity.utils import createContentInContainer
from plone.testing.z2 import Browser


class TestConfigureModal(unittest.TestCase):
    layer = INTERAKTIV_tile_slider_FUNCTIONAL_TESTING
    app = None
    portal = None
    request = None
    browser = None
    image_a = None
    slider_tile_a = None

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']

        self.browser = Browser(self.app)
        self.browser.handleErrors = False

        setRoles(self.portal, TEST_USER_ID, ['Manager'])

        createContentInContainer(
            self.portal,
            'MediaContainerCT',
            id='media_container_a',
            image_type="slider_images"
        )
        self.portal.media_container_a.reindexObject()
        self.image_a = createContentInContainer(
            self.portal.media_container_a,
            'Image',
            id='image_a',
        )
        createContentInContainer(
            self.portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )
        createContentInContainer(
            self.portal.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        self.slider_tile_a = createContentInContainer(
            self.portal.tiles_container_a.tile_row_a,
            'SliderCT',
            id='slider_tile_a',
        )
        transaction.commit()

    def test_configure_modal(self):
        self.request.form['slides'] = [
            self.image_a.UID()
        ]
        self.request.form['submit'] = ''
        self.request.form['duration'] = '3000'
        self.request.form['slider_height'] = '200'

        slide_settings = [
            {
                "label": "test_label",
                "class": "first-class second-class",
                "title": "test_title",
                "subtitle": "test_subtitle",
                "internal_link": "",
                "external_link": "",
                "link_type": ""
            }
        ]
        self.request.form['slide_settings'] = json.dumps(slide_settings)

        view = api.content.get_view(
            name='update_content',
            context=self.slider_tile_a,
            request=self.request
        )
        view()

        self.assertEquals(self.slider_tile_a.duration, 3000)
        self.assertEquals(self.slider_tile_a.slider_height, 200)
        slides = self.slider_tile_a.slides
        self.assertEquals(len(slides), 1)
        self.assertEquals(slides[0].to_object, self.image_a)
        self.assertEquals(
            json.loads(self.slider_tile_a.slide_settings),
            slide_settings
        )

    def tearDown(self):
        if 'tiles_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['tiles_container_a'])
        if 'media_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['media_container_a'])
        transaction.commit()


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestConfigureModal))
    return suite
