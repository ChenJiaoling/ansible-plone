import transaction
import unittest2 as unittest
from interaktiv.tile_slider.testing import INTERAKTIV_tile_slider_FUNCTIONAL_TESTING
from plone.app.testing import TEST_USER_ID, setRoles
from plone.dexterity.utils import createContentInContainer
from plone.testing.z2 import Browser


class TestApiUpdate(unittest.TestCase):

    layer = INTERAKTIV_tile_slider_FUNCTIONAL_TESTING
    app = None
    portal = None
    request = None
    browser = None

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']

        self.browser = Browser(self.app)
        self.browser.handleErrors = False

        setRoles(self.portal, TEST_USER_ID, ['Manager'])

        createContentInContainer(
            self.portal,
            'MediaContainerCT',
            id='media_container_a',
            image_type="slider_images"
        )
        self.portal.media_container_a.reindexObject()
        createContentInContainer(
            self.portal.media_container_a,
            'Image',
            id='image_a',
        )
        self.portal.media_container_a.image_a.reindexObject()
        createContentInContainer(
            self.portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )
        createContentInContainer(
            self.portal.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        # noinspection PyAttributeOutsideInit
        self.slider_tile_a = createContentInContainer(
            self.portal.tiles_container_a.tile_row_a,
            'SliderCT',
            id='slider_tile_a',
        )
        transaction.commit()

    def tearDown(self):
        if 'tiles_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['tiles_container_a'])
        if 'media_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['media_container_a'])
        transaction.commit()


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestApiUpdate))
    return suite
