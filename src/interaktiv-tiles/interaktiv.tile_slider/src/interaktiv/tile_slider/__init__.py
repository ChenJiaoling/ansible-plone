from zope.i18nmessageid import MessageFactory
import logging

_ = MessageFactory("interaktiv.tile_slider")
logger = logging.getLogger('interaktiv.tile_slider')


def initialize(context):
    """Initializer called when used as a Zope 2 product."""
