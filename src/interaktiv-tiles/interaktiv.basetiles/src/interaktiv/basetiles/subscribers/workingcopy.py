# -*- coding: utf-8 -*-
import transaction
from plone.app.iterate import lock
from plone.app.lockingbehavior.behaviors import ILocking
from plone.app.uuid.utils import uuidToObject
from zope.event import notify
from zope.lifecycleevent import ObjectModifiedEvent


def _unlock_object_recursive(container):
    if ILocking.providedBy(container):
        lock.unlockContext(container)
    for obj in container.objectValues():
        if ILocking.providedBy(obj):
            _unlock_object_recursive(obj)


def _update_object(obj):
    obj.reindexObject()
    notify(ObjectModifiedEvent(obj))


def workingcopy_removed(obj, event):
    oringinal_uid = obj.workingcopy_original_uid
    if not oringinal_uid:
        return
    original = uuidToObject(oringinal_uid)
    if not original:
        return
    original.workingcopy_uid = ''
    original.workingcopy_original_uid = ''
    _unlock_object_recursive(original)
    _update_object(original)
    transaction.commit()
