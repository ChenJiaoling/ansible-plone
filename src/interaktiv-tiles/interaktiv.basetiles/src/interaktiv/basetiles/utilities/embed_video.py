# -*- coding: utf-8 -*-
from urlparse import parse_qs
from urlparse import urlparse

import requests
from zope.component import getUtility
from zope.interface import Interface


class IEmbedVideoUtility(Interface):
    """ utility to provide methods for embedding videos
    """


def get_embedvideo_utility():
    return getUtility(IEmbedVideoUtility)


class EmbedVideoUtility(object):
    """ utility to provide methods for embedding videos
    """

    context = None
    selected_types = list()

    def get_embed_url(self, video_url, portal):
        """ Return embed url for a given video url and video portal. """
        generator = self.get_method_by_string('get_%s_embed_url' % portal)
        video_id = self.get_video_id(video_url, portal)
        return generator(video_id)

    def get_video_thumbnail(self, video_url, portal):
        """ Return image url of a video thumbnail for a given video url and
        video portal. """
        generator = self.get_method_by_string('get_%s_thumbnail' % portal)
        video_id = self.get_video_id(video_url, portal)
        return generator(video_id)

    def get_video_id(self, video_url, portal):
        """ Return video id for a given video url and video portal. """
        generator = self.get_method_by_string('get_%s_video_id' % portal)
        # Check if url is valid
        if not self.valid_url(video_url):
            return video_url  # We assume that the given url is actually an id
        return generator(video_url)

    # Youtube
    @classmethod
    def get_youtube_embed_url(cls, video_id):
        if not video_id:
            return str()
        # build embed youtube url
        url = "https://www.youtube.com/embed/" + video_id + "?rel=0"
        return url

    @classmethod
    def get_youtube_thumbnail(cls, video_id):
        videothumbnail = None
        if video_id:
            videothumbnail = "https://img.youtube.com/vi/" + video_id + \
                             "/mqdefault.jpg"
        return videothumbnail

    @classmethod
    def get_youtube_video_id(cls, video_url):
        parsed_url = urlparse(video_url)
        parsed_query = parse_qs(parsed_url.query)
        if 'v' in parsed_query:
            return str(parsed_query['v'][0])
        return str()

    # Vimeo
    @classmethod
    def get_vimeo_embed_url(cls, video_id):
        if not video_id:
            return str()
        # build embed url
        url = "https://player.vimeo.com/video/" + video_id + \
              "?title=1&byline=0&badge=0"
        return url

    @classmethod
    def get_vimeo_thumbnail(cls, video_id):
        videothumbnail = None
        if video_id:
            request_url = 'http://vimeo.com/api/v2/video/' + video_id + '.json'
            request = requests.get(request_url)
            if not request.ok:
                return None
            data = request.json()
            if data and len(data) > 0:
                videothumbnail = data[0].get('thumbnail_medium', None)
        return videothumbnail

    @classmethod
    def get_vimeo_video_id(cls, video_url):
        """ Supported vimeo url formart:
                http://vimeo.com/193266260 """
        parsed_url = urlparse(video_url)
        if parsed_url.path:
            return parsed_url.path.lstrip("/")

        # Some Notes:
        # response = re.search(r'^(http://)?(www\.)?(vimeo\.com/)?(\d+)',
        #                      embed_url).group(4)
        # (https?://)?(www.)?(player.)?vimeo.com/([a-z]*/)*([0-9]{6,11})[?]?.*

        return str()

    # Helper methods
    @staticmethod
    def valid_url(url):
        """ Validate a url"""
        try:
            result = urlparse(url)
            return True if all([result.scheme, result.netloc,
                                result.path]) else False
        except:
            return False

    def get_method_by_string(self, method_name):
        method = getattr(self, method_name, None)
        if not callable(method):
            raise LookupError(
                'Could not found method name %s' % method_name)
        return method
