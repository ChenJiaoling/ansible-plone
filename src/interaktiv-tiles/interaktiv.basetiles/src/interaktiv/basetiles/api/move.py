import transaction
from Acquisition import aq_parent
from OFS.interfaces import IOrderedContainer
from interaktiv.basetiles.api.api import ApiBaseView
from interaktiv.basetiles.api.api import json_api_call
from interaktiv.basetiles.api.interfaces import IAPIMethodWeakCaching
from zope.interface import implements


class MoveContent(ApiBaseView):
    implements(IAPIMethodWeakCaching)

    @json_api_call
    def __call__(self, *kw, **kwargs):
        if not self.handle_required_fields(['position'], self.request.form):
            return False

        position = self.request.form['position']
        if not position.isdigit():
            msg = 'BAD REQUEST - Position should be a positive integer'
            self.request.response.setStatus(
                400,
                reason=msg,
                lock=None
            )
            return False

        ordered_container = IOrderedContainer(aq_parent(self.context))
        ordered_container.moveObjectToPosition(
            self.context.id, int(position)
        )

        transaction.commit()
        return {
            'uid': self.context.UID()
        }
