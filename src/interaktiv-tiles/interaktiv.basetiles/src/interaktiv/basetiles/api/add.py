# -*- coding: utf-8 -*-
import transaction
from interaktiv.basetiles.api.api import ApiBaseView
from interaktiv.basetiles.api.api import json_api_call
from interaktiv.basetiles.api.interfaces import IAPIMethodWeakCaching
from plone.dexterity.utils import createContentInContainer
from OFS.interfaces import IOrderedContainer
from zope.interface import implements

from interaktiv.framework.utilities.api import get_api_utility


class AddTile(ApiBaseView):
    implements(IAPIMethodWeakCaching)

    @json_api_call
    def __call__(self, *kw, **kwargs):
        if not self.handle_required_fields(['portal_type'], self.request.form):
            return False

        if 'position' in self.request.form.keys():
            position = self.request.form['position']
            if not position.isdigit():
                msg = 'BAD REQUEST - Position should be a positive integer'
                self.request.response.setStatus(
                    400,
                    reason=msg,
                    lock=None
                )
                return False
        else:
            position = -1

        # Get portal_type
        portal_type = self.request.form['portal_type']
        api_utility = get_api_utility()

        if self.context.portal_type == 'TileRowCT':
            tile_row = self.context

            if len(tile_row.objectValues()) >= 12:
                msg = 'There shouldn\'t be more than 12 Tiles in one row'
                self.request.response.setStatus(
                    400,
                    reason=msg,
                    lock=None
                )
                return False

        elif self.context.portal_type == 'TilesContainerCT':
            # create tile row
            tile_row = createContentInContainer(
                self.context,
                'TileRowCT',
                id=api_utility.generate_id(),
            )
            tile_row.title = 'TileRowCT'
        else:
            return False

        # create and move tile
        tile = self._create_tile(tile_row, portal_type, api_utility)
        if position > -1:
            if self.context.portal_type == 'TilesContainerCT':
                ordered_container = IOrderedContainer(self.context)
                # noinspection PyArgumentList
                ordered_container.moveObjectToPosition(
                    tile_row.id, int(position)
                )
            elif self.context.portal_type == 'TileRowCT':
                ordered_container = IOrderedContainer(tile_row)
                # noinspection PyArgumentList
                ordered_container.moveObjectToPosition(
                    tile.id, int(position)
                )

        # tile_config: set tilerow_span
        if tile.tile_config.get('tilerow_span', False):
            tile_row.tilerow_span = True

        transaction.commit()

        data = {
            'url': tile_row.absolute_url(),
            'uid': tile_row.UID(),
            'render': tile_row.render(self.request)
        }
        return data

    def _create_tile(self, tile_row, portal_type, api_utility=None):
        if not api_utility:
            api_utility = get_api_utility()
        # create tile and set title
        tile = createContentInContainer(
            tile_row,
            portal_type,
            id=api_utility.generate_id(),
        )
        tile.title = portal_type

        # set tile inverted state
        inverted = self.request.form.get('inverted', '')
        if inverted == 'True':
            tile.inverted = True
        else:
            tile.inverted = False

        # create default entries for tiles which contain entries
        if 'entry_count' in self.request.form.keys():
            tile.create_default_entries(
                self.request.form['entry_count']
            )
            for entry in tile.objectValues():
                api_utility.publish_object(entry)
                entry.reindexObject()

        # set fields from request
        api_utility.set_fields(tile, self.request.get('fields', '{}'))

        # reindex and return api result
        tile.reindexObject()
        return tile
