# -*- coding: utf-8 -*-
import json

from interaktiv.basetiles.api.api import ApiBaseView
from interaktiv.basetiles.api.api import json_api_call
from interaktiv.basetiles.api.interfaces import IAPIMethodWeakCaching
from interaktiv.basetiles.contenttypes.tilescontainerct import ITilesContainerCT
from interaktiv.framework.helper import IFHelper
from plone import api
from plone.dexterity.utils import createContentInContainer
from zope.interface import implements


class TileTemplateBase(ApiBaseView):

    def get_templates(self):
        # load current templates
        templates = api.portal.get_registry_record(
            'interaktiv.basetiles.tile-templates')
        return IFHelper.load_json_from_string(templates)

    def get_template(self, uid):
        for template in self.get_templates():
            if uid in template['uid']:
                return template
        return dict()


class AddTileTemplate(TileTemplateBase):
    implements(IAPIMethodWeakCaching)

    @json_api_call
    def __call__(self, *kw, **kwargs):
        form = self.request.form
        if self.handle_required_fields(['template_name'], form):
            if ITilesContainerCT.providedBy(self.context):
                new_template = self._build_template(
                    self.context,
                    form.get('template_name')
                )
                if new_template:
                    return self._add_template(new_template)
        return False

    def _build_template(self, tilescontainer, template_name):
        template = {
            'uid': IFHelper.generate_uuid(),
            'name': template_name,
            'command': []
        }
        for tilerow_id, tilerow in tilescontainer.contentItems():
            tiles = list()
            for tile_id, tile in tilerow.contentItems():
                tiles.append({'portal_type': tile.portal_type})
            template['command'].append({
                'portal_type': tilerow.portal_type,
                'tiles': tiles
            })
        return template

    def _add_template(self, new_template):
        templates = self.get_templates()
        templates.append(new_template)
        api.portal.set_registry_record(
            'interaktiv.basetiles.tile-templates',
            unicode(json.dumps(templates))
        )
        return True


class LoadTileTemplate(TileTemplateBase):
    implements(IAPIMethodWeakCaching)

    @json_api_call
    def __call__(self, *kw, **kwargs):
        if self.handle_required_fields(['template_uid'], self.request.form):
            template = self.get_template(self.request.form['template_uid'])
            if template:
                self._create_tiles(template['command'])
                return True
        return False

    def _create_tiles(self, command):
        tilescontainer = self.context
        for tilerow_config in command:
            tilerow = createContentInContainer(
                tilescontainer,
                tilerow_config['portal_type'],
                id=IFHelper.generate_uuid(),
            )
            tilerow.reindexObject()
            for tile_config in tilerow_config['tiles']:
                tile = createContentInContainer(
                    tilerow,
                    tile_config['portal_type'],
                    id=IFHelper.generate_uuid(),
                )
                tile.reindexObject()
        return self.context


class RemoveTileTemplate(ApiBaseView):
    implements(IAPIMethodWeakCaching)

    @json_api_call
    def __call__(self, *kw, **kwargs):
        if self.handle_required_fields(['template_uid'], self.request.form):
            template = self.get_template(self.request.form['template_uid'])
            if template:
                templates = self.get_templates()
                templates.pop(template)
                return True
        return False
