# -*- coding: utf-8 -*-
import json

from Products.CMFPlone.interfaces.siteroot import IPloneSiteRoot
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from interaktiv.basetiles.api.interfaces import (
    IAPIRequest,
    IAPIMethod,
    IRestApi
)
from plone.protect.interfaces import IDisableCSRFProtection
from zope.component import (
    adapter, getMultiAdapter, getGlobalSiteManager)
from zope.component.hooks import getSite
from zope.interface import alsoProvides
from zope.interface import implementer
from zope.interface import implements
from zope.publisher.interfaces.http import IHTTPRequest
from zope.traversing.interfaces import ITraversable

from zope.event import notify
from zope.lifecycleevent import ObjectModifiedEvent


def json_api_call(func):
    """ Returns json output
    """

    def decorator(*args, **kwargs):
        instance = args[0]
        request = getattr(instance, 'request', None)
        request.response.setHeader(
            'Content-Type',
            'application/json; charset=utf-8'
        )
        result = func(*args, **kwargs)
        return json.dumps(result, indent=2)

    return decorator


class ApiBaseView(BrowserView):
    """ Base view to set some needed headers for all Api views.
    """
    implements(IAPIMethod)

    api_util = None
    form = dict()

    def __init__(self, context, request):
        self.context = context
        self.request = request
        request.response.setHeader('X-Theme-Disabled', 'True')
        alsoProvides(request, IDisableCSRFProtection)

    def handle_required_fields(self, required_fields, form):
        """ Check all required parameters of api call
        :param list - required_fields: all required parameters (fields)
        :param dict - form: all given parameters of api call
        :return list: if parameter is missing
        :return bool: if all required parameters exist
        """
        missing_fields = list()
        for required_field in required_fields:
            if isinstance(required_field, tuple):
                found = False
                for required_field_sub in required_field:
                    if required_field_sub in form:
                        found = True
                if not found:
                    missing_fields.append(' or '.join(required_field))
            else:
                if required_field not in form:
                    missing_fields.append(required_field)
        if missing_fields:
            msg = "BAD REQUEST - Missing parameters: " + ", ".join(
                missing_fields
            )
            self.request.response.setStatus(
                400,
                reason=msg,
                lock=None
            )
            return list()
        return True

    def _update_object(self, obj):
        obj.reindexObject()
        notify(ObjectModifiedEvent(obj))


@adapter(IPloneSiteRoot, IHTTPRequest)
@implementer(ITraversable)
class APITraverser(object):
    """ The root API traverser
    """

    def __init__(self, context, request=None):
        self.context = context
        self.request = request

    def traverse(self, name, postpath):  # pylint: disable=unused-argument
        alsoProvides(self.request, IAPIRequest)
        return self.context


class ApiOverview(BrowserView):

    template = ViewPageTemplateFile('templates/api.pt')

    def __call__(self):
        self.request.response.setHeader('X-Theme-Disabled', 'True')
        return self.template()

    def api_views(self):
        portal = getSite()
        portal_url = portal.absolute_url()
        gsm = getGlobalSiteManager()
        api_views = list()
        for api_adapter in gsm.registeredAdapters():
            if not api_adapter.provided == IRestApi:
                continue
            api_view = dict()
            api_view['id'] = api_adapter.name
            view = getMultiAdapter(
                (portal, self.request),
                IRestApi,
                name=api_adapter.name)
            # XXX: this works only for directly subclass from BrowserView
            api_view['description'] = view.__implemented__.inherit.__doc__
            api_view['url'] = '%s/++api++v1/%s' % (
                portal_url, api_adapter.name)
            api_views.append(api_view)
        return api_views
