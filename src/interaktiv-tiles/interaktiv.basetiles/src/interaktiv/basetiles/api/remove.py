import transaction
from Acquisition import aq_parent
from interaktiv.basetiles.api.api import ApiBaseView
from interaktiv.basetiles.api.api import json_api_call
from interaktiv.basetiles.api.interfaces import IAPIMethodWeakCaching
from zope.interface import implements


class RemoveContent(ApiBaseView):
    implements(IAPIMethodWeakCaching)

    @json_api_call
    def __call__(self, *kw, **kwargs):
        uid = self.context.UID()
        container = aq_parent(self.context)
        container.manage_delObjects([self.context.id])

        # if removed content is a tile and
        # after removing the tilerow is now empty
        # then also remove the tilerow
        if container.portal_type == 'TileRowCT':
            if len(container) == 0:
                tilescontainer = aq_parent(container)
                tilescontainer.manage_delObjects([container.id])

        transaction.commit()
        return {
            'uid': uid
        }
