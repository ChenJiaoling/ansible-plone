# -*- coding: utf-8 -*-
import transaction
from interaktiv.basetiles.api.api import ApiBaseView
from interaktiv.basetiles.api.api import json_api_call
from interaktiv.basetiles.api.interfaces import IAPIMethodWeakCaching

from zope.interface import implements
from interaktiv.basetiles.contenttypes.tilerowct import \
    ITileRowCT
from interaktiv.basetiles.contenttypes.basetile import IBaseTile

from interaktiv.framework.utilities.api import get_api_utility


class UpdateContent(ApiBaseView):
    implements(IAPIMethodWeakCaching)

    html_parser = None

    @json_api_call
    def __call__(self, *kw, **kwargs):
        """ Update Plone content
        @param kw:
        @param kwargs:
        @return bool: API-Call has worked
        or
        @return dict: For Tile Content
        """
        api_utility = get_api_utility()
        api_utility.set_fields(self.context, self.request.form)

        self._update_object(self.context)
        transaction.commit()

        do_render = False
        if 'do_no_render' not in self.request.form.keys():
            if ITileRowCT.providedBy(self.context):
                do_render = True
            if IBaseTile.providedBy(self.context):
                do_render = True

        if do_render:
            # Render Tile and build json output
            data = {
                'uid': self.context.UID(),
                'url': self.context.absolute_url(),
                'render': self.context.render(self.request)
            }
            return data
        return True
