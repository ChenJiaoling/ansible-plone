from zope.interface import Interface


class IAPIRequest(Interface):
    """Marker for API requests.
    """


class IAPIMethod(Interface):
    """Marker for API methods.
    """


class IAPIMethodWeakCaching(Interface):
    """Marker for API method with weak caching.
    """


class IJsonApiCall(Interface):
    """Marker Interface for all JSON API calls.
    """

    def __call__():
        """Returns a JSON object.
        """


class IAPIUtility(Interface):
    """ Utility for API methods.
    """


class IRestApi(Interface):
    """
    """

    def traversal():
        """Returns the object as json.
        """

    def navigation_tree():
        """Returns the full portlet navigation tree.
        """

    def top_navigation():
        """Returns the top navigation entries.
        """

    def breadcrumbs():
        """Returns the breadcrumb entries.
        """
