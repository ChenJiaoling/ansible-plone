from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile


class AddTileTemplateModalView(BrowserView):
    """ View: To trigger add tile templates """

    template = ViewPageTemplateFile('templates/add_tile_template.pt')

    def __call__(self):
        return self.template()
