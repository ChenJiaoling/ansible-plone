from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from interaktiv.basetiles.vocabularies.paddings import PADDINGS
from interaktiv.framework.utilities.api import get_api_utility


class TilePaddingModalView(BrowserView):
    """ View: Select Padding Modal"""

    template = ViewPageTemplateFile('templates/tile_select_padding.pt')
    saving_successful = False

    def __call__(self):
        if 'submit' in self.request.form:
            api_utility = get_api_utility()
            api_utility.set_fields(self.context, self.request.form)
            self.saving_successful = True
        return self.template()

    @staticmethod
    def get_paddings():
        return PADDINGS
