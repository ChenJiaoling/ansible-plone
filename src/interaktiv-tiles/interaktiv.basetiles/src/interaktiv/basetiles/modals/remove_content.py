from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile


class RemoveContentModalView(BrowserView):
    """ View: To trigger remove content """

    template = ViewPageTemplateFile('templates/remove_content.pt')

    def __call__(self):
        return self.template()
