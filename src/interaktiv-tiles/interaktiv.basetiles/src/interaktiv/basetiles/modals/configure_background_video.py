# -*- coding: UTF-8 -*-
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from interaktiv.basetiles.vocabularies.animations import ANIMATIONS
import json
from interaktiv.basetiles.utilities.embed_video import get_embedvideo_utility


class ConfigureBackgroundVideoModalView(BrowserView):
    """ View: Background Color """

    template = ViewPageTemplateFile('templates/configure_background_video.pt')

    def __call__(self):
        return self.template()

    @staticmethod
    def get_animations():
        return ANIMATIONS

    @staticmethod
    def get_background_video_object(context_obj):
        video_obj = getattr(context_obj, 'background_video_data', None)
        if video_obj:
            return video_obj.to_object
        return ''

    @staticmethod
    def select_portaltype():
        return json.dumps(['File', 'VideourlCT'])

    @staticmethod
    def get_background_video_embedurl(video_obj):
        videourl = getattr(video_obj, 'videourl', '')
        if videourl:
            embedvideo_utility = get_embedvideo_utility()
            youtube_video_id = embedvideo_utility.get_youtube_video_id(videourl)
            youtube_embed_url = embedvideo_utility.get_youtube_embed_url(
                youtube_video_id
            )
            return youtube_embed_url + '&autoplay=1&controls=0&mute=1&' \
                                       'iv_load_policy=3&showinfo=0'
        return ''




