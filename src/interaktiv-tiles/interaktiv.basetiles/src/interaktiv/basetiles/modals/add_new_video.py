# -*- coding: UTF-8 -*-
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
import json
from random import choice
from plone.app.uuid.utils import uuidToObject
from plone.namedfile import NamedBlobFile
from plone.dexterity.utils import createContentInContainer
import transaction
# noinspection PyProtectedMember
from interaktiv.basetiles import _


class AddNewVideoModalView(BrowserView):
    """ View: Background Color """

    template = ViewPageTemplateFile('templates/add_new_video.pt')

    def __call__(self):
        self.new_video_data = None
        self.errormessage = None
        if 'submit' in self.request.form:
            add_new_video_response = self.add_new_video()
            if isinstance(add_new_video_response, basestring):
                self.errormessage = _(add_new_video_response)
            else:
                self.new_video_data = json.dumps(add_new_video_response)

        return self.template()

    @staticmethod
    def select_portaltype():
        return json.dumps(['MediaContainerCT'])

    def add_new_video(self):
        form = self.request.form

        video_container_uid = form.get('video_container_uid', '')
        if not video_container_uid:
            return 'video_uid_not_found'
        selected_container = uuidToObject(video_container_uid)
        if not selected_container:
            return 'container_not_found'

        video_title = form.get('video_title', '')
        if not video_title:
            return 'video_title_not_found'

        # get video file or videourl
        select_video_file = form.get('select_video_file', None)
        videourl = form.get('youtube_url', '')
        if select_video_file:
            video_file = NamedBlobFile(
                select_video_file.read(),
                filename=unicode(select_video_file.filename, "utf-8")
            )
            video = createContentInContainer(
                selected_container,
                'File',
                id=self._generate_id(),
                file=video_file,
                title=str(video_title)
            )
            video.reindexObject()
            transaction.commit()
            return {
                "uid": video.UID(),
                "videourl": video.absolute_url(),
                "portaltype": video.portal_type,
                "title": video.title
            }
        elif videourl:
            video = createContentInContainer(
                selected_container,
                'VideourlCT',
                id=self._generate_id(),
                title=str(video_title),
                videourl=videourl
            )
            video.reindexObject()
            transaction.commit()
            return {
                "uid": video.UID(),
                "videourl": video.videourl,
                "portaltype": video.portal_type,
                "title": video.title
            }
        return 'video_file_and_youtube_video_url_not_found'

    @staticmethod
    def _generate_id():
        # noinspection PyUnusedLocal
        return str().join([choice('abcd1234') for i in range(12)])
