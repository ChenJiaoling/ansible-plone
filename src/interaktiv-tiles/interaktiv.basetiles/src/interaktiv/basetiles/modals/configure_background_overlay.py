# -*- coding: UTF-8 -*-
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from interaktiv.basetiles.vocabularies.colors import COLOR_OPTIONS
from interaktiv.basetiles.vocabularies.transparencies import TRANSPARENCIES


class ConfigureBackgroundOverlayModalView(BrowserView):
    """ View: Background Color """

    template = ViewPageTemplateFile('templates/configure_background_overlay.pt')

    def __call__(self):
        return self.template()

    def get_background_overlay_colors(self):
        available_colors = COLOR_OPTIONS
        current_color = getattr(self.context, "background_overlay_color", "")

        options = list()
        for color in available_colors:
            is_color_in_use = bool(current_color == color.get('id', ""))

            # Build color option dict
            option = {
                'id': color.get('id', ""),
                'value': color.get('value', ""),
                'title': color.get('title', ""),
                'class': [
                    "color-item",
                    color.get('icon', ""),
                    "selected" if is_color_in_use else ""
                ],
                'style': {
                    "background-color": color.get('value', "")
                },
            }

            # Concatenate CSS classes
            option['class'] = ' '.join(option['class'])

            # Concatenate CSS inline stylings
            style = ""
            for key in option['style']:
                style += key + ':' + option['style'][key] + ';'
            option['style'] = style

            # Add new option
            options.append(option)
        return options

    @staticmethod
    def get_transparencies():
        return TRANSPARENCIES







