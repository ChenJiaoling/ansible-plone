# -*- coding: UTF-8 -*-
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from interaktiv.framework.utilities.api import get_api_utility


class RemoveBackgroundOptionsModalView(BrowserView):
    """ View: To trigger remove content """

    template = ViewPageTemplateFile('templates/remove_background_options.pt')

    def __call__(self):
        form = self.request.form
        if 'submit' in form:
            form['background_image_data'] = None
            form['background_video_data'] = None
            form['background_color'] = 'transparent'
            form['background_overlay_opacity'] = '0'
            form['background_overlay_color'] = 'transparent'
            api_utility = get_api_utility()
            api_utility.set_fields(self.context, form)
        return self.template()
