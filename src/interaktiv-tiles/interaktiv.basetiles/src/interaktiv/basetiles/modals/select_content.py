# -*- coding: UTF-8 -*-
from Acquisition import aq_parent
from Products.CMFCore.utils import getToolByName
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.app.uuid.utils import uuidToObject
from plone.dexterity.interfaces import IDexterityContainer
from plone.protect.interfaces import IDisableCSRFProtection
from zope.interface import alsoProvides
from zope.site.hooks import getSite
from interaktiv.framework.helper import IFHelper
from interaktiv.basetiles.contenttypes.videourlct import IVideourlCT


class SelectContentModalView(BrowserView):
    """ Select Content From Content Tree """

    template = ViewPageTemplateFile('templates/select_content.pt')

    def __init__(self, context, request):
        super(SelectContentModalView, self).__init__(context, request)
        alsoProvides(request, IDisableCSRFProtection)

    def __call__(self):
        return self.template()

    def _get_list_value_from_request(self, key):
        value = self.request.form.get(key, [])
        if not value:
            value = self.request.form.get(key + '[]', [])
        if not value:
            return []
        if isinstance(value, basestring):
            return [value]
        return value

    @staticmethod
    def _is_selectable(portal_type, selectable_types):
        if not selectable_types:
            return True
        if portal_type in selectable_types:
            return True
        return False

    def get_content_tree(self):
        portal = getSite()
        actual_container = portal
        form = IFHelper.parse_deep_form(self.request.form)
        initial_container_id = self.request.form.get(
            'initial_container_id', ''
        )

        is_multi_select = form.get('multi-select', False)
        item_uids = form.get('item-uids', [])
        item_uid = form.get('item_uid', '')
        selected_list = list()
        if is_multi_select:
            if item_uids:
                selected_list = item_uids
        else:
            selected_list.append(item_uid)

        if 'select-portaltype' in form:
            selectable_types = form.get('select-portaltype', '')

        if initial_container_id and initial_container_id in portal:
            container = portal[initial_container_id]
            uid = container.UID()
            if container:
                actual_container = uuidToObject(uid)
        elif 'uid' in form:
            uid = self.request.form.get('uid', '')
            if not uid:
                actual_container = portal
            else:
                container = uuidToObject(uid)
                if container:
                    actual_container = uuidToObject(uid)
        elif item_uid:
            item = uuidToObject(item_uid)
            if item:
                actual_container = aq_parent(item)

        data = {
            'crumbs': [{
                'title': 'Home',
                'url': portal.absolute_url(),
                'uid': ''
            }],
            'content': [],
        }
        path_container = portal
        for path_id in actual_container.getPhysicalPath()[2:]:
            path_container = path_container[path_id]
            data['crumbs'].append({
                'title': path_container.title,
                'url': path_container.absolute_url(),
                'uid': path_container.UID(),
                'selectable': self._is_selectable(
                    path_container.portal_type, selectable_types
                )
            })
        #
        catalog = getToolByName(self, 'portal_catalog')
        path = '/'.join(actual_container.getPhysicalPath())
        query = {
            'path': {'query': path, 'depth': 1},
            'sort_on': 'getObjPositionInParent'
        }
        for brain in catalog(query):
            obj = brain.getObject()

            if getattr(obj, 'is_workingcopy', False):
                continue

            obj_url = obj.absolute_url()
            # for videourlct, obj_url set to be the videourl field
            # instead of the absolute url
            if IVideourlCT.providedBy(obj):
                if getattr(obj, 'videourl', ''):
                    obj_url = obj.videourl

            data['content'].append({
                'title': obj.title,
                'url': obj_url,
                'uid': obj.UID(),
                'portaltype': obj.portal_type,
                'is_container': IDexterityContainer.providedBy(obj),
                'selected': obj.UID() in selected_list,
                'selectable': self._is_selectable(
                    obj.portal_type, selectable_types
                )
            })
        return data

    def get_field(self):
        return self.request.form.get('field', '')

    @staticmethod
    def get_portaltypes():
        portal = getSite()
        portal_types = getToolByName(portal, "portal_types")
        types = portal_types.listContentTypes()
        return types

    @staticmethod
    def show_select_portaltype(select_portaltype):
        if isinstance(select_portaltype, basestring):
            return select_portaltype
        elif isinstance(select_portaltype, list):
            return ', '.join(select_portaltype)
        return None

