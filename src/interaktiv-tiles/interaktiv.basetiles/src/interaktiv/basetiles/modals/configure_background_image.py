# -*- coding: UTF-8 -*-
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from interaktiv.basetiles.vocabularies.alignments import ALIGNMENTS
from interaktiv.basetiles.vocabularies.animations import ANIMATIONS


class ConfigureBackgroundImageModalView(BrowserView):
    """ View: Background Color """

    template = ViewPageTemplateFile('templates/configure_background_image.pt')

    def __call__(self):
        return self.template()

    @staticmethod
    def get_background_image_object(obj):
        image_obj = getattr(obj, 'background_image_data', None)
        if image_obj:
            return image_obj.to_object
        return ''

    @staticmethod
    def get_alignments():
        return ALIGNMENTS

    @staticmethod
    def get_animations():
        return ANIMATIONS

