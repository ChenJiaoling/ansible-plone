from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile


class RowPaddingModalView(BrowserView):
    """ View: Select Padding Modal"""

    template = ViewPageTemplateFile('templates/row_select_padding.pt')

    def __call__(self):
        return self.template()
