# -*- coding: UTF-8 -*-
from interaktiv.basetiles.contenttypes.basetile import IBaseTile
from plone.indexer import indexer


# noinspection PyPep8Naming,PyUnusedLocal
@indexer(IBaseTile)
def SearchableTextIndexerBaseTile(obj):
    return str()
