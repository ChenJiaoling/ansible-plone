# -*- coding: UTF-8 -*-
from interaktiv.basetiles import logger
from interaktiv.basetiles.contenttypes.tilescontainerct import ITilesContainerCT
from plone.indexer import indexer


# noinspection PyPep8Naming
@indexer(ITilesContainerCT)
def SearchableTextIndexerTilesContainerCT(tilescontainer_obj):
    """ Reindexing of tiles content
    @param obj:
    """
    st = list()

    try:
        st.append(getattr(tilescontainer_obj, 'title', str()))
        # iterate over all tiles from tilescontainer
        for tile_row_id, tile_row_obj in tilescontainer_obj.items():
            st = get_st(tile_row_obj, st)
            for tile_id, tile_obj in tile_row_obj.items():
                st = get_st(tile_obj, st)
                if tile_obj.portal_type == 'TileRowCT':
                    for tile_entry_id, tile_entry_obj in tile_obj.items():
                        st = get_st(tile_entry_obj, st)

    except Exception as e:
        logger.warning(
            'Error processing SearchableText "%s".' % e,
            exc_info=True
        )
        raise
    searchable_text = ''
    for entry in st:
        if entry:
            searchable_text += ' ' + entry
    return searchable_text


def get_st(obj, searchable_text):
    if hasattr(obj, 'get_searchable_text'):
        searchable_text_from_tile = obj.get_searchable_text()
        if isinstance(searchable_text_from_tile, str):
            searchable_text_from_tile = searchable_text_from_tile \
                .decode('utf-8')
        searchable_text.append(searchable_text_from_tile)
    return searchable_text
