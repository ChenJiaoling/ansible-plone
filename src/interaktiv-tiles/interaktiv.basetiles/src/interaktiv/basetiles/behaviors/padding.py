# -*- coding: utf-8 -*-
# noinspection PyProtectedMember
from interaktiv.basetiles import _

from plone.autoform.interfaces import IFormFieldProvider
from plone.dexterity.interfaces import IDexterityContent
from plone.supermodel import model
from zope import schema
from zope.component import adapts
from zope.interface import alsoProvides
from zope.interface import implements


class IPaddingBehavior(model.Schema):
    model.fieldset(
        'label_padding_options',
        label=_(
            u"label_padding_options",
            default=u"Padding Einstellungen"
        ),
        fields=[
            'padding_top',
            'padding_left',
            'padding_right',
            'padding_bottom'
        ]
    )

    padding_top = schema.TextLine(
        title=_(
            u'label_padding_top',
            default=u'Padding Oben'
        ),
        description=u"",
        default=u"0px",
        required=False
    )

    padding_left = schema.TextLine(
        title=_(
            u'label_padding_left',
            default=u'Padding Links'
        ),
        description=u"",
        default=u"0px",
        required=False
    )

    padding_right = schema.TextLine(
        title=_(
            u'label_padding_right',
            default=u'Padding Rechts'
        ),
        description=u"",
        default=u"0px",
        required=False
    )

    padding_bottom = schema.TextLine(
        title=_(
            u'label_padding_bottom',
            default=u'Padding Unten'
        ),
        description=u"",
        default=u"0px",
        required=False
    )


alsoProvides(IPaddingBehavior, IFormFieldProvider)


class PaddingBehavior(object):
    implements(IPaddingBehavior)
    adapts(IDexterityContent)

    def __init__(self, context):
        self.context = context
