# -*- coding: utf-8 -*-
from interaktiv.basetiles import _

from plone.autoform import directives
from plone.autoform.interfaces import IFormFieldProvider
from plone.dexterity.interfaces import IDexterityContent
from plone.supermodel import model
from zope import schema
from zope.component import adapts
from zope.interface import alsoProvides
from zope.interface import implements


class IWorkingcopyBehavior(model.Schema):
    """ Behavior gives the ability
        to set the Workingcopy
    """

    model.fieldset(
        'arbeitskopie',
        label=_(u"Arbeitskopie"),
        fields=[
            'is_workingcopy',
            'has_workingcopy',
            'workingcopy_uid',
            'workingcopy_original_uid'
        ],
    )
    # directives.mode(is_workingcopy='hidden')
    is_workingcopy = schema.Bool(
        title=_(
            u'label_is_workingcopy',
            default=u'Ist eine Arbeitskopie'
        ),
        required=False
    )

    # directives.mode(has_workingcopy='hidden')
    has_workingcopy = schema.Bool(
        title=_(
            u'label_has_workingcopy',
            default=u'Besitzt bereits eine Arbeitskopie'
        ),
        required=False
    )

    # directives.mode(workingcopy_uid='hidden')
    workingcopy_uid = schema.TextLine(
        title=_(
            u'label_workingcopy_uid',
            default=u'Arbeitskopie UID'
        ),
        required=False
    )

    # directives.mode(workingcopy_original_uid='hidden')
    workingcopy_original_uid = schema.TextLine(
        title=_(
            u'label_workingcopy_original_uid',
            default=u'Arbeitskopie Original UID'
        ),
        required=False
    )


alsoProvides(IWorkingcopyBehavior, IFormFieldProvider)


class WorkingcopyBehavior(object):
    implements(IWorkingcopyBehavior)
    adapts(IDexterityContent)

    def __init__(self, context):
        self.context = context
