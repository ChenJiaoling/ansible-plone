# -*- coding: utf-8 -*-
from interaktiv.basetiles import _

from plone.autoform.interfaces import IFormFieldProvider
from plone.dexterity.interfaces import IDexterityContent
from plone.supermodel import model
from zope import schema
from zope.component import adapts
from zope.interface import alsoProvides
from zope.interface import implements
from z3c.relationfield.schema import RelationChoice
from plone.app.vocabularies.catalog import CatalogSource


class IBackgroundBehavior(model.Schema):
    model.fieldset(
        'background_options',
        label=_(
            u"label_background_options",
            default=u"Hintergrund Einstellungen"
        ),
        fields=[
            'background_color',
            'background_image_data',
            'background_image_repeat',
            'background_image_animation',
            'background_image_alignment',
            'background_video_data',
            'background_video_loop',
            'background_video_animation',
            'background_overlay_opacity',
            'background_overlay_color'
        ]
    )
    background_color = schema.Choice(
        title=_(
            u'label_background_color',
            default=u'Hintergrund Farbe'
        ),
        description=u"Hintergrund Farbe",
        source="interaktiv.basetiles.vocabularies.colors",
        required=False
    )

    background_image_data = RelationChoice(
        title=_(
            u'label_background_image',
            default=u'Hintergrund Bild'
        ),
        description=u"Hintergrund Bild für den Tile Rows.",
        source=CatalogSource(portal_type=['Image']),
        required=False
    )

    background_image_repeat = schema.Bool(
        title=_(
            u"label_background_repeat",
            default=u"Hintergrund Bild Repeat"
        ),
        description=u"Repeat für Hintergrund Bild",
        default=False,
        required=False
    )

    background_image_animation = schema.Choice(
        title=_(
            u'label_background_image_animation',
            default=u'Hintergrund Bild Animation'
        ),
        description=u"Animation für Hintergrund Bild",
        source="interaktiv.basetiles.vocabularies.animations",
        required=False,
    )

    background_image_alignment = schema.Choice(
        title=_(
            u'label_background_image_alignment',
            default=u'Hintergrund Bild Ausrichtung'
        ),
        description=u"Ausrichtung für Hintergrund Bild",
        source="interaktiv.basetiles.vocabularies.alignments",
        required=False,
    )

    background_video_data = RelationChoice(
        title=_(
            u'label_background_video',
            default=u'Hintergrund Video'
        ),
        description=u"Hintergrund Video für den Tile/TileRow.",
        source=CatalogSource(portal_type=['File', 'VideourlCT']),
        required=False
    )

    background_video_loop = schema.Bool(
        title=_(
            u"label_background_video_loop",
            default=u"Hintergrund Video Loop"
        ),
        description=u"Loop für Hintergrund Video",
        default=False,
        required=False
    )

    background_video_animation = schema.Choice(
        title=_(
            u'label_background_video_animation',
            default=u'Hintergrund Video Animation'
        ),
        description=u"Animation für Hintergrund Video",
        source="interaktiv.basetiles.vocabularies.animations",
        required=False,
    )

    background_overlay_opacity = schema.Choice(
        title=_(
            u'label_background_overlay_opacity',
            default=u'Hintergrund Bild Overlay Transparenz'
        ),
        description=u"Overlay Transparenz für Hintergrund Bild",
        source="interaktiv.basetiles.vocabularies.transparencies",
        required=False,
    )

    background_overlay_color = schema.Choice(
        title=_(
            u'label_background_overlay_color',
            default=u'Hintergrund Bild Overlay Farben'
        ),
        description=u"Overlay Farben für Hintergrund Bild",
        source="interaktiv.basetiles.vocabularies.colors",
        required=False,
    )


alsoProvides(IBackgroundBehavior, IFormFieldProvider)


class BackgroundBehavior(object):
    implements(IBackgroundBehavior)
    adapts(IDexterityContent)

    def __init__(self, context):
        self.context = context
