# -*- coding: utf-8 -*-
# noinspection PyProtectedMember
from interaktiv.basetiles import _

from plone.autoform.interfaces import IFormFieldProvider
from plone.dexterity.interfaces import IDexterityContent
from plone.supermodel import model
from zope import schema
from zope.component import adapts
from zope.interface import alsoProvides
from zope.interface import implements


class IVerticalAligmentBehavior(model.Schema):
    model.fieldset(
        'label_vertical_align_options',
        label=_(
            u"label_vertical_align_options",
            default=u"Vertikale Ausrichtungseinstellungen"
        ),
        fields=[
            'valign'
        ]
    )

    valign = schema.Choice(
        title=_(
            u'label_valign',
            default=u'Vertikale Ausrichtung'
        ),
        description=u"",
        source="interaktiv.basetiles.vocabularies.vertical_alignments",
        default="stretch",
        required=False
    )


alsoProvides(IVerticalAligmentBehavior, IFormFieldProvider)


class VerticalAligmentBehavior(object):
    implements(IVerticalAligmentBehavior)
    adapts(IDexterityContent)

    def __init__(self, context):
        self.context = context
