# -*- coding: UTF-8 -*-

# Tiles
# @TODO: Not Used, can be deleted?
ROW_ACTIONS = [
    {
        'action_id': 'move_up',
        'url': "/move_row_up",
        'data_modal': '',
        'data_modal_params': '',
        'attr_title': 'Reihenfolge ändern',
        'attr_class': 'btn btn-darkblue edit-icon '
                      'icon-suzuki-move-up move-tile move-tile-up '
                      'interaktiv-icon-basetiles-move-row-up',
    },
    {
        'action_id': 'move_down',
        'url': "/move_row_down",
        'data_modal': '',
        'data_modal_params': '',
        'attr_title': 'Reihenfolge ändern',
        'attr_class': 'btn btn-darkblue edit-icon '
                      'icon-suzuki-move-down move-tile move-tile-down '
                      'interaktiv-icon-basetiles-move-row-down',
    },
    {
        'action_id': 'remove',
        'url': "/remove_row",
        'data_modal': "/remove_content_modal",
        'data_modal_params': '{"size": "xs"}',
        'attr_title': '',
        'attr_class': 'btn btn-darkblue edit-icon '
                      'icon-suzuki-delete remove-tile '
                      'interaktiv-icon-basetiles-delete-row',
    }
]
TILE_ACTIONS = [
    {
        'action_id': 'remove',
        'url': "/remove_tile",
        'data_modal': "/remove_content_modal",
        'data_modal_size': 'xs',
        'data_modal_params': '{"tagClass": "notice"}',
        'data_modal_init': 'initializeRemoveContent',
        'attr_title': 'label_remove_tile',
        'attr_class': 'delete tool interaktiv-icon-basetilestheme-delete',
    },
    {
        'action_id': 'configure_padding',
        'url': '/tile_padding',
        'data_modal': '/tile_padding_modal',
        'data_modal_size': 'md',
        'data_modal_params': '{"id": "tile-configure-paddings"}',
        'data_modal_init': 'initializeTilePaddingModal',
        'attr_title': 'label_configure_padding',
        'attr_class': 'tool padding '
                      'interaktiv-icon-basetilestheme-option-padding',

    },
    {
        'action_id': 'configure_background_image',
        'url': "/configure_background_image_modal",
        'data_modal': "/configure_background_image_modal",
        'data_modal_size': 'lg',
        'data_modal_params': '{"tagClass": "notice"}',
        'data_modal_init': 'initializeConfigureBackgroundImage',
        'attr_title': 'label_configure_background_image',
        'attr_class': 'tool interaktiv-icon-basetilestheme-image',
    },
    {
        'action_id': 'configure_background_color',
        'url': "/configure_background_color_modal",
        'data_modal': "/configure_background_color_modal",
        'data_modal_size': 'lg',
        'data_modal_params': '{"tagClass": "notice"}',
        'data_modal_init': 'initializeConfigureBackgroundColor',
        'attr_title': 'label_configure_background_color',
        'attr_class': 'tool interaktiv-icon-basetilestheme-color-fill',
    },
    {
        'action_id': 'configure_background_overlay',
        'url': "/configure_background_overlay_modal",
        'data_modal': "/configure_background_overlay_modal",
        'data_modal_size': 'lg',
        'data_modal_params': '{"tagClass": "notice"}',
        'data_modal_init': 'initializeConfigureBackgroundOverlay',
        'attr_title': 'label_configure_background_overlay',
        'attr_class': 'tool interaktiv-icon-basetilestheme-overlay',
    },
    {
        'action_id': 'configure_vertical_align',
        'url': "/configure_vertical_align",
        'attr_title': 'label_configure_vertical_align',
        'attr_class': 'tool'
                      ' interaktiv-icon-basetilestheme-vertical-align-center',
        'options': [
            {
                'attr_class': 'interaktiv-icon-basetilestheme'
                              '-vertical-align-top',
                'attr_title': 'label_vertical_align_top',
                'value': 'top'
            },
            {
                'attr_class': 'interaktiv-icon-basetilestheme'
                              '-vertical-align-center',
                'attr_title': 'label_vertical_align_center',
                'value': 'center'
            },
            {
                'attr_class': 'interaktiv-icon-basetilestheme'
                              '-vertical-align-stretch',
                'attr_title': 'label_vertical_align_stretch',
                'value': 'stretch'
            },
            {
                'attr_class': 'interaktiv-icon-basetilestheme'
                              '-vertical-align-bottom',
                'attr_title': 'label_vertical_align_bottom',
                'value': 'bottom'
            }
        ]
    }
]
BASE_TILE_CONFIG = {
    'portal_type': 'TileCT',
    'title': 'Tile',
    'active': True,
    'tilerow_span': False,
    'initial_options': [],
    'actions': [
        'remove',
        'padding'
    ]
}

# Workingcopy
MSG_IS_WORKINGCOPY = \
    u"Sie befinden sich auf einer Arbeitskopie. " \
    u"Die Änderungen werden zwischengespeichert und sind nicht auf der " \
    u"Webseite zu sehen. Wählen Sie [ Speichern ], " \
    u"um Ihre Änderungen auf der Webseite sichtbar zu machen."
MSG_HAS_WORKINGCOPY = \
    u"Es befinden sich Änderungen auf der Seite, welche " \
    u"noch nicht sichtbar zu sehen sind. " \
    u"Wählen Sie [ Bearbeiten ], " \
    u"um Ihre Änderungen zu sehen."

# Stylguide
STYLEGUIDE_FILENAME = '_styleguide.less'
STYLEGUIDE_LIST_STYLES = [
    {'value': 'initial', 'label': 'Standard'},
    {'value': 'lower-alpha', 'label': 'Lower Alpha'},
    {'value': 'lower-roman', 'label': 'Lower Roman'},
    {'value': 'upper-alpha', 'label': 'Upper Alpha'},
    {'value': 'upper-roman', 'label': 'Upper Roman'},
    {'value': 'none', 'label': 'Kein Stil'}
]
STYLEGUIDE_TEXT_DECORATIONS = [
    {'value': 'underline', 'label': 'Unterstrichen'},
    {'value': 'none', 'label': 'Nicht Unterstrichen'},
]
STYLEGUIDE_FONT_FAMILIES = [
    {'value': 'Arial', 'label': 'Arial'},
    {'value': 'Sans-serif', 'label': 'Sans-serif'},
    {'value': 'Helvetica', 'label': 'Helvetica'},
    {'value': 'Monospace', 'label': 'Monospace'},
    {'value': 'Roboto', 'label': 'Roboto'},
]
STYLEGUIDE_FONT_WEIGHTS = [
    {'value': '300', 'label': 'Light'},
    {'value': '400', 'label': 'Normal'},
    {'value': '600', 'label': 'Bold'}
]

STYLEGUIDE_GLOBAL_VARIABLES = [
    {
        'label': 'Webseite - Schriftart',
        'css_class': 'global-font-family',
        'less_variable': 'global-font-family',
        'style_attribute': 'font-family',
        'style_value': 'Arial',
        'field_type': 'dropdown',
        'selectable': STYLEGUIDE_FONT_FAMILIES
    },
    {
        'label': 'Webseite - Schriftdicke',
        'css_class': 'global-font-weight',
        'less_variable': 'global-font-weight',
        'style_attribute': 'font-weight',
        'style_value': '400',
        'field_type': 'dropdown',
        'selectable': STYLEGUIDE_FONT_WEIGHTS
    },
    {
        'label': 'Webseite - Schriftgröße',
        'css_class': 'global-font-size',
        'less_variable': 'global-font-size',
        'style_attribute': 'font-size',
        'style_value': '1.6rem',
        'field_type': 'textline'
    },
    {
        'label': 'Webseite - Grundfarbe',
        'css_class': 'global-color',
        'less_variable': 'global-color',
        'style_attribute': 'color',
        'style_value': '#000000',
        'field_type': 'colorpicker'
    }
]
STYLEGUIDE_BUTTON_VARIABLES = [
    {
        'label': 'Button Info - Hintergrundfarbe',
        'css_class': 'btn-theme-info',
        'less_variable': 'btn-theme-info_bg-color',
        'style_attribute': 'background-color',
        'style_value': '#FFFFFF',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Button Info - Schriftfarbe',
        'css_class': 'btn-theme-info',
        'less_variable': 'btn-theme-info_color',
        'style_attribute': 'color',
        'style_value': '#333333',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Button Info - Border',
        'css_class': 'btn-theme-info',
        'less_variable': 'btn-theme-info_border',
        'style_attribute': 'border',
        'style_value': '2px solid #000000',
        'field_type': 'textline'
    },
    {
        'label': 'Button Info - Border-radius',
        'css_class': 'btn-theme-info',
        'less_variable': 'btn-theme-info_border-radius',
        'style_attribute': 'border-radius',
        'style_value': '0px',
        'field_type': 'textline'
    },
    {
        'label': 'Button Info - Padding',
        'css_class': 'btn-theme-info',
        'less_variable': 'btn-theme-info_padding',
        'style_attribute': 'padding',
        'style_value': '5px 5px 5px 5px',
        'field_type': 'textline'
    },
    {
        'label': 'Button Primary - Hintergrundfarbe',
        'css_class': 'btn-theme-primary',
        'less_variable': 'btn-theme-primary_bg-color',
        'style_attribute': 'background-color',
        'style_value': '#428bca',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Button Primary - Schriftfarbe',
        'css_class': 'btn-theme-primary',
        'less_variable': 'btn-theme-primary_color',
        'style_attribute': 'color',
        'style_value': '#FFFFFF',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Button Primary - Border',
        'css_class': 'btn-theme-primary',
        'less_variable': 'btn-theme-primary_border',
        'style_attribute': 'border',
        'style_value': '2px solid #000000',
        'field_type': 'textline'
    },
    {
        'label': 'Button Primary - Border-radius',
        'css_class': 'btn-theme-primary',
        'less_variable': 'btn-theme-primary_border-radius',
        'style_attribute': 'border-radius',
        'style_value': '0px',
        'field_type': 'textline'
    },
    {
        'label': 'Button Primary - Padding',
        'css_class': 'btn-theme-primary',
        'less_variable': 'btn-theme-primary_padding',
        'style_attribute': 'padding',
        'style_value': '5px 5px 5px 5px',
        'field_type': 'textline'
    },
    {
        'label': 'Button Success - Hintergrundfarbe',
        'css_class': 'btn-theme-success',
        'less_variable': 'btn-theme-success_bg-color',
        'style_attribute': 'background-color',
        'style_value': '#5cb85c',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Button Success - Schriftfarbe',
        'css_class': 'btn-theme-success',
        'less_variable': 'btn-theme-success_color',
        'style_attribute': 'color',
        'style_value': '#FFFFFF',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Button Success - Border',
        'css_class': 'btn-theme-success',
        'less_variable': 'btn-theme-success_border',
        'style_attribute': 'border',
        'style_value': '2px solid #000000',
        'field_type': 'textline'
    },
    {
        'label': 'Button Success - Border-radius',
        'css_class': 'btn-theme-success',
        'less_variable': 'btn-theme-success_border-radius',
        'style_attribute': 'border-radius',
        'style_value': '0px',
        'field_type': 'textline'
    },
    {
        'label': 'Button Success - Padding',
        'css_class': 'btn-theme-success',
        'less_variable': 'btn-theme-success_padding',
        'style_attribute': 'padding',
        'style_value': '5px 5px 5px 5px',
        'field_type': 'textline'
    },
    {
        'label': 'Button Alert - Hintergrundfarbe',
        'css_class': 'btn-theme-alert',
        'less_variable': 'btn-theme-alert_bg-color',
        'style_attribute': 'background-color',
        'style_value': '#f0ad4e',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Button Alert - Schriftfarbe',
        'css_class': 'btn-theme-alert',
        'less_variable': 'btn-theme-alert_color',
        'style_attribute': 'color',
        'style_value': '#FFFFFF',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Button Alert - Border',
        'css_class': 'btn-theme-alert',
        'less_variable': 'btn-theme-alert_border',
        'style_attribute': 'border',
        'style_value': '2px solid #000000',
        'field_type': 'textline'
    },
    {
        'label': 'Button Alert - Border-radius',
        'css_class': 'btn-theme-alert',
        'less_variable': 'btn-theme-alert_border-radius',
        'style_attribute': 'border-radius',
        'style_value': '0px',
        'field_type': 'textline'
    },
    {
        'label': 'Button Alert - Padding',
        'css_class': 'btn-theme-alert',
        'less_variable': 'btn-theme-alert_padding',
        'style_attribute': 'padding',
        'style_value': '5px 5px 5px 5px',
        'field_type': 'textline'
    },
    {
        'label': 'Button Error - Hintergrundfarbe',
        'css_class': 'btn-theme-error',
        'less_variable': 'btn-theme-error_bg-color',
        'style_attribute': 'background-color',
        'style_value': '#d9534f',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Button Error - Schriftfarbe',
        'css_class': 'btn-theme-error',
        'less_variable': 'btn-theme-error_color',
        'style_attribute': 'color',
        'style_value': '#FFFFFF',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Button Error - Border',
        'css_class': 'btn-theme-error',
        'less_variable': 'btn-theme-error_border',
        'style_attribute': 'border',
        'style_value': '2px solid #000000',
        'field_type': 'textline'
    },
    {
        'label': 'Button Error - Border-radius',
        'css_class': 'btn-theme-error',
        'less_variable': 'btn-theme-error_border-radius',
        'style_attribute': 'border-radius',
        'style_value': '0px',
        'field_type': 'textline'
    },
    {
        'label': 'Button Error - Padding',
        'css_class': 'btn-theme-error',
        'less_variable': 'btn-theme-error_padding',
        'style_attribute': 'padding',
        'style_value': '5px 5px 5px 5px',
        'field_type': 'textline'
    },
]
STYLEGUIDE_HEADER_VARIABLES = [
    {
        'label': 'Header 1 - Schriftgröße',
        'css_class': 'h1-theme',
        'less_variable': 'h1-theme-font-size',
        'style_attribute': 'font-size',
        'style_value': '3.2rem',
        'field_type': 'textline'
    },
    {
        'label': 'Header 1 - Schriftfarbe',
        'css_class': 'h1-theme',
        'less_variable': 'h1-theme-color',
        'style_attribute': 'color',
        'style_value': '#000000',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Header 1 - Schriftart',
        'css_class': 'h1-theme',
        'less_variable': 'h1-theme-font-family',
        'style_attribute': 'font-family',
        'style_value': 'Arial',
        'field_type': 'dropdown',
        'selectable': STYLEGUIDE_FONT_FAMILIES
    },
    {
        'label': 'Header 1 - Schriftdicke',
        'css_class': 'h1-theme',
        'less_variable': 'h1-theme-font-weight',
        'style_attribute': 'font-weight',
        'style_value': '400',
        'field_type': 'dropdown',
        'selectable': STYLEGUIDE_FONT_WEIGHTS
    },
    {
        'label': 'Header 2 - Schriftgröße',
        'css_class': 'h2-theme',
        'less_variable': 'h2-theme-font-size',
        'style_attribute': 'font-size',
        'style_value': '2.5rem',
        'field_type': 'textline'
    },
    {
        'label': 'Header 2 - Schriftfarbe',
        'css_class': 'h2-theme',
        'less_variable': 'h2-theme-color',
        'style_attribute': 'color',
        'style_value': '#000000',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Header 2 - Schriftart',
        'css_class': 'h2-theme',
        'less_variable': 'h2-theme-font-family',
        'style_attribute': 'font-family',
        'style_value': 'Arial',
        'field_type': 'dropdown',
        'selectable': STYLEGUIDE_FONT_FAMILIES
    },
    {
        'label': 'Header 2 - Schriftdicke',
        'css_class': 'h2-theme',
        'less_variable': 'h2-theme-font-weight',
        'style_attribute': 'font-weight',
        'style_value': '400',
        'field_type': 'dropdown',
        'selectable': STYLEGUIDE_FONT_WEIGHTS
    },
    {
        'label': 'Header 3 - Schriftgröße',
        'css_class': 'h3-theme',
        'less_variable': 'h3-theme-font-size',
        'style_attribute': 'font-size',
        'style_value': '1.8rem',
        'field_type': 'textline'
    },
    {
        'label': 'Header 3 - Schriftfarbe',
        'css_class': 'h3-theme',
        'less_variable': 'h3-theme-color',
        'style_attribute': 'color',
        'style_value': '#000000',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Header 3 - Schriftart',
        'css_class': 'h3-theme',
        'less_variable': 'h3-theme-font-family',
        'style_attribute': 'font-family',
        'style_value': 'Arial',
        'field_type': 'dropdown',
        'selectable': STYLEGUIDE_FONT_FAMILIES
    },
    {
        'label': 'Header 3 - Schriftdicke',
        'css_class': 'h3-theme',
        'less_variable': 'h3-theme-font-weight',
        'style_attribute': 'font-weight',
        'style_value': '400',
        'field_type': 'dropdown',
        'selectable': STYLEGUIDE_FONT_WEIGHTS
    },
    {
        'label': 'Header 4 - Schriftgröße',
        'css_class': 'h4-theme',
        'less_variable': 'h4-theme-font-size',
        'style_attribute': 'font-size',
        'style_value': '1.2rem',
        'field_type': 'textline'
    },
    {
        'label': 'Header 4 - Schriftfarbe',
        'css_class': 'h4-theme',
        'less_variable': 'h4-theme-color',
        'style_attribute': 'color',
        'style_value': '#000000',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Header 4 - Schriftart',
        'css_class': 'h4-theme',
        'less_variable': 'h4-theme-font-family',
        'style_attribute': 'font-family',
        'style_value': 'Arial',
        'field_type': 'dropdown',
        'selectable': STYLEGUIDE_FONT_FAMILIES
    },
    {
        'label': 'Header 4 - Schriftdicke',
        'css_class': 'h4-theme',
        'less_variable': 'h4-theme-font-weight',
        'style_attribute': 'font-weight',
        'style_value': '400',
        'field_type': 'dropdown',
        'selectable': STYLEGUIDE_FONT_WEIGHTS
    }
]
STYLEGUIDE_LINK_VARIABLES = [
    {
        'label': 'Link - Schriftfarbe',
        'css_class': 'a-theme',
        'less_variable': 'a-theme-color',
        'style_attribute': 'color',
        'style_value': '#000000',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Link - Schriftgröße',
        'css_class': 'a-theme',
        'less_variable': 'a-theme-font-size',
        'style_attribute': 'font-size',
        'style_value': '1rem',
        'field_type': 'textline'
    },
    {
        'label': 'Link - Textdekoration',
        'css_class': 'a-theme',
        'less_variable': 'a-theme-text-decoration',
        'style_attribute': 'text-decoration',
        'style_value': 'underline',
        'field_type': 'dropdown',
        'selectable': STYLEGUIDE_TEXT_DECORATIONS
    }
]
STYLEGUIDE_LIST_VARIABLES = [
    {
        'label': 'Ungeordnete Liste - Stil',
        'css_class': 'ul-theme',
        'less_variable': 'ul-theme-list-style-type',
        'style_attribute': 'list-style-type',
        'style_value': 'initial',
        'field_type': 'dropdown',
        'selectable': STYLEGUIDE_LIST_STYLES
    },
    {
        'label': 'Ungeordnete Liste - Schriftfarbe',
        'css_class': 'ul-li-theme',
        'less_variable': 'ul-li-theme-color',
        'style_attribute': 'color',
        'style_value': '#000000',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Ungeordnete Liste - Schriftgröße',
        'css_class': 'ul-li-theme',
        'less_variable': 'ul-li-theme-font-size',
        'style_attribute': 'font-size',
        'style_value': '1.5em',
        'field_type': 'textline'
    },
    {
        'label': 'Geordnete Liste - Stil',
        'css_class': 'ol-theme',
        'less_variable': 'ol-theme-list-style-type',
        'style_attribute': 'list-style-type',
        'style_value': 'initial',
        'field_type': 'dropdown',
        'selectable': STYLEGUIDE_LIST_STYLES
    },
    {
        'label': 'Geordnete Liste - Schriftfarbe',
        'css_class': 'ol-li-theme',
        'less_variable': 'ol-li-theme-color',
        'style_attribute': 'color',
        'style_value': '#000000',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Geordnete List - Schriftgröße',
        'css_class': 'ol-li-theme',
        'less_variable': 'ol-li-theme-font-size',
        'style_attribute': 'font-size',
        'style_value': '1.5em',
        'field_type': 'textline'
    }
]
STYLEGUIDE_TABLE_VARIABLES = [
    {
        'label': 'Tabelle - Border Style',
        'css_class': 'table-theme',
        'less_variable': 'table-border-style',
        'style_attribute': 'border-style',
        'style_value': 'solid',
        'field_type': 'textline'
    },
    {
        'label': 'Tabelle - Border Color',
        'css_class': 'table-theme',
        'less_variable': 'table-border-color',
        'style_attribute': 'border-color',
        'style_value': '#000000',
        'field_type': 'colorpicker'
    },
    {
        'label': 'Tabelle - Border Width',
        'css_class': 'table-theme',
        'less_variable': 'table-border-width',
        'style_attribute': 'border-width',
        'style_value': '1px',
        'field_type': 'textline'
    }
]

STYLEGUIDE_VARIABLES = STYLEGUIDE_GLOBAL_VARIABLES + \
                       STYLEGUIDE_BUTTON_VARIABLES + \
                       STYLEGUIDE_HEADER_VARIABLES + \
                       STYLEGUIDE_LINK_VARIABLES + \
                       STYLEGUIDE_LIST_VARIABLES + \
                       STYLEGUIDE_TABLE_VARIABLES