import os
import subprocess

from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from interaktiv.basetiles.config import STYLEGUIDE_FILENAME
from interaktiv.basetiles.config import STYLEGUIDE_GLOBAL_VARIABLES
from interaktiv.basetiles.config import STYLEGUIDE_BUTTON_VARIABLES
from interaktiv.basetiles.config import STYLEGUIDE_HEADER_VARIABLES
from interaktiv.basetiles.config import STYLEGUIDE_LINK_VARIABLES
from interaktiv.basetiles.config import STYLEGUIDE_LIST_VARIABLES
from interaktiv.basetiles.config import STYLEGUIDE_TABLE_VARIABLES
from interaktiv.basetiles.config import STYLEGUIDE_VARIABLES

BUILDOUT_DIR = os.environ.get('BUILDOUT_DIR', '')
INSTANCE_HOME = '/'.join(os.environ.get('INSTANCE_HOME', '').split('/')[:-2])
if BUILDOUT_DIR:
    PATH_TO_ROOT_DIR = BUILDOUT_DIR
else:
    PATH_TO_ROOT_DIR = INSTANCE_HOME


class TilesStyleguideView(BrowserView):
    """ View: Tiles Styleguide View """

    template = ViewPageTemplateFile('templates/tiles_styleguide.pt')
    skip_unimportant_variables = ['save-styleguide', '_authenticator']

    def __call__(self):
        form = self.request.form
        if 'save-styleguide' in form:
            self.get_styleguide('all')
            self.save_styleguide(form)
        return self.template()

    def _get_less_filepath(self):
        filepath = PATH_TO_ROOT_DIR + '/src/interaktiv-tiles/interaktiv.basetiles/src' \
                                      '/interaktiv/basetiles/static/less/'
        return filepath + STYLEGUIDE_FILENAME

    def get_styleguide_variables(self, sg_type):
        if sg_type == 'global':
            return STYLEGUIDE_GLOBAL_VARIABLES
        if sg_type == 'button':
            return STYLEGUIDE_BUTTON_VARIABLES
        if sg_type == 'header':
            return STYLEGUIDE_HEADER_VARIABLES
        if sg_type == 'link':
            return STYLEGUIDE_LINK_VARIABLES
        if sg_type == 'list':
            return STYLEGUIDE_LIST_VARIABLES
        if sg_type == 'table':
            return STYLEGUIDE_TABLE_VARIABLES
        if sg_type == 'all':
            return STYLEGUIDE_VARIABLES
        return list()

    def get_styleguide(self, sg_type):
        styleguide = self.get_styleguide_variables(sg_type)
        filepath_less_file = self._get_less_filepath()

        # create file if not exists
        if not os.path.isfile(filepath_less_file):
            open(filepath_less_file, 'a').close()

        # generate file
        with open(filepath_less_file, 'r') as less_file:
            for line in less_file:

                # get name and value
                variables = line.split(' ')
                variable_name = variables[0].replace('@', '').replace(':', '')
                variable_value_list = list()
                if len(variables) > 2:
                    for value in variables[1:len(variables)]:
                        variable_value_list.append(value.replace(';\n', ''))
                else:
                    variable_value_list.append(variables[1].replace(';\n', ''))

                for sg_variable in styleguide:
                    if variable_name == sg_variable['less_variable']:
                        sg_variable['style_value'] = \
                            ' '.join(variable_value_list)

        return styleguide

    def save_styleguide(self, form):
        less_file = open(self._get_less_filepath(), 'w')
        for variable, value in form.items():
            if variable in self.skip_unimportant_variables:
                continue
            if '|' in value:
                values = value.split('|')
                if len(values) != 2:
                    raise Exception(
                        'The Styleguide Font-Family ''is wrong configured!'
                    )
                line = '@' + variable + ': ' + values[0] + ';\n'
                line += '@' + variable + '-weight: ' + values[1] + ';\n'
            else:
                line = '@' + variable + ': ' + value + ';\n'
            less_file.write(line)
        less_file.close()
        subprocess.call(['grunt'])
        return True
