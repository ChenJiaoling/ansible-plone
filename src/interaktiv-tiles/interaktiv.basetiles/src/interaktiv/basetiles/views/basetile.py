from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from lxml import html
from plone import api
from plone.app.uuid.utils import uuidToObject
from Acquisition import aq_parent
from interaktiv.basetiles import _


class BaseView(BrowserView):

    def get_edit_mode(self):
        if not api.user.is_anonymous():
            user = api.user.get_current()
            if user:
                if getattr(self.context, 'is_workingcopy', False):
                    return True
        return False

    @staticmethod
    def translate(s):
        return _(s)


class BaseTileView(BaseView):

    template = ViewPageTemplateFile('templates/basetile.pt')
    template_edit_mode = ViewPageTemplateFile('templates/basetile.pt')

    classes = str()
    style = str()
    data = dict()
    position = 0
    is_last = False
    jsinit = str()

    def __call__(self):
        options = self.get_options()
        self._set_tile_parameters(options)

        if self.get_edit_mode():
            return self.template_edit_mode()
        return self.template()

    def get_action_conditions(self):
        parent_ids = self.context.aq_parent.objectIds()
        conditions = {
            'first': self.context.id == parent_ids[0],
            'last': self.context.id == parent_ids[-1],
        }
        return conditions

    def get_options(self):
        options = self.context.get_options()
        options['class'] += [
            'tile'
        ]

        return options

    @staticmethod
    def get_text(obj, field):
        text = getattr(obj, field, None)
        if not text:
            return str()
        text = getattr(text, 'output', None)
        if not text:
            return str()
        tree = html.fromstring(text)

        all_links = tree.xpath('//a[@data-linktype="internal" and @data-val]')

        for link in all_links:
            uuid = link.get('data-val')
            linked_obj = uuidToObject(uuid)
            if linked_obj:
                link.set('href', linked_obj.absolute_url())

        return html.tostring(tree)

    def _set_tile_parameters(self, options):
        tile_ids = aq_parent(self.context).objectIds()
        self.position = tile_ids.index(self.context.id)
        self.is_last = (self.position + 1) == len(tile_ids)

        if self.is_last:
            options['class'].append('last')

        self.classes = ' '.join(options['class'])
        self.data = options['data']

        for key in options['style']:
            self.style += key + ':' + options['style'][key] + ';'

    def get_background_image(self):
        image_options = self.context.get_background_image()
        image_style = str()
        if image_options:
            for key in image_options:
                image_style += key + ':' + image_options[key] + ';'

        return image_style

    def get_background_color(self):
        color_options = self.context.get_background_color()
        color_style = str()
        if color_options:
            for key in color_options:
                color_style += key + ':' + color_options[key] + ';'

        return color_style

    def get_background_video(self):
        video = self.context.get_background_video()
        if video:
            return video
        return str()

    def get_background_overlay_options(self):
        overlay_options = self.context.get_background_overlay_options()
        overlay_style = str()
        if overlay_options:
            for key in overlay_options:
                overlay_style += key + ':' + overlay_options[key] + ';'

        return overlay_style
