# -*- coding: UTF-8 -*-
import transaction
from Acquisition import aq_parent
from Products.CMFCore.utils import getToolByName
from Products.Five.browser import BrowserView
from plone import api
from plone.app.iterate import lock
from plone.app.layout.globals.layout import LayoutPolicy
from plone.app.lockingbehavior.behaviors import ILocking
from plone.app.uuid.utils import uuidToObject
from plone.dexterity.browser import edit
from zope.event import notify
from zope.lifecycleevent import ObjectModifiedEvent
from zope.security import checkPermission
from interaktiv.basetiles.views.basetile import BaseView
from zope.interface import alsoProvides
from plone.protect.interfaces import IDisableCSRFProtection
from interaktiv.basetiles.contenttypes.tilescontainerct import ITilesContainerCT
from Products.statusmessages.interfaces import IStatusMessage


class WorkingcopyLayoutPolicy(LayoutPolicy, BaseView):

    def bodyClass(self, template, view):
        body_class = super(WorkingcopyLayoutPolicy, self).bodyClass(
            template, view
        )

        classes_to_add = list()

        # Add edit/view mode
        if self.get_edit_mode():
            body_class += ' edit-mode'
        else:
            body_class += ' view-mode'

        if getattr(self.context, 'has_workingcopy', False):
            classes_to_add.append('has-workingcopy')
        else:
            classes_to_add.append('no-workingcopy')

        if getattr(self.context, 'is_workingcopy', False):
            classes_to_add.append('is-workingcopy')

        if checkPermission('cmf.ModifyPortalContent', self.context):
            classes_to_add.append('user-modify-content')

        body_class += ' ' + ' '.join(classes_to_add)

        return body_class


class WorkingcopyEditForm(edit.DefaultEditForm):

    def __call__(self):
        workingcopy = None
        if self.context.has_workingcopy:
            workingcopy = uuidToObject(self.context.workingcopy_uid)
        if workingcopy:
            workingcopy_edit_url = workingcopy.absolute_url() + '/edit'
            self.request.response.redirect(workingcopy_edit_url)
        else:
            return super(WorkingcopyEditForm, self).__call__()


class WorkingcopyAvailableView(BrowserView):
    """ View: Workingcopy Available View """

    def __call__(self):
        if self.context.is_workingcopy:
            return False
        if self.context.has_workingcopy:
            return False
        return True


class CreateWorkingcopyView(BrowserView):
    """ View: Create Workingcopy View """

    def __call__(self):
        workingcopy = self._create_workingcopy()
        return self.request.response.redirect(workingcopy.absolute_url())

    @staticmethod
    def _update_object(obj):
        obj.reindexObject()
        notify(ObjectModifiedEvent(obj))

    def _lock_object_recursive(self, container):
        if ILocking.providedBy(container):
            lock.lockContext(container)
        for obj in container.objectValues():
            if ILocking.providedBy(obj):
                self._lock_object_recursive(obj)

    def _create_workingcopy(self):
        parent = aq_parent(self.context)

        api.content.copy(
            self.context,
            parent,
            self.context.id + '-arbeitskopie'
        )

        workingcopy = parent[self.context.id + '-arbeitskopie']
        workingcopy.is_workingcopy = True
        workingcopy.workingcopy_uid = workingcopy.UID()
        workingcopy.workingcopy_original_uid = self.context.UID()
        #
        self.context.has_workingcopy = True
        self.context.workingcopy_uid = workingcopy.UID()
        self.context.workingcopy_original_uid = self.context.UID()
        self._lock_object_recursive(self.context)

        self._update_object(workingcopy)
        self._update_object(self.context)

        transaction.commit()
        return workingcopy


class CheckinWorkingcopyView(BrowserView):
    """ View: Checkin Workingcopy View """
    def __init__(self, context, request):
        super(CheckinWorkingcopyView, self).__init__(context, request)
        alsoProvides(request, IDisableCSRFProtection)

    def __call__(self):
        # validate tiles
        if ITilesContainerCT.providedBy(self.context) and \
           hasattr(self.context, 'validate_tilerows') and \
           callable(self.context.validate_tilerows):
            is_valid = self.context.validate_tilerows()
            if not is_valid:
                messages = IStatusMessage(self.request)
                messages.add(
                    u"Nicht alle Inhaltselemente sind befüllt. Daher kann "
                    u"diese Seite nicht gespeichert und "
                    u"veröffentlicht werden!"
                )
                return self.request.response.redirect(
                    self.context.absolute_url()
                )

        workingcopy = self._checkin_workingcopy()
        return self.request.response.redirect(workingcopy.absolute_url())

    @staticmethod
    def _update_object(obj):
        obj.reindexObject()
        notify(ObjectModifiedEvent(obj))

    def _unlock_object_recursive(self, container):
        if ILocking.providedBy(container):
            lock.unlockContext(container)
        for obj in container.objectValues():
            if ILocking.providedBy(obj):
                self._unlock_object_recursive(obj)

    def _checkin_workingcopy(self):
        if self.context.is_workingcopy:
            workingcopy = self.context
            original = uuidToObject(self.context.workingcopy_original_uid)
        else:
            workingcopy = uuidToObject(self.context.workingcopy_uid)
            original = self.context

        #
        original_id = original.id
        #
        is_default_page = False
        parent = aq_parent(original)
        if parent.getDefaultPage() == original_id:
            is_default_page = True
        #
        wf_tool = getToolByName(self.context, 'portal_workflow')
        original_review_state = wf_tool.getInfoFor(original, 'review_state')
        #
        self._unlock_object_recursive(original)
        api.content.delete(original, check_linkintegrity=False)

        api.content.rename(workingcopy, original_id)
        workingcopy.is_workingcopy = False
        workingcopy.workingcopy_uid = ''
        workingcopy.workingcopy_original_uid = ''
        if is_default_page:
            parent.setDefaultPage(original_id)
        if original_review_state == 'published':
            wf_tool.doActionFor(workingcopy, 'publish')
        #
        self._update_object(workingcopy)

        transaction.commit()
        return workingcopy


class DiscardWorkingcopyView(BrowserView):
    """ View: Discard Workingcopy View """

    def __init__(self, context, request):
        super(DiscardWorkingcopyView, self).__init__(context, request)
        alsoProvides(request, IDisableCSRFProtection)

    @staticmethod
    def _update_object(obj):
        obj.reindexObject()
        notify(ObjectModifiedEvent(obj))

    def __call__(self):
        # get objects
        if self.context.is_workingcopy:
            workingcopy = self.context
            original = uuidToObject(self.context.workingcopy_original_uid)
        else:
            workingcopy = uuidToObject(self.context.workingcopy_uid)
            original = self.context

        # remove workingcopy
        if workingcopy:
            api.content.delete(workingcopy, check_linkintegrity=False)

        # regress the original
        original.has_workingcopy = False
        original.workingcopy_uid = ''
        original.workingcopy_original_uid = ''
        self._update_object(original)

        # redirect to original object
        original_url = original.absolute_url()
        return self.request.response.redirect(original_url)
