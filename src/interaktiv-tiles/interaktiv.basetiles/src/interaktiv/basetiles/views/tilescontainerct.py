from Products.CMFCore.utils import getToolByName
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from interaktiv.basetiles.contenttypes.tilerowct import ITileRowCT
from interaktiv.basetiles.views.basetile import BaseView
from Products.statusmessages.interfaces import IStatusMessage
from interaktiv.basetiles.config import MSG_IS_WORKINGCOPY
from interaktiv.basetiles.config import MSG_HAS_WORKINGCOPY
from plone.app.uuid.utils import uuidToObject
from zope.security import checkPermission


class TilesContainerCTView(BaseView):
    """ View: TilesContainerCT """

    template = ViewPageTemplateFile("templates/tilescontainerct.pt")

    def __call__(self):
        messages = IStatusMessage(self.request)
        if checkPermission('cmf.ModifyPortalContent', self.context):
            if getattr(self.context, 'is_workingcopy', False):
                messages.add(MSG_IS_WORKINGCOPY)
            elif getattr(self.context, 'has_workingcopy', False):
                messages.add(MSG_HAS_WORKINGCOPY)
        return self.template()

    def get_tile_rows(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        path = '/'.join(self.context.getPhysicalPath())
        query = {
            'path': {'query': path, 'depth': 1},
            'object_provides': ITileRowCT.__identifier__,
            'sort_on': 'getObjPositionInParent',
        }
        tilerow_objects = list()
        for brain in catalog(query):
            tilerow_object = brain.getObject()
            tilerow_objects.append(tilerow_object)
        return tilerow_objects

    def get_original_url(self):
        original_uid = getattr(self.context, 'workingcopy_original_uid', '')
        if not original_uid:
            return str()
        original = uuidToObject(self.context.workingcopy_original_uid)
        return original.absolute_url()
