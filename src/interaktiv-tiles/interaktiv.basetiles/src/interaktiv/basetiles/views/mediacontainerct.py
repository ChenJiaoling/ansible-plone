from Products.CMFCore.utils import getToolByName
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone import api
from zope.site.hooks import getSite


class MediaContainerCTView(BrowserView):
    """ View: MediaContainerCT """

    template = ViewPageTemplateFile("templates/mediacontainerct.pt")

    def __call__(self):
        if self.is_view_allowed():
            return self.template()
        portal = getSite()
        self.request.response.redirect(portal.absolute_url())

    def is_view_allowed(self):
        if api.user.is_anonymous():
            return False
        permissions = api.user.get_permissions()
        return permissions["Modify portal content"]

    def get_media_container_content(self):
        portal = getSite()
        path = "/".join(self.context.getPhysicalPath())
        query = {
            "path": {'query': path, 'depth': 1},
            "portal_type": ["ImageContainerCT", "File", "MediaContainerCT"]
        }
        catalog = getToolByName(portal, "portal_catalog")
        content = list()
        for brain in catalog(query):
            obj = brain.getObject()
            content.append({
                "id": obj.id,
                "uid": obj.UID(),
                "title": obj.title,
                "url": obj.absolute_url(),
                "obj": obj,
                "portal_type": obj.portal_type
            })
        return content
