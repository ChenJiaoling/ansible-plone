from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone import api
from zope.site.hooks import getSite
from plone.protect.interfaces import IDisableCSRFProtection
from zope.interface import alsoProvides
from plone.app.multilingual.interfaces import ITranslationManager
from Products.CMFPlone.interfaces import ILanguage


class CreateTranslationView(BrowserView):
    """ View: TilesContainerCT """

    template = ViewPageTemplateFile("templates/create_translation.pt")

    def __call__(self):
        alsoProvides(self.request, IDisableCSRFProtection)
        portal = getSite()

        # get language container
        form = self.request.form
        language = form.get('language', '')
        language_container = None
        if language and language in portal:
            language_container = portal[language]

        if language_container:
            # copy current language to language folder
            new_language_obj = api.content.copy(
                source=self.context,
                target=language_container,
                id=self.context.id + '-uebersetzung-en'
            )

            # set relation
            ITranslationManager(self.context).register_translation(
                language,
                new_language_obj
            )
            ILanguage(new_language_obj).set_language(language)

            # redirect to new language object in edit mode
            if new_language_obj:
                self.request.response.redirect(
                    new_language_obj.absolute_url() + '?edit-mode=on'
                )
        else:
            raise Exception(
                'Language Container "' + str(language) + '" does not exist!'
            )
