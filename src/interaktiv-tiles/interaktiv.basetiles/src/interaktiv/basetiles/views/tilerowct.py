from Acquisition import aq_parent
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from interaktiv.basetiles.views.basetile import BaseView
from interaktiv.basetiles.vocabularies.tilerow_span import TILEROW_SPAN


class TileRowView(BaseView):

    template = ViewPageTemplateFile('templates/tilerowct.pt')
    template_edit_mode = ViewPageTemplateFile('templates/tilerowct_edit.pt')

    is_last = False
    position = 0
    tilerow_spans = TILEROW_SPAN

    classes = str()
    style = str()
    data = dict()

    def __call__(self):
        self._set_row_parameters()
        if self.get_edit_mode():
            return self.template_edit_mode()
        return self.template()

    def _set_row_parameters(self):
        tile_ids = self.context.objectIds()
        if not tile_ids:
            self.request['grid_width'] = '12'
        else:
            self.request['grid_width'] = str(12 / len(tile_ids))

        tilerow_ids = aq_parent(self.context).objectIds()
        self.position = tilerow_ids.index(self.context.id)
        self.is_last = (self.position + 1) == len(tilerow_ids)
        options = self.context.get_options()

        # you can manipulate the options here
        if self.is_last:
            options['class'].append('last')

        options['class'] += ['row', 'tile-row']

        # set class for columns
        options['class'].append('columns-' + str(len(tile_ids)))

        # Tilerow span class
        tilerow_span = getattr(self.context, 'tilerow_span', 'dynamic_content')
        if tilerow_span == 'full':
            options['class'].append('tile-row-full-width')
        elif tilerow_span == 'dynamic_row':
            options['class'].append('tile-row-dynamic-width')
        else:
            options['class'].append('tile-row-dynamic-content-width')

        self.classes = ' '.join(options['class'])
        self.data = options['data']

        for key in options['style']:
            self.style += key + ':' + options['style'][key] + ';'

    def get_tiles(self):
        return self.context.objectValues()

    def get_background_image(self):
        image_options = self.context.get_background_image()
        image_style = str()
        if image_options:
            for key in image_options:
                image_style += key + ':' + image_options[key] + ';'

        return image_style

    def get_background_color(self):
        color_options = self.context.get_background_color()
        color_style = str()
        if color_options:
            for key in color_options:
                color_style += key + ':' + color_options[key] + ';'

        return color_style

    def get_background_video(self):
        video = self.context.get_background_video()
        if video:
            return video
        return str()

    def get_background_overlay_options(self):
        overlay_options = self.context.get_background_overlay_options()
        overlay_style = str()
        if overlay_options:
            for key in overlay_options:
                overlay_style += key + ':' + overlay_options[key] + ';'

        return overlay_style



