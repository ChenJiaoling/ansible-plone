//noinspection JSUnusedGlobalSymbols
function initializeRemoveBackgroundOptions(modal) {
  // Click Remove Button
  var base_url = modal.$modal.find('.interaktivbasetiles-modal-header').data('base_url');
  var row_uid = modal.$modal.find('.interaktivbasetiles-modal-header').data('uid');

  modal.$modal.find('[data-action="submit"]').on('click', function(){
    modal.submitForm(base_url+'/remove_background_options_modal', 'html', function(success){
      if(success){
        refreshTiles(row_uid);
        setTimeout(function(){
          modal.close();
        }, 600);
      }
    });
  });
}
