function initializeConfigureBackgroundColor(modal) {

    // init the colors
    var $colors = $('.color-item');
    var $current_color = $('.interaktivbasetiles-modal-header').data('current_color');


    $colors.on('click', function () {
        $current_color = $(this).data('bg-color');
        $colors.removeClass("selected");
        $(this).addClass("selected");
        $('input[name=background_color]').val($(this).data('bg-color'));
    });


    modal.$modal.on('click', '.btn-ok', function () {
        var base_url = $('.interaktivbasetiles-modal-header').data('base_url');
        modal.submitForm(base_url + '/update_content', 'json', function (success, response) {
          if (success) {
            refreshTiles(response.uid, response.render);
            modal.close();
          }
        });
    });
}

