function initializeConfigureBackgroundOverlay(modal) {

    // Pick ovelay color
    var $colors = $('.color-item');
    $colors.on('click', function () {
        $colors.removeClass("selected");
        $(this).addClass("selected");
        $('input[name=background_overlay_color]').val($(this).data('bg-overlay-color'));
    });

    // CLick Ok button
    modal.$modal.on('click', '.btn-ok', function () {
        var base_url = $('.interaktivbasetiles-modal-header').data('base_url');

        modal.submitForm(base_url + '/update_content', 'json', function (success, response) {
          if (success) {
            refreshTiles(response.uid, response.render);
            modal.close();
          }
        });
    });
}

