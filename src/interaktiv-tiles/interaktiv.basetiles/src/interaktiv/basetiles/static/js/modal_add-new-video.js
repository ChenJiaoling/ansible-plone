function initializeAddNewVideo(modal, ConfigureBackgroundVideoModal) {
    // Select one video data type
    modal.$modal.on('change', 'select#select-video-type', function () {
        var selected_value = $(this).val();
        if (selected_value == 'file') {
            $('.video-file-section').removeClass('hidden');
            $('.video-youtube-section').addClass('hidden');
        }
        else if (selected_value == 'url') {
            $('.video-youtube-section').removeClass('hidden');
            $('.video-file-section').addClass('hidden');
        }
        else {
            $('.video-file-section').addClass('hidden');
            $('.video-youtube-section').addClass('hidden');
        }
    });
    // CLick Ok button
    modal.$modal.on('click', '.btn-ok', function () {
        var portal_url = $('body').data('portal-url');
        var is_valid = validateFields();
        if (!is_valid) {
            modal.showErrorInline('Bitte überprüfen Sie Ihre Angaben.');
        }
        else {
            modal.submitForm(portal_url + "/@@add_new_video_modal", 'html', function (success) {
                if(!success) return;

                var new_video_data = modal.$modal.find('#new-video-data').data('new_video_data');
                var errormessage = modal.$modal.find('#new-video-data').data('errormessage');
                if(!new_video_data && errormessage) {
                    modal.showErrorInline(errormessage);
                    return;
                }

                var video_uid = new_video_data['uid'];
                if (!video_uid) {
                    modal.showErrorInline('Video UID konnte nicht gefunden werden');
                    return;
                }

                // update uid in hidden input field
                ConfigureBackgroundVideoModal.$modal.find('#background-video-data').val(video_uid);
                // update title
                var $titlefield = ConfigureBackgroundVideoModal.$modal.find('#select-video-button');
                var video_title = new_video_data['title'];
                if (video_title && $titlefield) {
                    $titlefield.html(video_title);
                }
                // update preview
                var videourl = new_video_data['videourl'];
                var portaltype = new_video_data['portaltype'];
                if (videourl && portaltype){
                    var $previewfield = ConfigureBackgroundVideoModal.$modal.find('#video-preview');
                    if (portaltype == 'File') {
                        setVideoPreview($previewfield, videourl);
                    }
                    else if (portaltype == 'VideourlCT') {
                        setVideoIframePreview($previewfield, videourl);
                    }
                }
                modal.close();
            });
        }
    });
}

function validateFields() {
    var video_title = $('input[name=video_title]').val();
    var select_video_file = $('input[name=select_video_file]').val();
    var youtube_url = $('input[name=youtube_url]').val();
    var video_container_uid = $('input[name=video_container_uid]').val();
    if (video_title && video_container_uid && (select_video_file || youtube_url)) {
        return true;
    }
    else {
        return false;
    }
}


