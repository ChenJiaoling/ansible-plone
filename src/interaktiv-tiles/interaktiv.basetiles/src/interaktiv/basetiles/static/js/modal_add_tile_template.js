function initializeAddTileTemplateModal(modal) {
  // Click Add Tile Button
  modal.$modal.on('click', '#btn-add-template', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var base_url = $('body').data('base-url');
    $.ajax({
      url    : base_url + '/add_tile_template_api',
      data   : {template_name: $('input[name="template_name"]').val()},
      success: function (response) {
        location.reload();
      },
      error  : function () {
        console.log('Error adding template:', arguments);
      }
    });
  });
}
