//noinspection JSUnusedGlobalSymbols
function initializeRowPadding(modal) {
  var base_url = modal.$modal.find('.interaktivbasetiles-modal-header')
                      .data('base_url');
  // Click ok button

  modal.$modal.on('click', '.btn-ok', function (e) {
    e.preventDefault();
    e.stopPropagation();

    modal.submitForm(base_url + '/update_content', 'json', function (success, response) {
      refreshTiles(response.uid, response.render);
    });
  });
}