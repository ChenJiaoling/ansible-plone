var initializeSelectContent = function (modal) {
  // Select content type and refresh the modal
  // (select element will not appear when single select portaltype is given)
  modal.$modal.on('change', 'select#portaltype', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var new_portaltype = $('select#portaltype').val();
    if (new_portaltype) {
      var requestParams = modal.request;
      if (requestParams['select-portaltype']) {
        requestParams['select-portaltype'].push(new_portaltype);
      }
    }
    var url = $('div.interaktivbasetiles-modal-header').data('base_url') + '/select_content_modal';
    modal.openUrl(url, requestParams);

  });
  // Select container and switch to its view
  modal.$modal.on('click', '.select-container', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var requestParams = modal.request;
    requestParams.uid = $(this).data('uid');
    var url = $('div.interaktivbasetiles-modal-header').data('base_url') + '/select_content_modal';
    modal.openUrl(url, requestParams);
  });

  // Select a Reference
  modal.$modal.on('click', '.chooser', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var select_portaltype = modal.request['select-portaltype'];
    if ("multi-select" in modal.request) {
      // Multi select
      if ($(this).hasClass('selected')) {
        $(this).removeClass('selected');
      }
      else {
        if (select_portaltype) {
          var clicked_portaltype = $(this).data('portaltype');

          if ($.inArray(clicked_portaltype, select_portaltype) >= 0) {
            $(this).addClass('selected');
          }
        }
        else {
          $(this).addClass('selected');
        }
      }
    }
    else {
      // Single select
      var setSelected = true;
      if ($(this).hasClass('unselectable')) {
        if ($(this).hasClass('selected')) {
          setSelected = false;
        }
      }
      $(this).closest('.content-tree-items').find('.chooser').each(function () {
        $(this).removeClass('selected');
      });
      if (setSelected) {
        if (select_portaltype) {
          // check if its one or multiple types
          if (select_portaltype instanceof Array) {
            if ( $.inArray($(this).data('portaltype'), select_portaltype) >= 0) {
              $(this).addClass('selected');
            }
          }
          else if (typeof(select_portaltype) == 'string') {
            if (select_portaltype == $(this).data('portaltype')) {
              $(this).addClass('selected');
            }
          }
        }
        else {
          $(this).addClass('selected');
        }
      }
    }
  });

  // Button "Speichern"
  modal.$modal.on('click', '.interaktivtilesmodal-select-content', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var field = modal.$modal.find('div.interaktivbasetiles-modal-header').data('field');
    var requestParams = modal.request;
    if ("multi-select" in requestParams) {
      var ul = $(requestParams['select-selector'] || '.references-list');
      modal.$modal.find('.content-tree-items .chooser.selected').each(function () {
        var uid = $(this).data("uid");
        $('<li>' +
          '<input type="checkbox" ' +
          '       checked="checked" ' +
          '       name="' + field + '" ' +
          '       class="field-input multi-reference" ' +
          '       value="' + uid + '"' +
          '       id="' + uid + '"/> ' +
          '<label class="title" for="' + uid + '">' + $(this).data('title') + '</label>' +
          '</li>'
        ).appendTo(ul);
      });
    }
    else {
      var $chooser = $('.content-tree-items .chooser.selected');
      var item_uid = $chooser.data('uid') || '';
      var item_url = $chooser.data('url') || '';
      var item_title = $chooser.data('title') || '';
      var item_portaltype = $chooser.data('portaltype') || '';

      $(requestParams['select-selector'] || '.field-input[name="' + field + '"]').val(item_uid);

      // set title to the selector element
      if('select-titlefield' in requestParams) {
        $(requestParams['select-titlefield']).html(item_title);
      }
      // set selected item uid to selector element
      if('select-uidfield' in requestParams) {
        $(requestParams['select-uidfield']).val(item_uid);
      }
      // set image/video to the selector element
      var select_mediafield = requestParams['select-mediafield'];
      var $mediafield = $(select_mediafield);

      if (!item_url) {
        $mediafield.html('');
        if('select-titlefield' in requestParams) {
          $(requestParams['select-titlefield']).html('Bild auswählen');
        }
      }
      else if (select_mediafield) {
        if (item_portaltype == 'Image') {
            setImagePreview($mediafield, item_url);
        }
        else if (item_portaltype == 'File') {
            setVideoPreview($mediafield, item_url);
        }
        else if (item_portaltype == 'VideourlCT') {
            setVideoIframePreview($mediafield, item_url);
        }
      }

      if (requestParams['select-portaltype'] == 'Folder') {
        initializeSelectFolder(modal.$modal);
      }
    }

    modal.close();
  });
};

function setImagePreview(mediafield,item_url) {
  if (mediafield.find('img').length == 0 ){
    mediafield.html('<img src="' + item_url + '/@@images/image/thumb">');
  }
  else {
    mediafield.find('img').attr('src', item_url + '/@@images/image/thumb');
  }
}

function setVideoPreview(mediafield,item_url) {
  if (mediafield.find('video').length == 0 ){
    mediafield.html('<video muted autoplay> + <source type="video/mp4" src="' + item_url + '">No Video Supported.</video>');
  }
  else {
    mediafield.find('video').attr('src', item_url);
  }
}

function setVideoIframePreview(mediafield,item_url) {
  var embed_url = item_url.replace('watch?v=', 'embed/') + '?rel=0&autoplay=1&controls=0&mute=1&iv_load_policy=3&showinfo=0';
  if (mediafield.find('iframe').length == 0 ){
    mediafield.html('<iframe frameborder="0" src="' + embed_url +'"></iframe><div class="iframe-overlay"></div>');
  }
  else {
    mediafield.find('iframe').attr('src', embed_url);
  }
}