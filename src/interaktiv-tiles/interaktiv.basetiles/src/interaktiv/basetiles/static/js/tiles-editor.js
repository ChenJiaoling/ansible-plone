$(document).ready(function () {
  var $body = $('body');

  if (!$body.hasClass('portaltype-tilescontainerct')) return;
  if (!$body.hasClass('user-modify-content')) return;

  var $tilesEditorPanel = $('#tiles-editor-panel');
  var $tilesContainer   = $('.tiles-container');
  var $menuPanel        = $('#menu-panel');

  // Redirect /edit on tilescontainerct to arbeitskopie
  $('#contentview-edit').on('click', function (e) {
    var base_url = $('.portaltype-tilescontainerct').data('base-url');
    if ($body.hasClass('is-workingcopy')) {
      e.preventDefault();
      window.location.replace(base_url + '/edit');
    } else if ($body.hasClass('has-workingcopy')) {
      e.preventDefault();
      window.location.replace(base_url + '-arbeitskopie');
    } else if ($body.hasClass('no-workingcopy')) {
      e.preventDefault();
      window.location.replace(base_url + '/@@create_workingcopy');
    }
  });

  // Redirect to Create Tiles Translation View
  $('.contentmenuflags').on('click', function (e) {
    var new_url;
    var id          = $(this).attr("id");
    var old_url     = $(this).attr("href");
    var current_url = window.location.href;
    if (current_url.indexOf("?") > -1) {
      current_url = current_url.substr(0, current_url.indexOf("?"));
    }

    if (~id.indexOf("translate_into_")) {
      e.preventDefault();
      new_url = current_url + '/@@create_tiles_translation?language=';
      new_url += old_url.substr(old_url.length - 2);
      window.location.replace(new_url);
    }
    if (~id.indexOf("babel_edit")) {
      e.preventDefault();
      new_url = old_url.replace("/babel_edit", "");
      new_url = new_url + '?edit_mode=on';
      window.location.replace(new_url);
    }
  });

  // Opening of tiles-editor-panel
  $('#contentview-tiles-editor').on('click', function (e) {
    e.preventDefault();
    $tilesEditorPanel.toggleClass('slide');
  });

  $('.template-load').on('click', function (e) {
    e.preventDefault();
    var base_url     = $('body').data('base-url');
    var template_uid = $(this).data('template_uid');
    $.ajax({
      url    : base_url + '/load_tile_template_api',
      data   : {template_uid: template_uid},
      success: function () {
        location.reload();
      },
      error  : function () {
        console.log('Error adding template', arguments);
      }
    });
  });

  $menuPanel.on('click', 'li[data-menu-type]', function (e) {
    e.preventDefault();
    var menupoint = $(this).data('menu-type');
    $menuPanel.addClass('slide-left').removeClass('slide-right');
    $tilesEditorPanel
      .children('[data-menu-type="' + menupoint + '"]')
      .addClass('slide-left')
      .removeClass('slide-right');
  });

  $('.return-to-main-panel').on('click', function (e) {
    e.preventDefault();
    $menuPanel.addClass('slide-right').removeClass('slide-left');
    $tilesEditorPanel
      .children('[data-menu-type]:not(#menu-panel)')
      .addClass('slide-right')
      .removeClass('slide-left');
  });

  // moving/sorting of tiles
  $tilesContainer.sortable({
    items : '> .tile-row',
    cursor: 'move',
    handle: '.row-mover',
    axis  : "y",
    update: function (event, ui) {
      updateTilePositions();
      var row_url      = ui.item.data('url');
      var $container   = $(event.target);
      var new_position = ui.item.index() - 1;

      $container.sortable('disable');

      $.ajax({
        url     : row_url + '/move_content',
        type    : 'GET',
        data    : {
          position: new_position
        },
        error   : function () {
          $container.sortable('cancel');
          ui.item.css({
            zIndex  : '',
            position: ''
          });
          updateTilePositions();
        },
        complete: function () {
          $container.sortable('enable');
          ui.item.css({
            zIndex  : '',
            position: ''
          });
        }
      });
    }
  });

  // Drag'n'Drop to add new Tiles
  $tilesEditorPanel.find('li[data-portaltype]').draggable({
    cursor          : 'crosshair',
    opacity         : 0.5,
    revert          : true,
    scope           : 'addable-tiles',
    helper          : function (event) {
      var $target = $(event.currentTarget);
      var $image  = $target.find('img').clone();
      $image.data('portaltype', $target.data('portaltype'));
      $image.css('z-index', 1000);
      return $image;
    },
    appendTo        : $tilesContainer,
    containment     : $tilesContainer,
    refreshPositions: true,
    drag            : function () {
      $tilesEditorPanel.removeClass('slide');
      $tilesContainer.addClass('drag-to-add');
      $tilesContainer
        .find('.tile-row')
        .on('mouseover', function () {
          $tilesContainer.find('.tile-row').removeClass('hovered');
          $(this).addClass('hovered');
        })
        .on('mouseleave', function () {
          $(this).removeClass('hovered');
        });
    },
    stop            : function () {
      $tilesContainer.removeClass('drag-to-add');
      $tilesContainer
        .find('.tile-row')
        .off('mouseover mouseleave');
    }
  });

  tilesContainerAssignDroppableEvent($tilesContainer);

  // tilerow options
  if (!$body.hasClass('is-workingcopy')) return;
  $tilesContainer.on('click', '[data-row-action="row_span"] [data-value]', function () {
    var $action    = $(this).closest('[data-row-action]');
    var update_url = $action.data('update_url');
    var value      = $(this).data('value');

    $.ajax({
      url    : update_url,
      method : 'POST',
      data   : {
        tilerow_span: value
      },
      success: function (response) {
        refreshTiles(response.uid);
      }
    });
  });

  $tilesContainer.on('click', '.tile-row-tools a, .tile-tools a', function (e) {
    e.preventDefault();
  });

  $tilesContainer.on('click', '[data-action="configure_vertical_align"] li > a', function (e) {
    e.preventDefault();
    var $tile    = $(this).closest('.tile');
    var tile_url = $tile.data('url');

    $.ajax({
      url    : tile_url + '/update_content',
      data   : {
        'valign': $(this).data('value')
      },
      method : 'GET',
      success: function (response, statusText, xhr) {
        if (xhr.status == 200) {
          $tile.replaceWith(response.render);
        }
      },
      error  : function () {
        console.log('valign settings failed');
      }
    })
  });

});

function tilesContainerAssignDroppableEvent($tilesContainer) {
  $tilesContainer.find('.droparea').droppable({
    scope    : 'addable-tiles',
    accept   : '[data-portaltype]',
    tolerance: 'pointer',
    drop     : function (event, ui) {
      var $droparea  = $(event.target);
      var url        = $tilesContainer.data('url');
      var portaltype = ui.helper.data('portaltype');
      var $tilerow   = $droparea.closest('.tile-row');
      var position   = ($tilerow.length) ? $tilerow.index() + 1 : 0;
      var replaceRow = false;

      // Add tile inside Row
      if ($(event.target).closest('.tiles-wrapper').length) {
        url        = $tilerow.data('url');
        var $tile  = $droparea.closest('.tile');
        replaceRow = true;
        position   = $tile.index();
        position   = ($droparea.hasClass('after')) ? position + 1 : position;
      }

      $.ajax({
        url    : url + '/add_tile',
        type   : 'GET',
        data   : {
          position   : position,
          portal_type: portaltype
        },
        success: function (response) {
          if (replaceRow) $tilerow.replaceWith(response.render);
          else $tilesContainer.find('.tile-rows-wrapper')
                              .appendAt(position, response.render);
          updateTilePositions();
          tilesContainerAssignDroppableEvent($tilesContainer);
        },
        error  : function (xhr, status, statusText) {
          var errorModal = new ModalClass('error');
          errorModal.showError(statusText, '', function () {
            errorModal.close();
          });
        }
      });
    }
  });
}

function updateTilePositions() {
  var $tileRows = $('div.tiles-container > div.tile-row');
  $tileRows.removeClass('last');
  $tileRows.each(function (index) {
    $(this).attr('data-position', index).data('position', index);
    if (index + 1 == $tileRows.length) {
      $(this).addClass('last');
    }

    var $tiles = $(this).children('.tile');
    $tiles.each(function (index) {
      $(this).attr('data-position', index).data('position', index);
    });
  });
}

/**
 * Update tilerow or tile in Editor
 * @param {string} tiles_id
 * @param {function|string=} replaceHtml - can be omitted and provided with callback function instead
 * @param {function=} callback
 */
function refreshTiles(tiles_id, replaceHtml, callback) {
  callback = callback || noCallback;
  if (typeof replaceHtml == 'function') {
    //noinspection JSValidateTypes
    callback    = replaceHtml;
    replaceHtml = '';
  }
  //noinspection JSJQueryEfficiency
  var $tiles = $('#' + tiles_id);
  var url      = $tiles.data('url');

  if (replaceHtml) {
    $tiles.replaceWith(replaceHtml);
    $tiles = $('#' + tiles_id);
    // check if the element is a tilerow or a tile
    if ($tiles.find('.tile-row').length > 0) {
      // at tilerow
      $tiles.find('.tile').each(function () {
        var $tile = $(this);
        var tileJSFunction = $tile.data('jsinit');
        if (tileJSFunction && tileJSFunction in window && typeof window[tileJSFunction] == 'function') {
          window[tileJSFunction]($tile);
        }
      });
    }
    else {
      //at single tile
      var tileJSFunction = $tiles.data('jsinit');
      if (tileJSFunction && tileJSFunction in window && typeof window[tileJSFunction] == 'function') {
        window[tileJSFunction]($tiles);
      }
    }
    callback(true);
  }
  else {
    $.ajax({
      url    : url,
      method : 'GET',
      success: function (data, status, xhr) {
        if (xhr.status == 200) {
          $tiles.replaceWith(data);
          $tiles = $('#' + tiles_id);

          if ($tiles.find('.tile-row').length > 0) {
            // tilerow
            tilesContainerAssignDroppableEvent($tiles.closest('.tiles-container'));
            $tiles.find('.tile').each(function () {
              var $tile = $(this);
              var tileJSFunction = $tile.data('jsinit');
              if (tileJSFunction && tileJSFunction in window && typeof window[tileJSFunction] == 'function') {
                window[tileJSFunction]($tile);
              }
            });
          }
          else {
            //tile
            var tileJSFunction = $tiles.data('jsinit');
            if (tileJSFunction && tileJSFunction in window && typeof window[tileJSFunction] == 'function') {
              window[tileJSFunction]($tiles);
            }
          }
          callback(true);
        }
        else {
          callback(false);
        }
      },
      error  : function () {
        callback(false);
      }
    })
  }

}