//noinspection JSUnusedGlobalSymbols
function initializeTilePaddingModal(modal){
  var base_url = modal.$modal.find('.interaktivbasetiles-modal-header').data('base_url');
  var tile_uid = modal.$modal.find('.interaktivbasetiles-modal-header').data('uid');

  modal.$modal.find('[data-action="submit"]').click(function(){
    modal.submitForm(base_url+'/tile_padding_modal', 'html', function(success){
      if(success){
        refreshTiles(tile_uid);
        setTimeout(function(){
          modal.close();
        }, 600);
      }
    });
  });
}