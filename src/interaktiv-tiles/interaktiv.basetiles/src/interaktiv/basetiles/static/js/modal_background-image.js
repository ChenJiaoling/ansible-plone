//noinspection JSUnusedGlobalSymbols
function initializeConfigureBackgroundImage(modal) {
  // CLick Ok button
  modal.$modal.on('click', '.btn-ok', function () {
    var base_url = $('.interaktivbasetiles-modal-header').data('base_url');

    modal.submitForm(base_url + '/update_content', 'json', function (success, response) {
      if (success) {
        refreshTiles(response.uid, response.render);
        modal.close();
      }
    });
  });
}
