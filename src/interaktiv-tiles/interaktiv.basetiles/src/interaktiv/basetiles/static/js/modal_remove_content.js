//noinspection JSUnusedGlobalSymbols
function initializeRemoveContent(modal) {
  // Click Remove Button
  modal.$modal.on('click', '.btn-remove', function (e) {
    e.preventDefault();
    e.stopPropagation();

    var base_url = $('.interaktivbasetiles-modal-header').data('base_url');
    $.ajax({
      url    : base_url + '/remove_content',
      success: function (response) {
        modal.close();
        var $element  = $('#' + response.uid);
        var isTileRow = ($element.hasClass('tile-row'));
        var $tileRow  = ($element.hasClass('tile-row')) ? $element : $element.closest('.tile-row');

        if (isTileRow) {
          $tileRow.remove();
        }
        else {
          refreshTiles($tileRow.attr('id'));
        }
      },
      error  : function () {
        console.log('Error removing tile', arguments);
      }
    });

  });
}
