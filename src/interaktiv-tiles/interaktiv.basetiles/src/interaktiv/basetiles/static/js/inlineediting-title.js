// Inline Editing Header with api call "update_title"
// @TODO: REMOVE time based init function!!!!
$(window).load(function () {
  setTimeout(function () {
    if ($('body').hasClass('view-mode')) return;

    // Initialize h1-Editing after 200ms
    tinymce.init({
      selector: 'body.userrole-authenticated h1.documentFirstHeading',
      inline  : true,
      toolbar : 'undo redo',
      menubar : false,
      setup   : function (ed) {
        ed.on('change', function () {
          $.ajax({
            url        : window.location.pathname + '/update_content',
            data       : {
              title: ed.getContent()
            },
            type       : 'GET',
            dataType   : 'json',
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            success    : function () {
              console.log("Changed title to: " + ed.getContent());
            },
            error      : function (XMLHttpRequest, textStatus, errorThrown) {
              console.log("Status: " + textStatus + " | " + "Error: " + errorThrown);
            }
          });
        });
      }
    })

  }, 200)
});