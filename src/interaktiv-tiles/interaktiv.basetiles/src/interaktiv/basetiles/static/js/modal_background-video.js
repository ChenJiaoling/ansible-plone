//noinspection JSUnusedGlobalSymbols
function initializeConfigureBackgroundVideo(modal) {
  var base_url = $('.interaktivbasetiles-modal-header').data('base_url');
  // CLick Ok button
  modal.$modal.on('click', '.btn-ok', function () {
    var base_url = $('.interaktivbasetiles-modal-header').data('base_url');

    modal.submitForm(base_url + "/@@update_content", 'json', function (success, response_data) {
      if (success) {
        refreshTiles(response_data.uid, response_data.render);
        modal.close();
      }
    });
  });

  modal.$modal.on('click', '[name="add_new_video_button"]', function () {
    var createVideoModal = new ModalClass('create_video');
    createVideoModal.openUrl(base_url + '/@@add_new_video_modal', function () {
      initializeAddNewVideo(createVideoModal, modal);
    });
  });
}
