$(document).on('click', '[data-modal]', function (e) {
  e.preventDefault();
  var $this = $(this);
  var params = $this.data('modal-params') || {};
  var modal_id = params['id'] || '';
  var request = {};
  var requestData = $this.data('modal-request');
  if (requestData) {
    if (typeof(requestData) == 'string') {
      request = jQuery.parseJSON(requestData);
    } else {
      request = requestData;
    }
  }

  // setup function.
  var setupName = $this.data('modal-init') || false;
  var callback = function () {
  };
  if (setupName) {
    if (setupName.indexOf(',') >= 0) {
      callback = [];
      var setupNames = setupName.split(',');
      forEach(setupNames, function (value) {
        if (typeof window[value] == 'function') {
          callback.push(window[value]);
        }
      });
    }
    else if (typeof window[setupName] == 'function') {
      callback = window[setupName];
    }
  }

  if ($this.data('modal-size')) {
    params['size'] = $this.data('modal-size');
  }

  var modal = new ModalClass(modal_id, params);
  modal.openUrl($(this).data('modal'), request, function () {
    if (callback instanceof Array) {
      for (var key in callback) {
        callback[key](modal);
      }
    }
    else {
      callback(modal);
    }
  });

});

