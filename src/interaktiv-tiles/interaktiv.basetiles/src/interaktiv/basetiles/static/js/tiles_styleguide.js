$(document).ready(function() {

  $('.bfh-colorpicker').on('change.bfhcolorpicker', function () {
    var input = $(this).find('input');
    var css_class = $(this).data("css_class");
    var styleValue = input.val();
    var styleAttribute = $(this).data("style-attribute");
    var result = $('#tiles-styleguide-result').find('.' + css_class);
    result.css(styleAttribute, styleValue);
  });

});