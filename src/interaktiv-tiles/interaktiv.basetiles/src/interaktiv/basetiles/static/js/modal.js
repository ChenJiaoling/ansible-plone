/**
 * @class modalClass
 *
 * @param {string=} modalId - ID for modal
 * @param {object=} params - Parameter to setup Modal
 * @param {string=} params.tagClass - Class to add to the modal-wrapper
 * @param {string=} params.size - Size of Modal (xs, sm, md, lg)
 * @param {string|bool=} params.backdrop - Backdrop settings default:'own' (true, false, 'own')
 */
function ModalClass(modalId, params) {
  var defaultParams  = {backdrop: 'own'};
  params             = (typeof params == 'object') ? $.extend(defaultParams, params) : defaultParams;
  this.params        = params;
  this.request       = {};
  var self           = this;
  var $body          = $('body');
  modalId            = 'modal' + ((modalId ) ? '-' + modalId : '');
  this.$base         = $('<div class="modal fade" role="dialog" id="' + modalId + '" />');
  this.$dialog       = $('<div class="modal-dialog"/>');
  this.$modal        = $('<div class="modal-wrapper"></div>');
  this.$overlay      = $('<div class="modal-overlay"/>');
  var closeFunctions = [];

  // Setup of Modal
  (function () {

    self.$base.click(function (e) {
      if (e.target !== this) {
        return;
      }

      if (self.$modal.hasClass('close-on-overlay-click')) {
        self.close();
      }
    });

    self.$modal.on('click', '.interaktivtilesmodal-close', function () {
      self.close();
    });

    $(window).bind('keydown', function (e) {
      if (e.keyCode === 27) {
        self.close();
      }
    });

    self.$modal.on('keydown', function (e) {
      if (e.which == 13) {
        e.preventDefault();
        e.stopPropagation();
        $('.interaktivtilesmodal-submit', self.$modal).trigger('click');
      }
    });

    self.$base.append(self.$dialog);
    self.$dialog.append(self.$modal);

    // add additional Class to Modal content area
    if (params.tagClass)
      self.$modal.addClass(params.tagClass);

    // define Modal size
    if (params.size && $.inArray(params.size, [
        'xs',
        'sm',
        'md',
        'lg',
        'max',
        'xxs'
      ]) >= 0)
      self.$dialog.addClass('modal-' + params.size);
    else
      self.$dialog.addClass('modal-lg');

    // Add modal to DOM
    if (!document.contains(self.$base[0])) {
      $body.append(self.$base);
    }

    // Calculate Z-Index
    var zIndex = parseInt(self.$base.css('z-index'));

    var indecies = $.map($('*'), function (elem) {
      var tmpZIndex = parseInt($(elem).css('z-index'));
      if ($(elem).attr('id') == 'spinner') {
        return 0;
      }
      return (!isNaN(tmpZIndex) && tmpZIndex > zIndex) ? tmpZIndex : 0;
    });
    indecies.push(zIndex);
    zIndex = Math.max.apply(undefined, indecies);

    if (params.backdrop == 'own') {
      self.$overlay.css('z-index', ++zIndex);
      self.$base.css('z-index', ++zIndex);
      $body.append(self.$overlay);
    }
    else if (params.backdrop && $body.find('.modal-overlay').size() == 0) {
      zIndex--;
      self.$overlay.css('z-index', zIndex);
      $body.append(self.$overlay);
    }
    self.$base.css('z-index', zIndex);
  }());

  /**
   * Set parameter for Modal
   * @param {string} param
   * @param {mixed} value
   */
  this.setParam = function (param, value) {
    self.params[param] = value;
  };

  /**
   * Set paramters for Modal
   * @param {object} dict
   */
  this.setParams = function (dict) {
    if (typeof dict == 'object') {
      for (var key in dict) {
        if (!dict.hasOwnProperty(key)) continue;
        self.setParam(key, dict[key]);
      }
    }
  };

  /**
   * Opens the modal
   * @param {string|jQuery=} html
   * @param {function=} callback
   */
  this.open = function (html, callback) {
    callback = callback || noCallback;
    $body.addClass('modal-open');
    this.$base.css('display', 'block');
    this.$base.addClass('in');
    if (html) this.content(html);
    callback(this);
  };

  /**
   * Close the modal
   */
  this.close = function () {
    this.$overlay.remove();
    this.$base.remove();
    if ($('.modal', $body).size() == 0)
      $body.removeClass('modal-open');

    for (var key in closeFunctions) {
      if (!closeFunctions.hasOwnProperty(key)) continue;
      closeFunctions[key]();
    }
  };

  /**
   * Add a function to call on close of Modal
   * @param {function} fct
   * @return {int|boolean}
   */
  this.onClose = function (fct) {
    if (typeof fct != 'function') return false;
    return closeFunctions.push(fct);
  };

  /**
   * sets html content to Modal
   * @param {string|jQuery} html
   */
  this.content = function (html) {
    self.$modal.html(html);
  };

  /**
   * Open the Modal with content from given URL
   * @param {string} url
   * @param {object|function=} params/callback - if no params are needed can directly give callback function
   * @param {function=} callback
   */
  this.openUrl = function (url, params, callback) {
    if (typeof params == 'function') {
      callback     = params;
      self.request = {};
    }
    else self.request = params || {};
    $.ajax({
      url        : url,
      data       : self.request,
      type       : 'GET',
      dataType   : 'html',
      contentType: 'application/x-www-form-urlencoded; charset=utf-8',
      success    : function (data) {
        self.content(data);
        self.open();
        $('.selectpicker', self.$modal).selectpicker();
        if (callback && typeof callback == 'function') callback();
      },
      error      : function (XMLHttpRequest, textStatus, errorThrown) {
        console.log("XMLHttpRequest:", XMLHttpRequest, "| Status: ", textStatus, "| Error:", errorThrown);
      }
    });
  };

  /**
   * Submit the only form inside modal content
   * @param {string} url
   * @param {string} datatype - 'json' or 'html' as parameter
   * @param {function} callback
   */
  this.submitForm = function (url, datatype, callback) {
    callback = callback || noCallback;
    var form = new FormData(self.$modal.find('form').get(0));
    form.append('submit', '');

    self.$modal.find('form input[type="checkbox"]').each(function(){
      if($(this).is(':checked')){
        form.append($(this).attr('name'), true);
      }
      else {
        form.append($(this).attr('name'), false);
      }
    });

    $.ajax({
      url        : url,
      data       : form,
      type       : 'POST',
      processData: false,
      dataType   : datatype,
      contentType: false,
      success    : function (data, status, xhr) {
        if (xhr.status == 200) {
          if (datatype == 'json') {
            callback(true, data);
          }
          else if (datatype == 'html') {
            self.content(data);
            self.open();
            $('.selectpicker', self.$modal).selectpicker();
            callback(true);
          }
        }
        else {
          if (callback && typeof callback == 'function') callback(false);
        }
      },
      error      : function (XMLHttpRequest, textStatus, errorThrown) {
        console.log("XMLHttpRequest:", XMLHttpRequest, "| Status: ", textStatus, "| Error:", errorThrown);
        callback(false);
      }
    });
  };

  /**
   * Show an Error modal
   * @param {string} message
   * @param {string=} title
   * @param {function=} callback
   */
  this.showError = function (message, title, callback) {
    title    = title || 'Es ist ein Fehler aufgetreten.';
    callback = callback || noCallback;
    if (!this.errorModal)
      this.errorModal = new ModalClass('', {
        'size'    : 'xs',
        'tagClass': 'error-message'
      });
    var html = '<div class="interaktivbasetiles-modal-header"><div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10">';
    html += '<span class="title">' + title + '</span>';
    html += '</div></div>';
    html += '<div class="interaktivbasetiles-modal-content row"><div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10">' + message + '</div></div>';
    html += '<div class="interaktivbasetiles-modal-footer row"><div class="col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10">';
    html += '<button id="btn-close-modal" class="interaktivtilesmodal-close btn btn-ib btn-ib-info modal-btn-right">OK</button>';
    html += '</div></div>';
    this.errorModal.onClose(function () {
      delete self.errorModal;
    });
    this.errorModal.content(html);
    this.errorModal.open('', callback);
  };

  /**
   * Show a Error message after Header
   * @param {string|boolean} message - if message is false, remove alert
   */
  this.showErrorInline = function (message) {
    var $alert = this.$modal.find('.interaktivbasetiles-modal-header')
                     .siblings('.alert');

    if (!message) {
      if ($alert.length) $alert.remove();
      return;
    }

    var html = '<div class="alert alert-danger"><span class="type">Fehlermeldung</span> ' + message + '</div>';
    if ($alert.length) $alert.replaceWith(html);
    else this.$modal.find('.interaktivbasetiles-modal-header').after(html);
  };

  //noinspection JSUnusedGlobalSymbols
  /**
   * Valdiates a URL
   * @param {string} url
   * @returns {boolean}
   *
   * @TODO: can be removed?
   */
  this.validateUrl = function (url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!$&'()*+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!$&'()*+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!$&'()*+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!$&'()*+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!$&'()*+,;=]|:|@)|\/|\?)*)?$/i.test(url);
  }
}
