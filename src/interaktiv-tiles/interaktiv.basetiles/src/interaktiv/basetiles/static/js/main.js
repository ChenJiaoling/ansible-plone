jQuery.fn.extend({
  getPath     : function () {
    var pathes = [];

    this.each(function (index, element) {
      var path, $node = jQuery(element);

      while ($node.length) {
        var realNode = $node.get(0), name = realNode.localName;
        if (!name) {
          break;
        }

        name                = name.toLowerCase();
        var parent          = $node.parent();
        var sameTagSiblings = parent.children(name);

        if (sameTagSiblings.length > 1) {
          var allSiblings = parent.children();
          //noinspection JSDuplicatedDeclaration
          var index       = allSiblings.index(realNode) + 1;
          if (index > 0) {
            name += ':nth-child(' + index + ')';
          }
        }

        path  = name + (path ? ' > ' + path : '');
        $node = parent;
      }

      pathes.push(path);
    });

    return pathes.join(',');
  },
  appendAt    : function (index, $element) {
    $element = $($element);
    index    = (index < 0) ? 0 : index;

    if (index == 0) {
      this.prepend($element);
    }
    else {
      var $relativeElement = this.children().eq(index - 1);

      if ($relativeElement.is($element)) {
        if ($relativeElement.next().length) {
          $element.insertAfter($relativeElement.next());
        }
        else {
          $relativeElement.parent().append($element);
        }
      }
      else {
        $element.insertAfter($relativeElement);
      }
    }
  },
  isStale     : function () {
    return !(this[0] === document || document.body.contains(this[0]));
  },
  hasScrollbar: function () {
    return this.get(0).scrollHeight > this.height();
  }
});
jQuery.extend({
  getScrollbarWidth: function () {
    var div = $('<div style="width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;"><div style="height:100px;"></div>');
    $('body').append(div);
    var w1 = $('div', div).innerWidth();
    div.css('overflow-y', 'scroll');
    var w2 = $('div', div).innerWidth();
    $(div).remove();
    return (w1 - w2);
  }
});

/**
 * Function to iterate over an Object/Array
 * @param {object} data
 * @param {function} fct
 */
function forEach(data, fct) {
  "use strict";
  for (var key in data) {
    if (!data.hasOwnProperty(key)) continue;
    fct(data[key], key);
  }
}
/**
 * Placeholder function
 */
function noCallback() {}