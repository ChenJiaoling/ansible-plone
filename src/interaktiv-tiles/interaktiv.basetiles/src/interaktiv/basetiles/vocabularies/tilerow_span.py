# -*- coding: UTF-8 -*-
from interaktiv.framework.vocabulary import IFVocabulary

TILEROW_SPAN = [
            {'id': 'full', 'title': 'fullwidth'},
            {'id': 'dynamic_content', 'title': 'dynamic content'},
            {'id': 'dynamic_row', 'title': 'dynamic row'},
        ]


def tilerow_span(context):
    return IFVocabulary.create_vocabulary(
        TILEROW_SPAN
    )
