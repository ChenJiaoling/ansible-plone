# -*- coding: UTF-8 -*-
from interaktiv.framework.vocabulary import IFVocabulary

TRANSPARENCIES = [
            {'id': '1', 'title': '0% Transparenz'},
            {'id': '0.9', 'title': '10% Transparenz'},
            {'id': '0.8', 'title': '20% Transparenz'},
            {'id': '0.7', 'title': '30% Transparenz'},
            {'id': '0.6', 'title': '40% Transparenz'},
            {'id': '0.5', 'title': '50% Transparenz'},
            {'id': '0.4', 'title': '60% Transparenz'},
            {'id': '0.3', 'title': '70% Transparenz'},
            {'id': '0.2', 'title': '80% Transparenz'},
            {'id': '0.1', 'title': '90% Transparenz'},
            {'id': '0', 'title': '100% Transparenz'}
        ]


def transparencies(context):
    return IFVocabulary.create_vocabulary(
        TRANSPARENCIES
    )
