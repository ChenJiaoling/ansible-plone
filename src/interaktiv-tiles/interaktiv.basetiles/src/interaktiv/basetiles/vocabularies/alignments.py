# -*- coding: UTF-8 -*-
from interaktiv.framework.vocabulary import IFVocabulary

ALIGNMENTS = [
    {'id': 'left top', 'title': 'left top'},
    {'id': 'left center', 'title': 'left center'},
    {'id': 'left bottom', 'title': 'left bottom'},
    {'id': 'right top', 'title': 'right top'},
    {'id': 'right center', 'title': 'right center'},
    {'id': 'right bottom', 'title': 'right bottom'},
    {'id': 'center top', 'title': 'center top'},
    {'id': 'center center', 'title': 'center center'},
    {'id': 'center bottom', 'title': 'center bottom'}
]


def alignments(context):
    return IFVocabulary.create_vocabulary(
        ALIGNMENTS
    )


VERTICAL_ALIGNMENTS = [
    {'id': 'top', 'title': 'Top'},
    {'id': 'center', 'title': 'Center'},
    {'id': 'bottom', 'title': 'Bottom'},
    {'id': 'stretch', 'title': 'Strech'}
]


def vertical_alignments(context):
    return IFVocabulary.create_vocabulary(
        VERTICAL_ALIGNMENTS
    )
