# -*- coding: UTF-8 -*-
from interaktiv.framework.vocabulary import IFVocabulary

PADDINGS = [
            {'id': '0px', 'title': '0px'},
            {'id': '5px', 'title': '5px'},
            {'id': '10px', 'title': '10px'},
            {'id': '15px', 'title': '15px'},
            {'id': '20px', 'title': '20px'},
            {'id': '30px', 'title': '30px'},
            {'id': '50px', 'title': '50px'},
        ]


def paddings(context):
    return IFVocabulary.create_vocabulary(
        PADDINGS
    )
