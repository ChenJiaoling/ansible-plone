# -*- coding: UTF-8 -*-
from zope.schema.vocabulary import SimpleVocabulary
from zope.schema.vocabulary import SimpleTerm

COLOR_OPTIONS = [
    {
        'id': 'white',
        'title': 'Weiß',
        'value': 'rgba(255,255,255,1)',
        'icon': "circle circle-bordered",
    },
    {
        'id': 'bewo_green',
        'title': 'Bewo Grün',
        'value': 'rgba(154,210,44,1)',
        'icon': "circle",
    },
    {
        'id': 'mantis',
        'title': 'Mantis',
        'value': 'rgba(123,198,115,1)',
        'icon': "circle",
    },
    {
        'id': 'chateau_green',
        'title': 'Chateau Green',
        'value': 'rgba(66,167,97,1)',
        'icon': "circle",
    },
    {
        'id': 'niagara',
        'title': 'Niagara',
        'value': 'rgba(3,168,134,1)',
        'icon': "circle",
    },
    {
        'id': 'mountain_meadow',
        'title': 'Mountain Meadow',
        'value': 'rgba(27,188,155,1)',
        'icon': "circle",
    },
    {
        'id': 'curious_blue',
        'title': 'Curious Blue',
        'value': 'rgba(39,170,224,1)',
        'icon': "circle",
    },
    {
        'id': 'boston_blue',
        'title': 'Boston Blue',
        'value': 'rgba(62,143,186,1)',
        'icon': "circle",
    },
    {
        'id': 'mariner',
        'title': 'Mariner',
        'value': 'rgba(43,130,201,1)',
        'icon': "circle",
    },
    {
        'id': 'Shiraz',
        'title': 'Mariner',
        'value': 'rgba(199,13,50,1)',
        'icon': "circle",
    },
    {
        'id': 'Casablanca',
        'title': 'Mariner',
        'value': 'rgba(249,175,66,1)',
        'icon': "circle",
    },
    {
        'id': 'transparent',
        'title': 'Transparent',
        'value': 'rgba(0,0,0,0)',
        'icon': "circle circle-bordered circle-banned",
    },
]


def _create_vocabulary(options_dict):
    terms = []
    for option in options_dict:
        terms.append(
            SimpleTerm(option['id'], option['id'], option['title'])
        )
    return SimpleVocabulary(terms)


def colors(context):
    return _create_vocabulary(
        COLOR_OPTIONS
    )
