# -*- coding: UTF-8 -*-
from interaktiv.framework.vocabulary import IFVocabulary

ANIMATIONS = [
            {'id': 'parallax', 'title': 'Parallax'}
        ]


def animations(context):
    return IFVocabulary.create_vocabulary(
        ANIMATIONS
    )
