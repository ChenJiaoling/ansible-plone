# -*- coding: UTF-8 -*-
from interaktiv.framework.vocabulary import IFVocabulary


def image_types(context):
    return IFVocabulary.create_vocabulary(
        [
            {'id': 'pool', 'title': 'Pool'},
            {'id': 'background_images', 'title': 'Hintergrundbilder'},
            {'id': 'slider_images', 'title': 'Silder-Bilder'},
        ]
    )
