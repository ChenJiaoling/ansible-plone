# -*- coding: UTF-8 -*-
from zope.interface import Interface


class IInteraktivWorkingcopyLayer(Interface):
    """ Layer for Workingcopy specific Configurations """


class IInteraktivBasetilesLayer(Interface):
    """ Layer """
