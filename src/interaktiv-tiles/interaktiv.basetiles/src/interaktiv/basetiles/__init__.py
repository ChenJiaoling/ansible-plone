import logging

# from Products.CMFPlone.interfaces import ILanguage
# from plone.app.multilingual.interfaces import ITranslatable
# from plone.app.multilingual.interfaces import ITranslationManager
# from zope.component import getMultiAdapter
from zope.i18nmessageid import MessageFactory
# from zope.site.hooks import getSite

_ = MessageFactory("interaktiv.basetiles")
logger = logging.getLogger('interaktiv.tiles')


def initialize(context):
    """Initializer called when used as a Zope 2 product."""


# def get_language_site(request):
#     portal = getSite()
#     portal_state = getMultiAdapter(
#         (portal, request), name=u'plone_portal_state'
#     )
#     current_language = portal_state.language()
#     if current_language in portal.objectIds():
#         return portal[current_language]
#     return portal
#
#
# def get_language(context):
#     try:
#         return ILanguage(context).get_language()
#     except Exception:
#         return ''
#
#
# def get_translated_object(obj, language):
#     if ITranslatable.providedBy(obj):
#         if ITranslationManager(obj).has_translation(language):
#             obj = ITranslationManager(obj).get_translation(language)
#     return obj
