# -*- coding: UTF-8 -*-
from plone.dexterity.content import Container
from interaktiv.basetiles import _
from zope import schema
from zope.interface import Interface
from zope.interface import implements


class IMediaContainerCT(Interface):
    """ Interface for MediaContainerCT """

    image_type = schema.Choice(
        title=_(
            u'label_image_type',
            default=u'Bildtyp'
        ),
        description=u"",
        source="interaktiv.basetiles.vocabularies.image_types",
        required=True
    )


class MediaContainerCT(Container):
    """ MediaContainerCT Container """
    implements(IMediaContainerCT)
