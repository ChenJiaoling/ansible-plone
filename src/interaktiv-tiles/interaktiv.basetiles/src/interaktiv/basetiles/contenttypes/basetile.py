# -*- coding: UTF-8 -*-
from Products.CMFCore.utils import getToolByName
from interaktiv.basetiles import _
from interaktiv.basetiles.config import TILE_ACTIONS
from interaktiv.basetiles.config import BASE_TILE_CONFIG
from plone import api
from plone.dexterity.content import Container
from zope import schema
from zope.interface import Interface
from zope.interface import implements
from zope.site.hooks import getSite
from copy import deepcopy
from interaktiv.basetiles.contenttypes.basect_methods \
    import BaseCTBackgroundMethods


class IBaseTile(Interface):
    """ Interface for Tiles """

    title = schema.TextLine(
        title=_(
            u'label_title',
            default=u'Titel'
        ),
        description=u"",
        required=True
    )


class BaseTile(Container, BaseCTBackgroundMethods):
    """ Base Tile Class """
    implements(IBaseTile)

    tile_config = BASE_TILE_CONFIG

    def render(self, request):
        view = api.content.get_view(
            name='view',
            context=self,
            request=request,
        )
        return view()

    def get_title(self):
        if self.title == self.portal_type:
            return ''
        return self.title

    def merge_tile_config(self, custom_tile_config):
        tile_config = BASE_TILE_CONFIG.copy()
        for key, value in custom_tile_config.items():
            tile_config[key] = value
        return tile_config

    def get_image(self, field='image'):
        image_ref = getattr(self, field, None)
        title = getattr(self, field + '_title', '')

        if not image_ref:
            return dict()

        image = image_ref.to_object
        if not image:
            portal = getSite()
            return {
                'title': title,
                'id': 'default.png',
                'uid': str(),
                'description': str(),
                'url': '%s/default.png' % (
                    portal.absolute_url()
                ),
                'link': str(),
                'alt_tag': str(),
                'title_tag': str(),
                'add_in_new_window': False
            }

        # check if title/alt tag is set
        alt_tag = str()
        title_tag = title
        add_in_new_window = False
        link = self.get_image_link(field)
        if hasattr(self, 'image_alt_tag') and self.image_alt_tag:
            alt_tag = self.image_alt_tag
        if hasattr(self, 'image_title') and self.image_title:
            title_tag = self.image_title

        if getattr(self, 'image_title_from_reference', False):
            if not getattr(self, 'image_external_link', 'sometext'):
                if link['title']:
                    title_tag = link['title']

        return {
            'title': title,
            'id': image.id,
            'uid': image.UID(),
            'description': image.description,
            'url': image.absolute_url() + '/images/image',
            'link': link,
            'alt_tag': alt_tag,
            'title_tag': title_tag,
            'add_in_new_window': add_in_new_window
        }

    def get_image_link(self, image_field='image'):
        url = ''
        external = False
        title = ''
        #
        link_type = getattr(self, image_field + '_link_type')
        if link_type == 'external_link':
            url = getattr(self, image_field + '_external_link', '')
            external = True
        elif link_type == 'internal_link':
            internal_link_ref = getattr(
                self, image_field + '_internal_link', ''
            )
            if internal_link_ref:
                internal_link_obj = internal_link_ref.to_object
                if internal_link_obj:
                    url = internal_link_obj.absolute_url()
                    title = internal_link_obj.title

        if not link_type or link_type == 'no_link':
            if getattr(self, image_field + '_external_link', ''):
                url = getattr(self, image_field + '_external_link', '')
                external = True
            elif getattr(self, image_field + '_internal_link', ''):
                internal_link_ref = getattr(
                    self, image_field + '_internal_link', ''
                )
                if internal_link_ref:
                    internal_link_obj = internal_link_ref.to_object
                    if internal_link_obj:
                        url = internal_link_obj.absolute_url()
                        title = internal_link_obj.title

        target = '_self'
        if hasattr(self, image_field + '_add_in_new_window'):
            if getattr(self, image_field + '_add_in_new_window'):
                target = '_blank'

        #
        if not url:
            return {
                'url': str(),
                'external': str(),
                'target': str(),
                'title': str()
            }
        if not (url[0:7] == 'http://' or url[0:8] == 'https://'):
            url = 'http://' + url

        return {
            'url': url,
            'external': external,
            'target': target,
            'title': title
        }

    def get_available_tile_actions(self):
        return deepcopy(TILE_ACTIONS)

    @staticmethod
    def append_tile_action(actions=None, action=None):
        if action is None:
            action = {}
        if actions is None:
            actions = []
        exists = False
        for key, value in enumerate(actions):
            if value['action_id'] == action['action_id']:
                exists = key

        if exists is not False:
            actions[exists] = action
        else:
            actions.append(action)

        return actions

    def get_is_admin(self):
        mtool = getToolByName(self, 'portal_membership')
        return mtool.checkPermission('Modify portal content', self)

    def get_options(self):
        options = {
            'style': dict(),
            'class': list(),
            'data': dict()
        }

        # get tile padding
        options['style']['padding-left'] = getattr(
            self, 'padding_left', '0px'
        )
        options['style']['padding-right'] = getattr(
            self, 'padding_right', '0px'
        )
        options['style']['padding-top'] = getattr(
            self, 'padding_top', '0px'
        )
        options['style']['padding-bottom'] = getattr(
            self, 'padding_bottom', '0px'
        )

        # get tile vertical align
        valign = getattr(self, 'valign', 'stretch')
        valign_class = 'tile-valign-stretch'
        if valign == 'top':
            valign_class = 'tile-valign-top'
        elif valign == 'center':
            valign_class = 'tile-valign-center'
        elif valign == 'stretch':
            valign_class = 'tile-valign-stretch'
        elif valign == 'bottom':
            valign_class = 'tile-valign-bottom'

        options['class'].append(valign_class)

        return options

