# -*- coding: UTF-8 -*-
# noinspection PyProtectedMember
from interaktiv.basetiles import _

from plone.dexterity.content import Container
from zope import schema
from zope.interface import Interface
from zope.interface import implements


class ITilesContainerCT(Interface):
    """ Interface for TilesContainerCT """

    title = schema.TextLine(
        title=_(
            u'label_title',
            default=u'Titel'
        ),
        description=u"",
        required=True
    )

    description = schema.Text(
        title=_(
            u'label_description',
            default=u'Beschreibung'
        ),
        description=u"",
        required=False
    )


class TilesContainerCT(Container):
    """ Add Interface to TilesContainerCT Container """
    implements(ITilesContainerCT)

    def validate_tilerows(self):
        valid_list = []
        if not self.contentItems():
            return True
        else:
            for tile_row in self.contentItems():
                tile_row = tile_row[1]
                if hasattr(tile_row, 'validate_tiles') and \
                   callable(tile_row.validate_tiles):
                    if not tile_row.validate_tiles():
                        valid_list.append(False)
                    else:
                        valid_list.append(True)
        if False in valid_list:
            return False
        else:
            return True

