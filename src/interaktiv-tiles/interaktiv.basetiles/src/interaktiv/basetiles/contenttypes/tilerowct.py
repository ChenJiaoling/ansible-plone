# -*- coding: UTF-8 -*-
# noinspection PyProtectedMember
from interaktiv.basetiles import _
from interaktiv.basetiles.config import ROW_ACTIONS
from plone import api
from plone.dexterity.content import Container
from plone.supermodel import model
from zope import schema
from zope.interface import implements
from interaktiv.basetiles.vocabularies.paddings import PADDINGS
from interaktiv.basetiles.contenttypes.basect_methods \
    import BaseCTBackgroundMethods


class ITileRowCT(model.Schema):
    """ Interface for TileRow """

    title = schema.TextLine(
        title=_(
            u'label_title',
            default=u'Titel'
        ),
        description=u"",
        required=True
    )
    tilerow_span = schema.Choice(
        title=_(
            u'label_tilerow_span',
            default=u'Zeilenspanne'
        ),
        description=u"Wie soll die Zeile begrenzt sein?",
        source="interaktiv.basetiles.vocabularies.tilerow_span",
        default="dynamic_content",
        required=False
    )
    undeletable = schema.Bool(
        title=_(
            u'label_undeletable',
            default=u'Reihe nicht löschbar'
        ),
        description=u"Reihe nicht über den Tiles Editor entfernbar.",
        default=False,
        required=True
    )

    # --------- Custom Row Options ----------------

    model.fieldset(
        'padding',
        label=_(u"Row Options"),
        fields=[
            'padding_top',
            'padding_bottom'
        ]
    )

    padding_top = schema.Choice(
        title=_(
            u'label_padding_top',
            default=u'Padding Top'
        ),
        description=u"Abstand nach oben festlegen (in Pixeln)",
        source="interaktiv.basetiles.vocabularies.paddings",
        default="0px",
        required=True,
    )

    padding_bottom = schema.Choice(
        title=_(
            u'label_padding_bottom',
            default=u'Padding Bottom'
        ),
        description=u"Abstand nach unten festlegen (in Pixeln)",
        source="interaktiv.basetiles.vocabularies.paddings",
        default="0px",
        required=True,
    )


class TileRowCT(Container, BaseCTBackgroundMethods):
    """ Add Interface to TileRowCT Container """
    implements(ITileRowCT)

    def render(self, request):
        view = api.content.get_view(
            name='view',
            context=self,
            request=request,
        )
        return view()

    def get_available_row_actions(self):
        actions = []
        for action in ROW_ACTIONS:
            if action['action_id'] == 'remove':
                if self.undeletable:
                    continue
            actions.append(action)
        return actions

    def get_options(self):
        options = {
            'style': dict(),
            'class': list(),
            'data': dict()
        }

        options['style']['padding-top'] = self.padding_top
        options['style']['padding-bottom'] = self.padding_bottom

        return options

    @staticmethod
    def get_paddings():
        return PADDINGS

    def validate_tiles(self):
        valid_list = []
        if not self.contentItems():
            return True
        else:
            for tile in self.contentItems():
                tile = tile[1]
                if hasattr(tile, 'validate_tile') and \
                   callable(tile.validate_tile):
                    if not tile.validate_tile():
                        valid_list.append(False)
                    else:
                        valid_list.append(True)

        if False in valid_list:
            return False
        else:
            return True
