# -*- coding: UTF-8 -*-
from interaktiv.basetiles.vocabularies.colors import COLOR_OPTIONS
from interaktiv.basetiles.utilities.embed_video import get_embedvideo_utility


class BaseCTBackgroundMethods:

    def __init__(self):
        pass

    def get_background_image(self):
        image_options = {}

        background_image_data = getattr(self, 'background_image_data', '')
        if background_image_data:
            image_object = background_image_data.to_object
            if image_object:
                background_image_url = 'url(' + image_object.absolute_url()\
                                       + ')'
                image_options['background-image'] = background_image_url
                # background-repeat
                background_image_repeat = getattr(
                    self, 'background_image_repeat', ''
                )
                if not background_image_repeat:
                    image_options['background-repeat'] = 'no-repeat'
                    image_options['background-size'] = 'cover'

                # animation
                background_image_animation = getattr(
                    self, 'background_image_animation', ''
                )
                if background_image_animation:
                    if background_image_animation == 'parallax':
                        image_options['background-attachment'] = 'fixed'

                # alignment background-position
                background_image_alignment = getattr(
                    self, 'background_image_alignment', ''
                )
                if background_image_alignment:
                    image_options['background-position'] = \
                        background_image_alignment

        return image_options

    def get_background_overlay_options(self):
        overlay_options = {}
        overlay_opacity = getattr(self, 'background_overlay_opacity', '')
        overlay_color = getattr(self, 'background_overlay_color', '')

        if overlay_opacity:
            overlay_options['opacity'] = overlay_opacity

        if overlay_color:
            for each in COLOR_OPTIONS:
                if overlay_color == each['id']:
                    overlay_options['background-color'] = each['value']
                    break

        return overlay_options

    def get_background_color(self):
        color_options = {}
        color = getattr(self, 'background_color', '')
        if color:
            for each in COLOR_OPTIONS:
                if color == each['id']:
                    color_options['background-color'] = each['value']
                    break

        return color_options

    def get_background_video(self):
        video = {}

        background_video_data = getattr(self, 'background_video_data', '')
        if background_video_data:
            # get video_object
            video_obj = background_video_data.to_object
            if video_obj:
                # add video portaltype
                portaltype = video_obj.portal_type
                video['portaltype'] = portaltype
                # add video url and loop
                is_loop = getattr(
                    self, 'background_video_loop', False
                )
                if portaltype == 'File':
                    video['background_video_url'] = video_obj.absolute_url()
                    video['background_video_loop'] = is_loop
                elif video_obj.portal_type == 'VideourlCT':
                    embedvideo_utility = get_embedvideo_utility()
                    youtube_video_id = embedvideo_utility.get_youtube_video_id(
                        video_obj.videourl
                    )
                    embed_url = embedvideo_utility.get_youtube_embed_url(
                        youtube_video_id
                    )

                    if is_loop:
                        video['background_video_url'] = \
                            embed_url + \
                            '&autoplay=1&controls=0&mute=1&' \
                            'iv_load_policy=3&showinfo=0&loop=1&playlist=' + \
                            youtube_video_id
                    else:
                        video['background_video_url'] = \
                            embed_url + '&autoplay=1&controls=0&mute=1&' \
                                        'iv_load_policy=3&showinfo=0&loop=0'

        return video
