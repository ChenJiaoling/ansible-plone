# -*- coding: UTF-8 -*-
from plone.dexterity.content import Item
from zope import schema
from zope.interface import Interface
from zope.interface import implements
from interaktiv.basetiles import _


class IVideourlCT(Interface):
    """ Interface for VideourlCT """
    title = schema.TextLine(
        title=_(
            u'label_videourltitle',
            default=u'Video-URL-Title'
        ),
        required=True
    )
    videourl = schema.TextLine(
        title=_(
            u'label_videourl',
            default=u'Video-URL'
        ),
        required=True
    )


class VideourlCT(Item):
    """ VideourlCT Container """
    implements(IVideourlCT)
