# -*- coding: UTF-8 -*-
from interaktiv.basetiles.contenttypes.mediacontainerct import \
    IMediaContainerCT
from interaktiv.basetiles.testing import \
    INTERAKTIV_BASETILES_INTEGRATION_TESTING
from plone.dexterity.interfaces import IDexterityFTI
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from zope.component import createObject
from zope.component import queryUtility

import unittest2 as unittest


class MediaContainerCTIntegrationTest(unittest.TestCase):

    layer = INTERAKTIV_BASETILES_INTEGRATION_TESTING
    portal = None
    request = None

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        self.request['ACTUAL_URL'] = self.portal.absolute_url()
        setRoles(self.portal, TEST_USER_ID, ['Manager'])

    def test_schema(self):
        fti = queryUtility(
            IDexterityFTI,
            name='MediaContainerCT'
        )
        schema = fti.lookupSchema()
        self.assertEquals(IMediaContainerCT, schema)

    def test_fti(self):
        fti = queryUtility(
            IDexterityFTI,
            name='MediaContainerCT'
        )
        self.assertNotEquals(None, fti)

    def test_factory(self):
        fti = queryUtility(
            IDexterityFTI,
            name='MediaContainerCT'
        )
        factory = fti.factory
        new_object = createObject(factory)
        self.assertEqual(
            new_object.__module__,
            'interaktiv.basetiles.contenttypes.mediacontainerct'
        )

    def test_media_container_ct_provides_interface(self):
        fti = queryUtility(
            IDexterityFTI,
            name='MediaContainerCT'
        )
        factory = fti.factory
        new_object = createObject(factory)

        self.failUnless(IMediaContainerCT.providedBy(new_object))

    def test_adding(self):
        self.portal.invokeFactory('MediaContainerCT', 'mc')
        self.assertTrue(self.portal['mc'])
