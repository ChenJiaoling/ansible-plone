# -*- coding: UTF-8 -*-
from interaktiv.basetiles.contenttypes.tilescontainerct import ITilesContainerCT
from interaktiv.basetiles.testing import \
    INTERAKTIV_BASETILES_INTEGRATION_TESTING

import transaction
import unittest2 as unittest
from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility


class TilesContainerCTTest(unittest.TestCase):

    layer = INTERAKTIV_BASETILES_INTEGRATION_TESTING
    portal = None
    request = None

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        self.request['ACTUAL_URL'] = self.portal.absolute_url()
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        transaction.commit()

    def tearDown(self):
        pass

    def test_schema(self):
        fti = queryUtility(
            IDexterityFTI,
            name='TilesContainerCT'
        )
        schema = fti.lookupSchema()
        self.assertEquals(ITilesContainerCT, schema)

    def test_fti(self):
        fti = queryUtility(
            IDexterityFTI,
            name='TilesContainerCT'
        )
        self.assertNotEquals(None, fti)

    def test_factory(self):
        fti = queryUtility(
            IDexterityFTI,
            name='TilesContainerCT'
        )
        factory = fti.factory
        new_object = createObject(factory)
        self.assertEqual(
            new_object.__module__,
            'interaktiv.basetiles.contenttypes.tilescontainerct'
        )

    def test_tilescontainer_ct_provides_interface(self):
        fti = queryUtility(
            IDexterityFTI,
            name='TilesContainerCT'
        )
        factory = fti.factory
        new_object = createObject(factory)

        self.failUnless(ITilesContainerCT.providedBy(new_object))

    def test_adding(self):
        self.portal.invokeFactory('TilesContainerCT', 'tilescontainerct')
        self.assertTrue(self.portal.tilescontainerct)
