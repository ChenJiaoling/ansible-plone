# -*- coding: UTF-8 -*-
from interaktiv.basetiles.testing import \
    INTERAKTIV_BASETILES_INTEGRATION_TESTING
from plone import api
import transaction
import unittest2 as unittest
from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles
from interaktiv.basetiles.vocabularies.paddings import PADDINGS
from zope.component import getUtility
from z3c.relationfield import RelationValue
from zope.intid.interfaces import IIntIds
import json
from plone.app.uuid.utils import uuidToObject
import os
from ZPublisher.HTTPRequest import FileUpload


class BaseTileModalTest(unittest.TestCase):

    layer = INTERAKTIV_BASETILES_INTEGRATION_TESTING
    portal = None
    request = None

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        self.request['ACTUAL_URL'] = self.portal.absolute_url()
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.portal.invokeFactory(
            "TilesContainerCT",
            id="tilescontainerct",
            title="TilesContainerCT",
        )
        self.portal.tilescontainerct.invokeFactory(
            "TileRowCT",
            id="tilerowct",
            title="TileRowCT",
        )
        self.portal.tilescontainerct.tilerowct.invokeFactory(
            "BaseTile",
            id="basetile",
            title="BaseTile",
        )
        self.portal.invokeFactory(
            "MediaContainerCT",
            id="media_container",
            title="MediaContainer",
        )
        self.portal.media_container.invokeFactory(
            'Image',
            id='image',
            title='image'
        )
        self.portal.media_container.invokeFactory(
            'File',
            id='file_video',
            title='file_video'
        )
        self.portal.media_container.invokeFactory(
            'VideourlCT',
            id='youtube_video',
            title='youtube_video',
            videourl='https://www.youtube.com/watch?v=660hAWKJEg8'
        )
        transaction.commit()

    def tearDown(self):
        if 'tilescontainerct' in self.portal.objectIds():
            self.portal.manage_delObjects(['tilescontainerct'])
        if 'media_container' in self.portal.objectIds():
            self.portal.manage_delObjects(['media_container'])
        transaction.commit()

    def test_paddings_modal(self):
        basetile = self.portal.tilescontainerct.tilerowct.basetile

        for padding in PADDINGS:
            self.request.form['padding_top'] = padding['id']
            self.request.form['padding_bottom'] = padding['id']
            self.request.form['padding_left'] = padding['id']
            self.request.form['padding_right'] = padding['id']
            self.request.form['submit'] = ''

            view = api.content.get_view(
                name='tile_padding_modal',
                context=basetile,
                request=self.request,
            )
            view()

            self.assertEqual(getattr(basetile, 'padding_top'), padding['id'])
            self.assertEqual(getattr(basetile, 'padding_bottom'), padding['id'])
            self.assertEqual(getattr(basetile, 'padding_left'), padding['id'])
            self.assertEqual(getattr(basetile, 'padding_right'), padding['id'])

    def test_background_image_modal(self):
        basetile = self.portal.tilescontainerct.tilerowct.basetile
        # set up background image
        image = self.portal.media_container.image
        intids = getUtility(IIntIds)
        reference = RelationValue(intids.getId(image))
        setattr(basetile, 'background_image_data', reference)
        transaction.commit()

        view = api.content.get_view(
            name='configure_background_image_modal',
            context=basetile,
            request=self.request,
        )
        background_image_object = view.get_background_image_object(basetile)
        self.assertEqual(background_image_object, image)

    def test_background_color_modal(self):
        basetile = self.portal.tilescontainerct.tilerowct.basetile
        setattr(basetile, 'background_color', 'bewo_green')
        transaction.commit()

        view = api.content.get_view(
            name='configure_background_color_modal',
            context=basetile,
            request=self.request,
        )
        color_options = view.get_background_colors()
        for color in color_options:
            if color['id'] == 'bewo_green':
                self.assertEqual(
                    color['class'],
                    'color-item circle selected'
                )
            elif color['id'] == 'white':
                self.assertEqual(
                    color['class'],
                    'color-item circle circle-bordered '
                )
            elif color['id'] == 'transparent':
                self.assertEqual(
                    color['class'],
                    'color-item circle circle-bordered circle-banned '
                )
            else:
                self.assertEqual(
                    color['class'],
                    'color-item circle '
                )

    def test_background_overlay_modal(self):
        basetile = self.portal.tilescontainerct.tilerowct.basetile
        setattr(basetile, 'background_overlay_color', 'bewo_green')
        transaction.commit()

        view = api.content.get_view(
            name='configure_background_overlay_modal',
            context=basetile,
            request=self.request,
        )
        color_options = view.get_background_overlay_colors()
        for color in color_options:
            if color['id'] == 'bewo_green':
                self.assertEqual(
                    color['class'],
                    'color-item circle selected'
                )
            elif color['id'] == 'white':
                self.assertEqual(
                    color['class'],
                    'color-item circle circle-bordered '
                )
            elif color['id'] == 'transparent':
                self.assertEqual(
                    color['class'],
                    'color-item circle circle-bordered circle-banned '
                )
            else:
                self.assertEqual(
                    color['class'],
                    'color-item circle '
                )

    def test_remove_background_options_modal(self):
        basetile = self.portal.tilescontainerct.tilerowct.basetile
        # set up background image
        image = self.portal.media_container.image
        intids = getUtility(IIntIds)
        reference = RelationValue(intids.getId(image))
        setattr(basetile, 'background_image_data', reference)
        # set up background color
        setattr(basetile, 'background_color', 'bewo_green')
        # set up background overlay
        setattr(basetile, 'background_overlay_opacity', '0.5')
        setattr(basetile, 'background_overlay_color', 'bewo_green')
        transaction.commit()

        self.request.form['submit'] = ''

        view = api.content.get_view(
            name='remove_background_options_modal',
            context=basetile,
            request=self.request,
        )
        view()

        self.assertEqual(basetile.background_image_data, None)
        self.assertEqual(basetile.background_color, 'transparent')
        self.assertEqual(basetile.background_overlay_opacity, '0')
        self.assertEqual(basetile.background_overlay_color, 'transparent')

    def test_background_video_modal_youtube_video(self):
        basetile = self.portal.tilescontainerct.tilerowct.basetile
        # set up background video
        youtube_video = self.portal.media_container.youtube_video
        intids = getUtility(IIntIds)
        reference = RelationValue(intids.getId(youtube_video))
        setattr(basetile, 'background_video_data', reference)
        transaction.commit()

        view = api.content.get_view(
            name='configure_background_video_modal',
            context=basetile,
            request=self.request,
        )

        self.assertEqual(
            view.get_background_video_object(basetile),
            youtube_video
        )
        self.assertEqual(
            view.get_background_video_embedurl(youtube_video),
            'https://www.youtube.com/embed/660hAWKJEg8?'
            'rel=0&autoplay=1&controls=0&mute=1&iv_load_policy=3&showinfo=0'
        )

    def test_background_video_modal_file_video(self):
        basetile = self.portal.tilescontainerct.tilerowct.basetile
        # set up background video
        file_video = self.portal.media_container.file_video
        intids = getUtility(IIntIds)
        reference = RelationValue(intids.getId(file_video))
        setattr(basetile, 'background_video_data', reference)
        transaction.commit()

        view = api.content.get_view(
            name='configure_background_video_modal',
            context=basetile,
            request=self.request,
        )

        self.assertEqual(
            view.get_background_video_object(basetile),
            file_video
        )

    def test_add_new_video_modal_youtube_video(self):
        form = self.request.form
        media_container = self.portal.media_container
        form['submit'] = ''
        form['video_container_uid'] = media_container.UID()
        form['video_title'] = 'Youtube Video'
        form['youtube_url'] = 'https://www.youtube.com/watch?v=660hAWKJEg8'

        view = api.content.get_view(
            name='add_new_video_modal',
            context=self.portal.media_container,
            request=self.request,
        )
        view()
        new_video_info = json.loads(view.new_video_data)

        new_video_object = uuidToObject(new_video_info['uid'])
        self.assertTrue(new_video_object)
        self.assertTrue(
            new_video_object.id in media_container.objectIds()
        )
        self.assertEqual(new_video_info['portaltype'], 'VideourlCT')
        self.assertEqual(new_video_info['title'], 'Youtube Video')
        self.assertEqual(
            new_video_info['videourl'],
            'https://www.youtube.com/watch?v=660hAWKJEg8'
        )

    def test_add_new_video_modal_file_video(self):
        form = self.request.form
        media_container = self.portal.media_container
        form['submit'] = ''
        form['video_container_uid'] = media_container.UID()
        form['video_title'] = 'Local Video'
        video_file = os.path.join(
            os.path.dirname(__file__),
            '../data',
            u'video.mp4'
        )
        video_file = open(video_file, 'r')
        a_field_storage = MockaFieldStorage(video_file, 'video.mp4')
        form['select_video_file'] = FileUpload(a_field_storage)

        view = api.content.get_view(
            name='add_new_video_modal',
            context=self.portal.media_container,
            request=self.request,
        )

        view()
        new_video_info = json.loads(view.new_video_data)

        new_video_object = uuidToObject(new_video_info['uid'])
        self.assertTrue(new_video_object)
        self.assertTrue(
            new_video_object.id in media_container.objectIds()
        )
        self.assertEqual(new_video_info['portaltype'], 'File')
        self.assertEqual(new_video_info['title'], 'Local Video')
        self.assertEqual(
            new_video_info['videourl'],
            new_video_object.absolute_url()
        )


class MockaFieldStorage(object):

    def __init__(self, file_obj, filename='', headers=''):
        self.file = file_obj
        self.filename = filename
        self.headers = headers



