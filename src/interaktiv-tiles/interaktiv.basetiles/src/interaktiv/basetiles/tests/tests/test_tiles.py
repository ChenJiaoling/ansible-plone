# -*- coding: UTF-8 -*-
import unittest2 as unittest
from interaktiv.basetiles.contenttypes.basetile import IBaseTile
from interaktiv.basetiles.testing import \
    INTERAKTIV_BASETILES_INTEGRATION_TESTING
from interaktiv.basetiles.vocabularies.paddings import PADDINGS
from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility
import transaction
from zope.component import getUtility
from z3c.relationfield import RelationValue
from zope.intid.interfaces import IIntIds
from interaktiv.basetiles.vocabularies.alignments import VERTICAL_ALIGNMENTS
from interaktiv.basetiles.vocabularies.colors import COLOR_OPTIONS
from interaktiv.basetiles.vocabularies.transparencies import TRANSPARENCIES
from interaktiv.basetiles.vocabularies.alignments import ALIGNMENTS


class BaseTileTest(unittest.TestCase):

    layer = INTERAKTIV_BASETILES_INTEGRATION_TESTING
    portal = None
    request = None

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        self.request['ACTUAL_URL'] = self.portal.absolute_url()
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.portal.invokeFactory(
            "TilesContainerCT",
            id="tilescontainerct",
            title="TilesContainerCT",
        )
        self.portal.tilescontainerct.invokeFactory(
            "TileRowCT",
            id="tilerowct",
            title="TileRowCT",
        )
        self.portal.tilescontainerct.tilerowct.invokeFactory(
            "BaseTile",
            id="basetile",
            title="BaseTile",
        )
        self.portal.invokeFactory(
            'Image',
            id='image',
            title='image'
        )
        transaction.commit()

    def tearDown(self):
        if 'tilescontainerct' in self.portal.objectIds():
            self.portal.manage_delObjects(['tilescontainerct'])
        if 'image' in self.portal.objectIds():
            self.portal.manage_delObjects(['image'])
        transaction.commit()

    def test_schema(self):
        fti = queryUtility(
            IDexterityFTI,
            name='BaseTile'
        )
        schema = fti.lookupSchema()
        self.assertEquals(IBaseTile, schema)

    def test_fti(self):
        fti = queryUtility(
            IDexterityFTI,
            name='BaseTile'
        )
        self.assertNotEquals(None, fti)

    def test_factory(self):
        fti = queryUtility(
            IDexterityFTI,
            name='BaseTile'
        )
        factory = fti.factory
        new_object = createObject(factory)
        self.assertEqual(
            new_object.__module__,
            'interaktiv.basetiles.contenttypes.basetile'
        )

    def test_text_ct_provides_interface(self):
        fti = queryUtility(
            IDexterityFTI,
            name='BaseTile'
        )
        factory = fti.factory
        new_object = createObject(factory)

        self.failUnless(IBaseTile.providedBy(new_object))

    def test_adding(self):
        self.portal.tilescontainerct.tilerowct.invokeFactory(
            'BaseTile', 'new_basetile'
        )
        self.assertTrue(self.portal.tilescontainerct.tilerowct.new_basetile)

    def test_padding_settings(self):
        basetile = self.portal.tilescontainerct.tilerowct.basetile
        orientations = ['left', 'right', 'top', 'bottom']

        for orientation in orientations:
            style_attr = 'padding-' + orientation
            schema_attr = 'padding_' + orientation
            for padding in PADDINGS:
                setattr(basetile, schema_attr, padding['id'])
                transaction.commit()

                tile_options = basetile.get_options()

                setted_padding = ''

                for setted_attr in tile_options['style']:
                    if setted_attr == style_attr:
                        setted_padding = tile_options['style'][setted_attr]
                        break

                self.assertEquals(setted_padding, padding['id'])

    def test_vertical_alignment_settings(self):
        basetile = self.portal.tilescontainerct.tilerowct.basetile
        for valign in VERTICAL_ALIGNMENTS:
            setattr(basetile, 'valign', valign['id'])
            wished_classname = 'tile-valign-' + valign['id']
            transaction.commit()

            tile_options = basetile.get_options()

            self.assertIn(wished_classname, tile_options['class'])

    def test_background_image_settings(self):
        basetile = self.portal.tilescontainerct.tilerowct.basetile
        image = self.portal.image
        intids = getUtility(IIntIds)
        reference = RelationValue(intids.getId(image))

        setattr(basetile, 'background_image_data', reference)
        setattr(basetile, 'background_image_repeat', True)
        setattr(basetile, 'background_image_animation', 'parallax')
        for each in ALIGNMENTS:
            setattr(basetile, 'background_image_alignment', each['id'])
            transaction.commit()

            self.assertEqual(
                basetile.get_background_image()['background-image'],
                'url(' + image.absolute_url() + ')'
            )
            self.assertEqual(
                basetile.get_background_image()['background-attachment'],
                'fixed'
            )
            self.assertEqual(
                basetile.get_background_image()['background-position'],
                each['id']
            )

    def test_background_color_settings(self):
        basetile = self.portal.tilescontainerct.tilerowct.basetile
        for each in COLOR_OPTIONS:
            setattr(basetile, 'background_color', each['id'])
            transaction.commit()
            self.assertEqual(
                basetile.get_background_color()['background-color'],
                each['value']
            )

    def test_background_overlay_settings(self):
        basetile = self.portal.tilescontainerct.tilerowct.basetile
        for each in TRANSPARENCIES:
            setattr(basetile, 'background_overlay_opacity', each['id'])
            transaction.commit()
            self.assertEqual(
                basetile.get_background_overlay_options()['opacity'],
                each['id']
            )

        setattr(basetile, 'background_overlay_opacity', 0.5)
        for each in COLOR_OPTIONS:
            setattr(basetile, 'background_overlay_color', each['id'])
            transaction.commit()
            self.assertEqual(
                basetile.get_background_overlay_options()['opacity'],
                0.5
            )
            self.assertEqual(
                basetile.get_background_overlay_options()['background-color'],
                each['value']
            )
