# -*- coding: UTF-8 -*-
from interaktiv.basetiles.testing import \
    INTERAKTIV_BASETILES_INTEGRATION_TESTING
import transaction
import unittest2 as unittest
from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility
from interaktiv.basetiles.contenttypes.tilerowct import ITileRowCT


class TileRowCTTest(unittest.TestCase):

    layer = INTERAKTIV_BASETILES_INTEGRATION_TESTING
    portal = None
    request = None

    def setUp(self):
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        self.request['ACTUAL_URL'] = self.portal.absolute_url()
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        transaction.commit()

    def tearDown(self):
        pass

    def test_schema(self):
        fti = queryUtility(
            IDexterityFTI,
            name='TileRowCT'
        )
        schema = fti.lookupSchema()
        self.assertEquals(ITileRowCT, schema)

    def test_fti(self):
        fti = queryUtility(
            IDexterityFTI,
            name='TileRowCT'
        )
        self.assertNotEquals(None, fti)

    def test_factory(self):
        fti = queryUtility(
            IDexterityFTI,
            name='TileRowCT'
        )
        factory = fti.factory
        new_object = createObject(factory)
        self.assertEqual(
            new_object.__module__,
            'interaktiv.basetiles.contenttypes.tilerowct'
        )

    def test_text_ct_provides_interface(self):
        fti = queryUtility(
            IDexterityFTI,
            name='TileRowCT'
        )
        factory = fti.factory
        new_object = createObject(factory)

        from interaktiv.basetiles.contenttypes.tilerowct import ITileRowCT
        self.failUnless(ITileRowCT.providedBy(new_object))

    def test_adding(self):
        self.portal.invokeFactory('TilesContainerCT', 'tilescontainerct')
        self.portal.tilescontainerct.invokeFactory(
            'TileRowCT', 'tilerowct'
        )
        self.assertTrue(self.portal.tilescontainerct.tilerowct)
