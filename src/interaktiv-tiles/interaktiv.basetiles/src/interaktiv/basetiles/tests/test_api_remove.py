from plone import api
from plone.app.testing import TEST_USER_ID, setRoles
from plone.dexterity.utils import createContentInContainer

import transaction

import unittest2 as unittest

from interaktiv.basetiles.testing import \
    INTERAKTIV_BASETILES_FUNCTIONAL_TESTING


class TestApiRemoveContent(unittest.TestCase):

    layer = INTERAKTIV_BASETILES_FUNCTIONAL_TESTING
    portal = None
    request = None
    app = None

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        createContentInContainer(
            self.portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )
        # noinspection PyAttributeOutsideInit
        self.tile_row_a = createContentInContainer(
            self.portal.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        # noinspection PyAttributeOutsideInit
        self.base_tile_a = createContentInContainer(
            self.portal.tiles_container_a.tile_row_a,
            'BaseTile',
            id='base_tile_a',
        )
        self.base_tile_a.reindexObject()

    def tearDown(self):
        if 'tiles_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['tiles_container_a'])
        transaction.commit()

    def test_remove_base_tile_removes_empty_tile_row(self):
        view = api.content.get_view(
            name='remove_content',
            context=self.base_tile_a,
            request=self.request,
        )
        view()
        self.assertEqual(
            len(self.portal.tiles_container_a.objectIds()), 0
        )

    def test_remove_base_tile_keeps_filled_tile_row(self):
        createContentInContainer(
            self.portal.tiles_container_a.tile_row_a,
            'BaseTile',
            id='base_tile_b',
        )
        view = api.content.get_view(
            name='remove_content',
            context=self.base_tile_a,
            request=self.request,
        )
        view()
        self.assertEqual(
            len(self.portal.tiles_container_a.tile_row_a.objectIds()), 1
        )
        self.assertEqual(
            self.portal.tiles_container_a.tile_row_a.objectIds()[0],
            'base_tile_b'
        )

    def test_remove_tile_row(self):
        view = api.content.get_view(
            name='remove_content',
            context=self.tile_row_a,
            request=self.request,
        )
        view()
        self.assertEqual(
            len(self.portal.tiles_container_a.objectIds()), 0
        )


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestApiRemoveContent))
    return suite
