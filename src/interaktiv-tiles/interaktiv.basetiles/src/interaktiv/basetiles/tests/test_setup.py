# -*- coding: utf-8 -*-
import unittest2 as unittest

from Products.CMFCore.utils import getToolByName

from interaktiv.basetiles.testing import \
    INTERAKTIV_BASETILES_INTEGRATION_TESTING


class TestSetup(unittest.TestCase):
    """ Test setup for OL theme """

    layer = INTERAKTIV_BASETILES_INTEGRATION_TESTING
    app = None
    portal = None
    qi_tool = None

    def setUp(self):
        """ Set up of every Test in OL theme """
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.qi_tool = getToolByName(self.portal, 'portal_quickinstaller')

    def test_interaktiv_basetiles_is_installed(self):
        """ Check if interaktiv.basetiles is installed
        :return bool (interaktiv.basetiles is installed):
        """
        pid = 'interaktiv.basetiles'
        installed = [p['id'] for p in self.qi_tool.listInstalledProducts()]
        self.assertTrue(
            pid in installed,
            'package %s appears not to have been installed' % pid
        )
