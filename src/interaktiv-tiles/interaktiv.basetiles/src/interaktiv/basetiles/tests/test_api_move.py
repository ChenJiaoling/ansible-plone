import transaction
import unittest2 as unittest
from interaktiv.basetiles.testing import INTERAKTIV_BASETILES_FUNCTIONAL_TESTING
from plone import api
from plone.app.testing import TEST_USER_ID, setRoles
from plone.dexterity.utils import createContentInContainer


class TestApiMoveContent(unittest.TestCase):

    layer = INTERAKTIV_BASETILES_FUNCTIONAL_TESTING
    portal = None
    request = None
    app = None

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        createContentInContainer(
            self.portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )
        self.tile_row_a = createContentInContainer(
            self.portal.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        self.portal.tiles_container_a.tile_row_a.reindexObject()

        self.tile_row_b = createContentInContainer(
            self.portal.tiles_container_a,
            'TileRowCT',
            id='tile_row_b',
        )
        self.portal.tiles_container_a.tile_row_b.reindexObject()

        transaction.commit()

    def test_move_tile_row_to_top(self):
        self.request.form['position'] = '0'
        view = api.content.get_view(
            name='move_content',
            context=self.portal.tiles_container_a.tile_row_b,
            request=self.request,
        )
        view()
        tile_row_ids = self.portal.tiles_container_a.objectIds()
        self.assertEqual(tile_row_ids[0], 'tile_row_b')
        self.assertEqual(tile_row_ids[1], 'tile_row_a')

    def test_move_tile_row_to_bottom(self):
        self.request.form['position'] = '1'
        view = api.content.get_view(
            name='move_content',
            context=self.portal.tiles_container_a.tile_row_a,
            request=self.request,
        )
        view()
        tile_row_ids = self.portal.tiles_container_a.objectIds()
        self.assertEqual(tile_row_ids[0], 'tile_row_b')
        self.assertEqual(tile_row_ids[1], 'tile_row_a')

    def test_move_tile_row_to_bottom_with_larger_position(self):
        self.request.form['position'] = '1000'
        view = api.content.get_view(
            name='move_content',
            context=self.portal.tiles_container_a.tile_row_a,
            request=self.request,
        )
        view()
        tile_row_ids = self.portal.tiles_container_a.objectIds()
        self.assertEqual(tile_row_ids[0], 'tile_row_b')
        self.assertEqual(tile_row_ids[1], 'tile_row_a')

    def tearDown(self):
        if 'tiles_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['tiles_container_a'])
        transaction.commit()


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestApiMoveContent))
    return suite
