import transaction
import unittest2 as unittest
from interaktiv.basetiles.testing import INTERAKTIV_BASETILES_FUNCTIONAL_TESTING
from plone import api
from plone.app.testing import TEST_USER_ID, setRoles
from plone.dexterity.utils import createContentInContainer
from plone.testing.z2 import Browser


class TestApiUpdate(unittest.TestCase):

    layer = INTERAKTIV_BASETILES_FUNCTIONAL_TESTING
    portal = None
    request = None
    app = None
    browser = None

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']

        self.browser = Browser(self.app)
        self.browser.handleErrors = False

        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        createContentInContainer(
            self.portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )
        createContentInContainer(
            self.portal.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        transaction.commit()

    def tearDown(self):
        if 'tiles_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['tiles_container_a'])
        transaction.commit()

    def test_update_tile_container_title(self):
        self.request.form['title'] = 'New Container Title A'
        view = api.content.get_view(
            name='update_content',
            context=self.portal.tiles_container_a,
            request=self.request,
        )
        view()
        transaction.commit()
        self.assertTrue(
            self.portal.tiles_container_a.title, 'New Container Title A'
        )


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestApiUpdate))
    return suite
