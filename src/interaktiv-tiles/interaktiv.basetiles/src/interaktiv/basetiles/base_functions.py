# -*- coding: utf-8 -*-
from zope.site.hooks import getSite
from plone import api


def tile_installer(entry_data):
    """ Installerscript to add Tile to Registry
    """

    registered_tiles = api.portal.get_registry_record(
        'interaktiv.basetiles.tiles'
    )

    exists = False
    for index, tile in enumerate(registered_tiles):
        if tile['id'] == entry_data['id']:
            exists = index

    if exists is False:
        registered_tiles.append(entry_data)
    else:
        registered_tiles[exists] = entry_data

    api.portal.set_registry_record(
        'interaktiv.basetiles.tiles',
        registered_tiles
    )
