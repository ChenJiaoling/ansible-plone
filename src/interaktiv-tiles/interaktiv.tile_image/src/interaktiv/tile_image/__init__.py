from zope.i18nmessageid import MessageFactory
import logging

_ = MessageFactory("interaktiv.tile_image")
logger = logging.getLogger('interaktiv.tile_image')


def initialize(context):
    """Initializer called when used as a Zope 2 product."""
