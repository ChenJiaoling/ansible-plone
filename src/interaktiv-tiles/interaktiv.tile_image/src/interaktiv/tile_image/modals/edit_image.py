from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile


class EditImageModalView(BrowserView):
    """ View: Set Image """

    template = ViewPageTemplateFile('templates/edit_image.pt')

    def __call__(self):
        return self.template()

    def get_image_field(self):
        return self.request.form.get('field', 'image')

    def get_image(self):
        image_ref = getattr(self.context, 'image', None)
        if not image_ref:
            return None
        return image_ref.to_object
