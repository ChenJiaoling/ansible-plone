# -*- coding: UTF-8 -*-
from interaktiv.framework.vocabulary import IFVocabulary


def link_types(context):
    return IFVocabulary.create_vocabulary(
        [
            {'id': 'no_link', 'title': 'Keine Verlinkung'},
            {'id': 'action_link', 'title': 'Aktion'},
            {'id': 'internal_link', 'title': 'Webseite'},
            {'id': 'external_link', 'title': 'URL'}
        ]
    )
