# -*- coding: UTF-8 -*-
# noinspection PyProtectedMember
from interaktiv.basetiles import _
from interaktiv.basetiles.contenttypes.basetile import BaseTile
from plone.app.vocabularies.catalog import CatalogSource
from z3c.relationfield.schema import RelationChoice
from zope import schema
from zope.interface import Interface
from zope.interface import implements
from interaktiv.tile_image.config import TILE_CONFIG


class IImageCT(Interface):
    """ Interface for ImageCT """

    title = schema.TextLine(
        title=_(
            u'label_title',
            default=u'Titel'
        ),
        description=u"",
        required=True
    )
    image = RelationChoice(
        title=_(
            u'label_image',
            default=u'Bild'
        ),
        source=CatalogSource(portal_type=['Image']),
        required=False
    )
    image_title = schema.TextLine(
        title=_(
            u'label_image_title',
            default=u'Bild Titel'
        ),
        description=u"",
        required=False
    )
    image_alt_tag = schema.TextLine(
        title=_(
            u'label_image_alt_tag',
            default=u'Bild Alt'
        ),
        description=u"",
        required=False
    )
    image_external_link = schema.TextLine(
        title=_(
            u'label_image_external_link',
            default=u'Externer Link'
        ),
        description=u"",
        required=False
    )
    image_add_in_new_window = schema.Bool(
        title=_(
            u'label_image_add_in_new_window',
            default=u'Im neuen Fenster öffnen'
        ),
        description=u"",
        required=False
    )
    image_internal_link = RelationChoice(
        title=_(
            u'label_image_internal_link',
            default=u'Interner Link'
        ),
        source=CatalogSource(),
        required=False
    )
    image_title_from_reference = schema.Bool(
        title=_(
            u'label_title_from_reference',
            default=u'Titel vom Link'
        ),
        description=u"",
        required=False
    )
    image_link_type = schema.Choice(
        title=_(
            u'label_image_link_type',
            default=u'Link Typ'
        ),
        description=u"",
        source="link_types_vocabulary",
        required=True
    )


class ImageCT(BaseTile):
    """ Add Interface to ImageCT Container """
    implements(IImageCT)

    # noinspection PyShadowingBuiltins
    def __init__(self, id=None, **kwargs):
        self.tile_config = self.merge_tile_config(TILE_CONFIG)
        self.is_valid = True
        BaseTile.__init__(self, id, **kwargs)

    def get_available_tile_actions(self):
        actions = super(ImageCT, self).get_available_tile_actions()
        url = self.absolute_url()
        actions = self.append_tile_action(actions=actions, action={
            'action_id': 'edit-image',
            'data_modal': "/edit-image-modal",
            'data_modal_size': 'md',
            'data_modal_params': '{'
                                 ' "compomentUrl":"' + url + '",'
                                 ' "tagClass": "edit-modal",'
                                 ' "id": "edit-image"'
                                 '}',
            'data_modal_init': 'initializeEditImage',
            'attr_title': 'label_edit_image',
            'attr_class': 'edit-image tool'
                          ' interaktiv-icon-basetilestheme-option-row',
        })

        return actions

    @property
    def selectable_types(self):
        return ['Image']

    def validate_tile(self):
        if not self.image or self.image.isBroken():
            self.is_valid = False
            return False
        self.is_valid = True
        return True
