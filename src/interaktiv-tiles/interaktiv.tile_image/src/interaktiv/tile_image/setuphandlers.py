# -*- coding: utf-8 -*-
from interaktiv.basetiles.base_functions import tile_installer


def setup_after(context):
    """Miscellanous steps import handle
    """

    tile_installer({
        'id': 'interaktiv_tile_image',
        'label': 'Bild',
        'portaltype': 'ImageCT',
        'active': True,
        'image': '++theme++interaktiv.tile_image/tile-icon.jpg'
    })

