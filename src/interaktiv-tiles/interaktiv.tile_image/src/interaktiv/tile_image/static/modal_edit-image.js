function initializeEditImage(modal) {
  // Click ok button

  modal.$modal.on('click', '.btn-ok', function (e) {
    e.preventDefault();
    e.stopPropagation();

    var component_url = modal.params.compomentUrl;

    modal.submitForm(component_url + '/update_content', 'json', function (success, response) {
      if (success) {
        refreshTiles(response.uid, response.render);
        modal.close();
      }
    });
  });
}