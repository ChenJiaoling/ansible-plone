from plone.app.testing import TEST_USER_ID, setRoles
from plone.dexterity.utils import createContentInContainer

from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue

import transaction

import unittest2 as unittest

from interaktiv.tile_image.testing import \
    INTERAKTIV_TILE_IMAGE_FUNCTIONAL_TESTING


class TestImageCTMethods(unittest.TestCase):

    layer = INTERAKTIV_TILE_IMAGE_FUNCTIONAL_TESTING
    app = None
    portal = None
    request = None

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        createContentInContainer(
            self.portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )
        # noinspection PyAttributeOutsideInit
        self.tile_row_a = createContentInContainer(
            self.portal.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )

    def test_get_image(self):
        createContentInContainer(
            self.portal,
            'Image',
            id='image_a',
        )
        image_tile = createContentInContainer(
            self.tile_row_a,
            'ImageCT',
            id='image_tile_a',
        )
        intids = getUtility(IIntIds)
        image_tile.image = RelationValue(
            intids.getId(self.portal.image_a)
        )

        image_data = image_tile.get_image()
        self.assertEqual(image_data['id'], 'image_a')

    def test_get_image_link(self):
        image_tile = createContentInContainer(
            self.tile_row_a,
            'ImageCT',
            id='image_tile_a',
        )
        image_tile.image_external_link = 'http://interaktiv.de'
        image_tile.image_link_type = 'external_link'
        link_data = image_tile.get_image_link()
        self.assertEqual(link_data['url'], 'http://interaktiv.de')
        self.assertEqual(link_data['external'], True)
        #
        image_tile.image_external_link = 'https://interaktiv.de'
        link_data = image_tile.get_image_link()
        self.assertEqual(link_data['url'], 'https://interaktiv.de')
        #
        image_tile.image_external_link = 'interaktiv.de'
        link_data = image_tile.get_image_link()
        self.assertEqual(link_data['url'], 'http://interaktiv.de')

    def tearDown(self):
        if 'tiles_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['tiles_container_a'])
        transaction.commit()


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestImageCTMethods))
    return suite
