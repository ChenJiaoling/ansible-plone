from plone import api
from plone.app.testing import TEST_USER_ID, setRoles
from plone.dexterity.utils import createContentInContainer

import transaction

from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from z3c.relationfield import RelationValue
from zc.relation.interfaces import ICatalog

from zope.event import notify
from zope.lifecycleevent import ObjectModifiedEvent

import unittest2 as unittest

from interaktiv.tile_image.testing import \
    INTERAKTIV_TILE_IMAGE_FUNCTIONAL_TESTING


class TestRemoveImage(unittest.TestCase):

    layer = INTERAKTIV_TILE_IMAGE_FUNCTIONAL_TESTING
    app = None
    portal = None
    request = None

    @staticmethod
    def update_object(obj):
        obj.reindexObject()
        notify(ObjectModifiedEvent(obj))

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        createContentInContainer(
            self.portal,
            'MediaContainerCT',
            id='media_container_a',
        )
        self.portal.media_container_a.reindexObject()
        createContentInContainer(
            self.portal.media_container_a,
            'Image',
            id='image_a',
        )
        self.portal.media_container_a.image_a.reindexObject()
        createContentInContainer(
            self.portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )
        # noinspection PyAttributeOutsideInit
        self.tile_row_a = createContentInContainer(
            self.portal.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        # noinspection PyAttributeOutsideInit
        self.image_tile_a = createContentInContainer(
            self.portal.tiles_container_a.tile_row_a,
            'ImageCT',
            id='image_tile_a',
        )
        transaction.commit()

    def test_remove_imagect_removes_reference(self):
        intids = getUtility(IIntIds)
        image_intid = intids.getId(
            self.portal.media_container_a.image_a
        )
        self.image_tile_a.image = RelationValue(image_intid)
        self.update_object(self.image_tile_a)
        transaction.commit()

        view = api.content.get_view(
            name='remove_content',
            context=self.image_tile_a,
            request=self.request,
        )
        view()
        self.assertEqual(len(self.tile_row_a.objectIds()), 0)
        # check brackrefs
        intid_catalog = getUtility(ICatalog)
        results = [
            res for res in intid_catalog.findRelations({'to_id': image_intid})
        ]
        self.assertEqual(len(results), 0)

    def tearDown(self):
        if 'media_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['media_container_a'])
        if 'tiles_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['tiles_container_a'])
        transaction.commit()


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestRemoveImage))
    return suite
