import transaction
import unittest2 as unittest
from interaktiv.tile_image.testing import \
    INTERAKTIV_TILE_IMAGE_FUNCTIONAL_TESTING
from plone import api
from plone.app.testing import TEST_USER_ID, setRoles
from plone.dexterity.utils import createContentInContainer
from plone.testing.z2 import Browser

from zope.event import notify
from zope.lifecycleevent import ObjectModifiedEvent


class TestApiUpdate(unittest.TestCase):

    layer = INTERAKTIV_TILE_IMAGE_FUNCTIONAL_TESTING
    app = None
    portal = None
    request = None
    browser = None

    @staticmethod
    def update_object(obj):
        obj.reindexObject()
        notify(ObjectModifiedEvent(obj))

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']

        self.browser = Browser(self.app)
        self.browser.handleErrors = False

        setRoles(self.portal, TEST_USER_ID, ['Manager'])

        createContentInContainer(
            self.portal,
            'MediaContainerCT',
            id='media_container_a',
        )
        self.portal.media_container_a.reindexObject()
        createContentInContainer(
            self.portal.media_container_a,
            'Image',
            id='image_a',
        )
        self.portal.media_container_a.image_a.reindexObject()
        createContentInContainer(
            self.portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )
        createContentInContainer(
            self.portal.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        # noinspection PyAttributeOutsideInit
        self.image_tile_a = createContentInContainer(
            self.portal.tiles_container_a.tile_row_a,
            'ImageCT',
            id='image_tile_a',
        )
        transaction.commit()

    def tearDown(self):
        if 'tiles_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['tiles_container_a'])
        if 'media_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['media_container_a'])
        transaction.commit()

    def test_save_image(self):
        self.request.form['title'] = 'New Image Title'
        view = api.content.get_view(
            name='update_content',
            context=self.portal.media_container_a.image_a,
            request=self.request,
        )
        view()
        self.assertEqual(
            self.portal.media_container_a.image_a.title, 'New Image Title'
        )

    def test_set_image(self):
        image_uid = self.portal.media_container_a.image_a.UID()
        self.request.form['image'] = image_uid
        view = api.content.get_view(
            name='update_content',
            context=self.image_tile_a,
            request=self.request,
        )
        view()

        image_ref = self.image_tile_a.image
        image = image_ref.to_object
        self.assertEqual(image.id, 'image_a')

    def test_set_image_external_link(self):
        self.request.form['image_external_link'] = 'http://interaktiv.de'
        view = api.content.get_view(
            name='update_content',
            context=self.image_tile_a,
            request=self.request,
        )
        view()
        self.assertEqual(
            self.image_tile_a.image_external_link,
            'http://interaktiv.de'
        )


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestApiUpdate))
    return suite
