from plone import api
from plone.app.testing import TEST_USER_ID, setRoles
from plone.dexterity.utils import createContentInContainer

import json
import transaction

import unittest2 as unittest

from interaktiv.tile_image.testing import \
    INTERAKTIV_TILE_IMAGE_FUNCTIONAL_TESTING


class TestApiAdd(unittest.TestCase):

    layer = INTERAKTIV_TILE_IMAGE_FUNCTIONAL_TESTING
    app = None
    portal = None
    request = None

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        # noinspection PyAttributeOutsideInit
        self.tiles_container_a = createContentInContainer(
            self.portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )
        # noinspection PyAttributeOutsideInit
        self.image = createContentInContainer(
            self.portal,
            'Image',
            id='image_a',
        )
        transaction.commit()

    def test_add_image_tile_to_tile_row(self):
        tile_row_a = createContentInContainer(
            self.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        self.request.form['portal_type'] = 'ImageCT'
        self.request.form['position'] = '0'
        # set fields in request
        ref_uid = self.tiles_container_a.UID()
        self.request.form['fields'] = json.dumps({
            'image': self.image.UID(),
            'image_title': 'Image Title',
            'image_alt_tag': 'Image Alt Tag',
            'image_external_link': 'Image External Link',
            'image_add_in_new_window': True,
            'image_internal_link': ref_uid,
            'image_title_from_reference': False,
            'image_link_type': 'internal_link'
        })
        view = api.content.get_view(
            name='add_tile',
            context=tile_row_a,
            request=self.request,
        )
        result = json.loads(view())
        self.assertEqual(len(self.portal.tiles_container_a.objectIds()), 1)
        tile_row = self.portal.tiles_container_a.objectValues()[0]
        self.assertEqual(result['url'], tile_row.absolute_url())
        tile = tile_row.objectValues()[0]
        self.assertEqual(tile.portal_type, 'ImageCT')
        # check fields
        referenced_image = tile.image.to_object
        self.assertEqual(referenced_image.UID(), self.image.UID())
        self.assertEqual(tile.image_title, 'Image Title')
        self.assertEqual(tile.image_alt_tag, 'Image Alt Tag')
        self.assertEqual(tile.image_external_link, 'Image External Link')
        self.assertEqual(tile.image_add_in_new_window, True)
        internal_referenced_obj = tile.image_internal_link.to_object
        self.assertEqual(internal_referenced_obj.UID(), ref_uid)
        self.assertEqual(tile.image_title_from_reference, False)
        self.assertEqual(tile.image_link_type, 'internal_link')

    def test_add_image_tile_to_tiles_container(self):
        self.request.form['portal_type'] = 'ImageCT'
        self.request.form['position'] = '0'
        # set fields in request
        ref_uid = self.tiles_container_a.UID()
        self.request.form['fields'] = json.dumps({
            'image': self.image.UID(),
            'image_title': 'Image Title',
            'image_alt_tag': 'Image Alt Tag',
            'image_external_link': 'Image External Link',
            'image_add_in_new_window': True,
            'image_internal_link': ref_uid,
            'image_title_from_reference': False,
            'image_link_type': 'internal_link'
        })
        view = api.content.get_view(
            name='add_tile',
            context=self.portal.tiles_container_a,
            request=self.request,
        )
        result = json.loads(view())
        self.assertEqual(len(self.portal.tiles_container_a.objectIds()), 1)
        tile_row = self.portal.tiles_container_a.objectValues()[0]
        self.assertEqual(result['url'], tile_row.absolute_url())
        tile = tile_row.objectValues()[0]
        self.assertEqual(tile.portal_type, 'ImageCT')
        # check fields
        referenced_image = tile.image.to_object
        self.assertEqual(referenced_image.UID(), self.image.UID())
        self.assertEqual(tile.image_title, 'Image Title')
        self.assertEqual(tile.image_alt_tag, 'Image Alt Tag')
        self.assertEqual(tile.image_external_link, 'Image External Link')
        self.assertEqual(tile.image_add_in_new_window, True)
        internal_referenced_obj = tile.image_internal_link.to_object
        self.assertEqual(internal_referenced_obj.UID(), ref_uid)
        self.assertEqual(tile.image_title_from_reference, False)
        self.assertEqual(tile.image_link_type, 'internal_link')

    def test_add_image_tile_to_tiles_container_no_position(self):
        self.request.form['portal_type'] = 'ImageCT'
        view = api.content.get_view(
            name='add_tile',
            context=self.portal.tiles_container_a,
            request=self.request,
        )
        view()
        self.assertEqual(len(self.portal.tiles_container_a.objectIds()), 1)
        tile_row = self.portal.tiles_container_a.objectValues()[0]
        tile = tile_row.objectValues()[0]
        self.assertEqual(tile.portal_type, 'ImageCT')

    def test_add_image_tile_to_tile_row_no_position(self):
        tile_row_a = createContentInContainer(
            self.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        createContentInContainer(
            tile_row_a,
            'ImageCT',
            id='image_tile_a',
        )
        self.request.form['portal_type'] = 'ImageCT'
        view = api.content.get_view(
            name='add_tile',
            context=tile_row_a,
            request=self.request,
        )
        view()
        self.assertEqual(len(tile_row_a.objectIds()), 2)
        self.assertEqual(tile_row_a.objectValues()[0].id, 'image_tile_a')

    def test_add_image_tile_to_tile_row_with_position(self):
        tile_row_a = createContentInContainer(
            self.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        createContentInContainer(
            tile_row_a,
            'ImageCT',
            id='image_tile_a',
        )
        self.request.form['portal_type'] = 'ImageCT'
        self.request.form['position'] = '0'
        view = api.content.get_view(
            name='add_tile',
            context=tile_row_a,
            request=self.request,
        )
        view()
        self.assertEqual(len(tile_row_a.objectIds()), 2)
        self.assertEqual(tile_row_a.objectValues()[1].id, 'image_tile_a')

    def test_add_image_tile_to_tile_container_with_position(self):
        createContentInContainer(
            self.tiles_container_a,
            'TileRowCT',
            id='tile_row_a',
        )
        self.request.form['portal_type'] = 'ImageCT'
        self.request.form['position'] = '0'
        view = api.content.get_view(
            name='add_tile',
            context=self.portal.tiles_container_a,
            request=self.request,
        )
        view()
        self.assertEqual(len(self.portal.tiles_container_a.objectIds()), 2)
        self.assertEqual(
            self.portal.tiles_container_a.objectValues()[1].id, 'tile_row_a'
        )

    def tearDown(self):
        if 'tiles_container_a' in self.portal.objectIds():
            self.portal.manage_delObjects(['tiles_container_a'])
        transaction.commit()


def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestApiAdd))
    return suite
