# -*- coding: utf-8 -*-
from AccessControl import getSecurityManager
from AccessControl.SecurityManagement import newSecurityManager
from AccessControl.SecurityManagement import setSecurityManager
from AccessControl.User import UnrestrictedUser as BaseUnrestrictedUser


class UnrestrictedUser(BaseUnrestrictedUser):
    """Unrestricted user that still has an id.
    """
    def getId(self):
        """Return the ID of the user.
        """
        return self.getUserName()


def execute_as_manager(portal, function, *args, **kwargs):
    """ http://docs.plone.org/develop/plone/security/permissions.html
    """
    role = 'Manager'
    sm = getSecurityManager()

    try:
        tmp_user = UnrestrictedUser(
            sm.getUser().getId(), '', [role], ''
        )
        tmp_user = tmp_user.__of__(portal.acl_users)
        newSecurityManager(None, tmp_user)
        # Call the function
        return function(*args, **kwargs)

    finally:
        setSecurityManager(sm)
