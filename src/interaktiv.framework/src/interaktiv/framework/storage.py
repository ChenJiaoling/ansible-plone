from zope.event import notify
from zope.lifecycleevent import ObjectModifiedEvent


class IFStorage:
    """ Interaktiv Framework Storage:
        Storage base class for all Interaktiv Framework classes
    """

    def __init__(self):
        pass

    @classmethod
    def set_field(cls, obj, fieldname, value):
        setattr(obj, fieldname, value)
        cls.update_object(obj)

    @classmethod
    def get_field(cls, obj, fieldname):
        return getattr(obj, fieldname, None)

    @classmethod
    def update_object(cls, obj):
        obj.reindexObject()
        notify(ObjectModifiedEvent(obj))
