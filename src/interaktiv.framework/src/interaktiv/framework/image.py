# -*- coding: utf-8 -*-
import sys
import time
# from PyQt4 import QtCore
# from PyQt4.QtCore import *
# from PyQt4.QtGui import *
# from PyQt4.QtWebKit import *


class IFImage():
    """ Interaktiv Framework Helper:
        Contains all image methods
    """
    screenshot = None

    @classmethod
    def get_screenshot_as_base64(cls, url):
        """ Create a Screenshot with QWebView and get as base64
        @param string - url:
        @return string: base64 string
        """
        if not cls.screenshot:
            cls.screenshot = Screenshot()
        return cls.screenshot.capture_as_base64(url)


class Screenshot(object):  # QWebView

    _loaded = False

    def __init__(self):
        self.app = QApplication(sys.argv)
        QWebView.__init__(self)
        self._loaded = False
        self.loadFinished.connect(self._loadFinished)

    def capture_as_base64(self, url):
        self.load(QUrl(url))
        self.wait_load()
        # set to webpage size
        frame = self.page().mainFrame()
        self.page().setViewportSize(frame.contentsSize())
        # render image
        image = QImage(self.page().viewportSize(), QImage.Format_ARGB32)
        painter = QPainter(image)
        frame.render(painter)
        painter.end()
        # get as base64
        ba = QtCore.QByteArray()
        buffer = QtCore.QBuffer(ba)
        buffer.open(QtCore.QIODevice.WriteOnly)
        image.save(buffer, 'PNG')
        return ba.toBase64().data()

    def wait_load(self, delay=0):
        # process app events until page loaded
        while not self._loaded:
            self.app.processEvents()
            time.sleep(delay)
        self._loaded = False

    def _loadFinished(self, result):
        self._loaded = True
