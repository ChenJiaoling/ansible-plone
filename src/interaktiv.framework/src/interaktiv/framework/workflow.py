from plone import api


class IFWorkflow:
    """ Interaktiv Framework Workflow:
        Contains all workflow related methods
    """

    def __init__(self):
        pass

    @staticmethod
    def get_review_state(obj):
        """ Get the review state of an object
            with the plone.api
        @param object - obj:
        @return string: review_state
        """
        try:
            return str(api.content.get_state(obj))
        except Exception as e:
            print e
            return str()

    @staticmethod
    def set_review_state(obj, transition):
        """ set the objects review_state with the given transition
            with the plone.api
        @param object - obj:
        @param string - transition:
        @return bool: set review state worked?
        """
        return bool(api.content.transition(obj=obj, transition=transition))

    @staticmethod
    def is_review_state(obj, state):
        """ Check if it is the given review_state
        @param object - obj:
        @param string - state:
        @return bool: is review state
        """
        if IFWorkflow.get_review_state(obj) == state:
            return True
        return False
