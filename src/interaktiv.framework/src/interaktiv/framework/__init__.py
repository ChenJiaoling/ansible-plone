import logging


logger = logging.getLogger('interaktiv.tiles')


def initialize(context):
    """Initializer called when used as a Zope 2 product."""
