# -*- coding: UTF-8 -*-
from zope.schema.vocabulary import SimpleVocabulary
from zope.schema.vocabulary import SimpleTerm


class IFVocabulary:
    """ Interaktiv Framework Vocabulary:
        Contains all vocabulary related methods
    """

    def __init__(self):
        pass

    @staticmethod
    def create_vocabulary(options_dict):
        terms = []
        for option in options_dict:
            terms.append(
                SimpleTerm(option['id'], option['id'], option['title'])
            )
        return SimpleVocabulary(terms)
