from plone.app.testing import PloneSandboxLayer
from plone.app.testing import applyProfile
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting

from plone.testing import z2

from zope.configuration import xmlconfig


class InteraktivframeworkLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configuration_context):
        # Load ZCML
        import interaktiv.framework
        xmlconfig.file(
            'configure.zcml',
            interaktiv.framework,
            context=configuration_context
        )

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'interaktiv.framework:default')

INTERAKTIV_FRAMEWORK_FIXTURE = InteraktivframeworkLayer()
INTERAKTIV_FRAMEWORK_INTEGRATION_TESTING = IntegrationTesting(
    bases=(INTERAKTIV_FRAMEWORK_FIXTURE,),
    name="InteraktivframeworkLayer:Integration"
)
INTERAKTIV_FRAMEWORK_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(INTERAKTIV_FRAMEWORK_FIXTURE, z2.ZSERVER_FIXTURE),
    name="InteraktivframeworkLayer:Functional"
)
