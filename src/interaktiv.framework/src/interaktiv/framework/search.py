# -*- coding: utf-8 -*-
from plone import api

# We should accept both a simple space, unicode u'\u0020 but also a
# multi-space, so called 'waji-kankaku', unicode u'\u3000'
MULTISPACE = u'\u3000'.encode('utf-8')
BAD_CHARS = ('?', '-', '+', '*', MULTISPACE)


class IFSearch:
    """ Interaktiv Framework Search:
        Contains all portal_catalog related methods for searching content.
    """

    def __init__(self):
        pass

    @classmethod
    def plone_search(cls, request):
        """ Wrapper for plone native search view, for example results() returns
        properly wrapped search results from the catalog. Everything in Plone
        that performs searches should go through this.

        If your are looking for more interessting methods and how plone handle
        things like substring based search, respecting types_not_searched
        setting or the expiration date have a look at plone's native search in
        Products/CMFPlone/browser/search.py.
        """
        portal = api.portal.get()
        return api.content.get_view(
            name='search',
            context=portal,
            request=request,
        )

