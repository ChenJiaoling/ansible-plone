# -*- coding: utf-8 -*-
from zope.component import getUtility
from zope.interface import Interface
from z3c.relationfield import RelationValue
from zope.intid.interfaces import IIntIds
from plone.app.uuid.utils import uuidToObject
from Products.CMFCore.utils import getToolByName
from Products.CMFCore.WorkflowCore import WorkflowException
import json
from random import choice

from plone.dexterity.interfaces import IDexterityFTI
from plone.dexterity.utils import getAdditionalSchemata
from zope.component import queryMultiAdapter
from zope.globalrequest import getRequest
from z3c.form.interfaces import IFieldWidget
from z3c.form.interfaces import IDataManager
from z3c.form.interfaces import IDataConverter

from zope.schema.interfaces import IChoice
from zope.schema.interfaces import IDate
from zope.schema.interfaces import IFromUnicode
from zope.schema.interfaces import IList
from zope.schema.interfaces import IBool
from z3c.relationfield.interfaces import IRelationList
from z3c.relationfield.interfaces import IRelationChoice
from collective.z3cform.datagridfield import IRow
from plone.namedfile.file import NamedBlobImage
from plone.namedfile.interfaces import INamedBlobImageField
from datetime import datetime

from interaktiv.framework import logger


class IApiUtility(Interface):
    """ utility to provide methods
    """


def get_api_utility():
    return getUtility(IApiUtility)


class ApiUtility(object):
    """ utility to provide methods
    """

    def set_fields(self, obj, fields):
        if not fields:
            return
        if not isinstance(fields, dict):
            fields = json.loads(fields)

        # get schema fields
        fti = getUtility(IDexterityFTI, name=obj.portal_type)
        schema = fti.lookupSchema()
        schema_fields = {}
        for name in schema:
            schema_fields[name] = schema[name]
        for schema in getAdditionalSchemata(portal_type=obj.portal_type):
            for name in schema:
                schema_fields[name] = schema[name]

        #
        for key in fields:
            if not hasattr(obj, key):
                continue
            value = fields[key]
            self._set_field(obj, schema_fields[key], value)

    def _set_field(self, obj, field, value, return_value=False):
        widget = queryMultiAdapter((field, getRequest()), IFieldWidget)
        converter = IDataConverter(widget)
        dm = queryMultiAdapter((obj, field), IDataManager)
        intids = getUtility(IIntIds)
        field_name = field.getName()

        if isinstance(value, dict):
            if 'value' in value and 'required' in value:
                value = value.get('value', value)

        if widget:
            if not IFromUnicode.providedBy(field):
                value = value
            elif isinstance(value, unicode):
                value = value
            elif isinstance(value, dict):
                value = value
            elif isinstance(value, list):
                value = value
            elif isinstance(value, basestring):
                value = unicode(str(value), 'utf-8', errors='ignore')

        if not dm:
            return

        # noinspection PyArgumentList
        if IDate.providedBy(field):
            # JSON Format: 2016-04-14T12:08:31.754Z
            date_spl = value.split('T')[0].split('-')
            date = datetime(
                int(date_spl[0]), int(date_spl[1]), 1
            )
            if return_value:
                return date
            else:
                setattr(obj, field_name, date)
        elif IRelationList.providedBy(field):
            # Go through every given reference and set it
            # with intid as RelationValue
            references = list()
            if isinstance(value, basestring):
                ref_obj = uuidToObject(value)
                if ref_obj:
                    ref_intid = intids.getId(ref_obj)
                    references.append(
                        RelationValue(ref_intid)
                    )
            elif isinstance(value, list):
                for ref in value:
                    ref_obj = None
                    if isinstance(ref, dict):
                        path = ref.get('path', ref.get('url', ''))
                        if path:
                            try:
                                ref_obj = self.portal.unrestrictedTraverse(path)
                            except AttributeError as e:
                                print '[AttributeError]', e
                        else:
                            ref_obj = uuidToObject(ref.get('uid', ''))
                    elif isinstance(ref, basestring):
                        ref_obj = uuidToObject(ref)

                    if ref_obj:
                        ref_intid = intids.getId(ref_obj)
                        references.append(
                            RelationValue(ref_intid)
                        )
            if return_value:
                return references
            else:
                setattr(obj, field_name, references)
        elif IRelationChoice.providedBy(field):
            reference = None
            related_obj = uuidToObject(value)
            if related_obj:
                intids = getUtility(IIntIds)
                reference = RelationValue(intids.getId(related_obj))
            setattr(obj, field_name, reference)
        elif INamedBlobImageField.providedBy(field):
            image = NamedBlobImage(
                data=value.read(),
                filename=unicode(
                    getattr(value, 'filename', str()),
                    'utf-8'
                )
            )
            if return_value:
                return image
            else:
                setattr(obj, field_name, image)
        elif IList.providedBy(field):
            new_value = list()
            value_type = getattr(field, 'value_type', None)
            if value_type:
                for row in value:
                    new_value.append(
                        self._set_field(obj, value_type, row, True)
                    )
            if return_value:
                return new_value
            else:
                setattr(obj, field_name, new_value)
        elif IRow.providedBy(field):
            new_value = dict()
            schema = getattr(field, 'schema', None)
            if not schema:
                raise Exception('No schema provided to field', field_name)
            fields = schema.namesAndDescriptions()
            for field_key, field in fields:
                va = value[field_key] if field_key in value else None
                new_value[field_key] = self._set_field(obj, field, va, True)
            if return_value:
                return new_value
            else:
                setattr(obj, field_name, new_value)
        elif IChoice.providedBy(field):
            if value:
                dm.set(value)
        elif IBool.providedBy(field):
            if isinstance(value, basestring):
                value = value.lower() == 'true'
            dm.set(value)
        else:
            # noinspection PyArgumentList
            new_value = converter.toFieldValue(value)

            if return_value:
                return new_value
            else:
                dm.set(new_value)

    def publish_object(self, obj):
        wftool = getToolByName(obj, 'portal_workflow')
        try:
            wftool.doActionFor(obj, 'publish')
        except WorkflowException:
            logger.info("Could not publish: %s already published?" % obj.id)
            pass

    def generate_id(self):
        letters = 'abcd1234'
        return "".join([choice(letters) for i in range(12)])
