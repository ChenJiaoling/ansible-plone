# coding=utf-8
from plone.app.uuid.utils import uuidToObject
from z3c.relationfield import RelationValue
from zope.component import getUtility
from zope.intid.interfaces import IIntIds
from interaktiv.framework.storage import IFStorage
from zc.relation.interfaces import ICatalog
from Acquisition import aq_inner


class IFReference(IFStorage):
    """ Interaktiv Framework Reference:
        Contains all Dexterity's reference / relation field related methods.

        Related docs:
            http://docs.plone.org/external/plone.app.dexterity/docs/advanced/references.html
            https://pypi.python.org/pypi/z3c.relationfield
    """

    def __init__(self):
        IFStorage.__init__(self)

    @classmethod
    def _create_relation(cls, to_uuid):
        """ Create a relation to the passed UUID of a referenced object.
        @param string - to_uuid:
        @return RelationValue - relation:
        """
        # Create a relation to passed uuid
        intids = getUtility(IIntIds)
        ref_obj = uuidToObject(to_uuid)
        if not ref_obj:
            # Could not find object
            raise RuntimeError(u"Could not look-up UUID:", to_uuid)
        return RelationValue(intids.getId(ref_obj))

    @classmethod
    def set_relation(cls, obj, fieldname, to_uuid):
        """ Set a relation to the passed UUID of a referenced object.
        @param object - obj:
        @param string - fieldname:
        @param string - to_uuid:
        @return RelationValue
        """
        relation = None

        if to_uuid:
            # get relation by uuid
            relation = cls._create_relation(to_uuid)

        # Set newly created relation to passed object
        cls.set_field(obj, fieldname, relation)

        return cls.get_field(obj, fieldname)

    @classmethod
    def set_relation_list(cls, obj, fieldname, to_uuids):
        """ Set a relation list to the passed UUID of a referenced object.
        @param object - obj:
        @param string - fieldname:
        @param list - to_uuids:
        @return RelationValue
        """
        relations = list()
        for to_uuid in to_uuids:
            relations.append(cls._create_relation(to_uuid))
        setattr(obj, fieldname, relations)
        return relations

    @classmethod
    def append_relation_list(cls, obj, fieldname, to_uuids):
        """ Append to a relation list
            to the passed UUID of a referenced object.
        @param object - obj:
        @param string - fieldname:
        @param list - to_uuids:
        @return RelationValue
        """
        # get relations by uuid
        relations = getattr(obj, fieldname, list())
        if not isinstance(relations, list):
            relations = list()

        # ToDo: Refactor to work with set_relation_list()
        for to_uuid in to_uuids:
            relations.append(cls._create_relation(to_uuid))
        setattr(obj, fieldname, relations)
        return relations

    @classmethod
    def get_relation_choice_obj(cls, obj, fieldname):
        """ get relations of a relation list field
        @param object - obj:
        @param string - fieldname:
        @return list - relation_object:
        """
        relation = cls.get_field(obj, fieldname)
        if relation is None:
            return None
        if hasattr(relation, 'to_object'):
            relation_object = relation.to_object
        else:
            relation_object = relation
        if relation_object:
            return relation_object
        return None

    @classmethod
    def get_relation_list_objs(cls, obj, fieldname):
        """ get relations of a relation list field
        @param object - obj:
        @param string - fieldname:
        @return list - relation_objects:
        """
        relation_objects = list()
        relations = cls.get_field(obj, fieldname)

        # fallback
        if not relations:
            return list()

        for relation in relations:
            if hasattr(relation, 'to_object'):
                relation_object = relation.to_object
            else:
                relation_object = relation
            if relation_object:
                relation_objects.append(relation_object)
            # ToDo: Add exception handling, fallbacks ...
        return relation_objects

    @classmethod
    def get_relating_objs_for(cls, target_obj, fieldname=''):
        """ get related objects of an obj
        with the target obj "to_id". Get objs
        in relation catalag by getting the "from_id" obj.
        @param object - target_obj:
        @param string - fieldname:
        @return list - related_objs:
        """
        # get relations by obj "to_id"
        intids = getUtility(IIntIds)
        intid_catalog = getUtility(ICatalog)
        target_obj_intid = intids.getId(aq_inner(target_obj))
        relations = intid_catalog.findRelations(
            {'to_id': int(target_obj_intid)}
        )

        # collect related objects by relation "from_id"s
        related_objs = list()
        for relation in relations:
            if fieldname:
                if not relation.from_attribute == fieldname:
                    continue
            related_objs.append(intids.queryObject(relation.from_id))
        return related_objs

    @classmethod
    def remove_from_relation_list(cls, source_obj, fieldname, to_uids):
        """ removefrom relation list
        @param object - source_obj:
        @param string - fieldname:
        @param list - to_uids:
        @return list - related_objs:
        """
        new_relations = []

        relations = getattr(source_obj, fieldname, [])

        # fallback
        if not relations:
            setattr(source_obj, fieldname, list())
            return list()

        for relation in relations:
            relation_object = relation.to_object
            if relation_object:
                if relation_object.UID() in to_uids:
                    continue
            new_relations.append(relation)

        setattr(source_obj, fieldname, new_relations)
        return new_relations
