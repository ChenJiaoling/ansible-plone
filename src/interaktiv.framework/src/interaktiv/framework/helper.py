# -*- coding: utf-8 -*-
import HTMLParser
from distutils.util import strtobool
from os import urandom

from Products.CMFPlone.utils import safe_unicode
from plone import api
from plone.i18n.normalizer.interfaces import IURLNormalizer
from plone.uuid.interfaces import IUUIDGenerator
from zope.component import getUtility
from interaktiv.framework import logger
import json


class IFHelper:
    """ Interaktiv Framework Helper:
        Contains all helper methods
    """

    def __init__(self):
        pass

    @classmethod
    def format_plone_date(cls, date_object):
        """ format the date by given datetime obj
        @param date_object: Given Date
        :return str - date: formated date
        """
        # Get start and end dates
        date = str()
        date += str(date_object.day) + "."
        date += str(date_object.month) + "."
        date += str(date_object.year)
        return date

    @classmethod
    def find_dict_item_in_list(cls, lst, key, value):
        """ Find the index of a dict within a list, by matching the dict's
        value.

        Usage example:
            Find the index position [0],[1], or [2] by matching on name = 'Tom'?

            list = [{'id':'1234','name':'Jason'},
                    {'id':'2345','name':'Tom'},
                    {'id':'3456','name':'Art'}]
            index = find_dict_item_in_list(list, 'name', 'Tom')
        """
        for i, dic in enumerate(lst):
            if dic[key] == value:
                return i
        raise ValueError

    @classmethod
    def str2bool(cls, some_string):
        """
        Convert a string representation of truth to true or false.

        True values are 'y', 'yes', 't', 'true', 'on' and '1';
        False values are None, '', 'n', 'no', 'f','false', 'off' and '0'.
        Raises ValueError if val is anything else.
        @param some_string:
        @return:
        """
        return bool(strtobool(some_string)) if some_string else False

    @classmethod
    def chunk_list(cls, chunk_list, size):
        """ Yield successive n-sized chunks from l.
            Example: list = [1, 2, 3, 4, 5, 6, 7] | size = 3
            Result: [[1, 2, 3], [4, 5, 6], [7]]

            @param list - chunk_list: list to split
            @param int - size: split list into n-sized chunks
        """
        for i in xrange(0, len(chunk_list), size):
            yield chunk_list[i:i + size]

    @classmethod
    def generate_password(cls, length=8):
        chars = "AaBbCcDdEeFfGgHhiJjKkLMmNnPpQqRrSsTtUuVvWwXxYyZz23456789"
        return "".join(chars[ord(c) % len(chars)] for c in urandom(length))

    @classmethod
    def get_default_page(cls, container):
        container_default_page_id = container.getDefaultPage()
        if container_default_page_id in container:
            return container[container_default_page_id]
        return None

    @classmethod
    def get_portal_url(cls):
        return api.portal.get().absolute_url()

    @classmethod
    def get_normalized_id(cls, title, parent=None, obj_id='', used_ids=None):
        # Todo remove html stuff -> lxml ?
        # make unicode
        if used_ids is None:
            used_ids = []
        if not isinstance(title, unicode):
            title = safe_unicode(title)

        # remove &nbsp;
        title = title.replace(u'\xc2\xa0', u' ')
        # strip uneccesary spaces
        title = title.strip()
        #
        new_id = getUtility(IURLNormalizer).normalize(title, locale='de')
        if parent:
            container_ids = parent.objectIds()
            # check additional_ids
            if used_ids:
                container_ids.extend(used_ids)
            #
            while new_id in container_ids:
                if new_id == obj_id:
                    break
                new_id_parts = new_id.rsplit('-', 1)
                if len(new_id_parts) > 1:
                    if new_id_parts[-1].isdigit():
                        new_id_parts[-1] = str(int(new_id_parts[-1]) + 1)
                    else:
                        new_id_parts.append('1')
                else:
                    new_id_parts.append('1')
                new_id = '-'.join(new_id_parts)
        return new_id

    @classmethod
    def generate_uuid(cls):
        generator = getUtility(IUUIDGenerator)
        return generator()

    @classmethod
    def unescape_title(cls, title):
        if not title:
            return ''
        html_parser = HTMLParser.HTMLParser()
        return html_parser.unescape(title)

    @classmethod
    def clean_title(cls, title, do_unicode=False):
        if not title:
            return ''
        if do_unicode:
            title = unicode(cls.unescape_title(title))
        else:
            title = cls.unescape_title(title)

        # remove &nbsp;
        title = title.replace(u'\xc2\xa0', u' ')
        title = title.replace(u'\xa0', u' ')

        # remove spaces with no width
        title = title.replace(u'\xef\xbb\xbf', u'')
        title = title.replace(u'\ufeff', u'')

        # remove linefeeds
        title = title.replace(u'\n', u'')
        title = title.replace(u'\r', u'')

        return title

    @classmethod
    def load_json_from_string(cls, json_string):
        if isinstance(json_string, basestring):
            try:
                return json.loads(json_string)
            except TypeError:
                logger.info('Could not load json from string:' + json_string)
                return list()
            except ValueError:
                return list()
        return list()

    @classmethod
    def parse_deep_form(cls, form):
        data = dict()
        for form_key in form:
            val = form[form_key]

            if '[' in form_key:
                key = form_key.replace('[', '').replace(']', '')

                if key not in data:
                    if isinstance(val, list):
                        data[key] = val
                    else:
                        data[key] = [val]
                else:
                    data[key].append(val)
            else:
                data[form_key] = form[form_key]
        return data
