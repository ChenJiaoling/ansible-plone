# -*- coding: UTF-8 -*-
from zope.viewlet.interfaces import IViewletManager
from zope.component import queryMultiAdapter
from Products.Five.browser import BrowserView as View


class IFTest:
    """ Interaktiv Framework Test:
        Contains all test related methods
    """

    def __init__(self):
        pass

    @staticmethod
    def check_is_workflow(test_obj, obj, state):
        return test_obj.assertEqual(
            test_obj.workflow_tool.getInfoFor(obj, 'review_state'),
            state
        )

    @staticmethod
    def get_viewlet(test_obj, obj, request, viewlet_name):
        view = View(obj, request)
        manager = queryMultiAdapter(
            (obj, request, view),
            IViewletManager,
            'plone.portalheader',
            default=None
        )
        test_obj.failUnless(manager)
        manager.update()
        viewlet = [
            v for v in manager.viewlets
            if v.__name__ == viewlet_name
        ][0]
        if not viewlet:
            return None
        return viewlet
