setuptools
Products.CMFPlone
plone.app.caching
plone.app.contenttypes
interaktiv.basetiles

[test]
plone.app.testing
plone.app.robotframework[debug]
lxml
requests
