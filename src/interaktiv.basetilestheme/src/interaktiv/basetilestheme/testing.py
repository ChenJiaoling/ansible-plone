from plone.app.testing import PloneSandboxLayer
from plone.app.testing import applyProfile
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting
from plone.testing import z2

from zope.configuration import xmlconfig


class InteraktivBasetileThemeLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load ZCML
        import interaktiv.basetilestheme
        xmlconfig.file(
            'configure.zcml',
            interaktiv.basetilestheme,
            context=configurationContext
        )

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'interaktiv.basetilestheme:default')

INTERAKTIV_BASETILESTHEME_FIXTURE = InteraktivBasetileThemeLayer()
INTERAKTIV_BASETILESTHEME_INTEGRATION_TESTING = IntegrationTesting(
    bases=(INTERAKTIV_BASETILESTHEME_FIXTURE,),
    name="InteraktivBasetileThemeLayer:Integration"
)
INTERAKTIV_BASETILESTHEME_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(INTERAKTIV_BASETILESTHEME_FIXTURE, z2.ZSERVER_FIXTURE),
    name="InteraktivBasetileThemeLayer:Functional"
)
