# ============================================================================
# Feature: Robot Test
# ============================================================================
#
#
# bin/robot-server interaktiv.basetilespolicy.testing.INTERAKTIV_BASETILESPOLICY_ROBOT_TESTING
# bin/robot src/interaktiv.basetilespolicy/src/interaktiv/basetilespolicy/tests/acceptance/test_robot.robot
#
# xvfb-run --server-args="-screen 0 1920x1200x24" ./bin/robot src/interaktiv.basetilespolicy/src/interaktiv/basetilespolicy/tests/acceptance/test_robot.robot
#
# ============================================================================

*** Variables ***

${FF_PROFILE_DIR}  ${CURDIR}/profile_1600_960



*** Settings ***

Variables  interaktiv/basetilespolicy/testing.py

Resource  plone/app/robotframework/selenium.robot
Resource  plone/app/robotframework/keywords.robot
Resource  keywords.robot

Library  Remote  ${PLONE_URL}/RobotRemote
#Library  Remote  ${PLONE_URL}/InteraktivTilesRobotRemote

Test Setup  Open test browser
Test Teardown  Run keywords  Close all browsers


*** Test Cases ***

Scenario: Open the portal
  Given a logged in Manager
  When I open the portal


*** Keywords ***

Test Teardown
  a logged in Manager
  Run keywords
  Close all browsers
  Delete test tiles


# Given

a logged in Manager
  Enable autologin as  Manager

# When

I open the portal
  Go to  http://localhost:55001/plone
