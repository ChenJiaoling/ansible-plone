from interaktiv.basetilespolicy.testing import INTERAKTIV_BASETILESPOLICY_ROBOT_TESTING
from plone.testing import layered
import robotsuite
import unittest
import os


def test_suite():
    suite = unittest.TestSuite()
    current_dir = os.path.abspath(os.path.dirname(__file__))
    acceptance_dir = os.path.join(current_dir, 'acceptance')
    # ROBOT tests
    acceptance_tests = [
        os.path.join('acceptance', doc) for doc in
        os.listdir(acceptance_dir) if doc.endswith('.robot') and
        doc.startswith('test_')
    ]
    for acceptance_test in acceptance_tests:
        robottest = robotsuite.RobotTestSuite(acceptance_test)
        robottest.level = 2
        suite.addTests([
            layered(
                robottest,
                layer=INTERAKTIV_BASETILESPOLICY_ROBOT_TESTING,
            ),
        ])
    return suite
