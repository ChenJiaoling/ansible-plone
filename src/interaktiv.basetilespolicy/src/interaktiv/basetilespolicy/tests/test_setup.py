# -*- coding: utf-8 -*-
import unittest2 as unittest
from Products.CMFCore.utils import getToolByName
from interaktiv.basetilespolicy.testing import \
    INTERAKTIV_BASETILESPOLICY_INTEGRATION_TESTING


class TestSetup(unittest.TestCase):

    layer = INTERAKTIV_BASETILESPOLICY_INTEGRATION_TESTING

    def setUp(self):
        self.app = self.layer['app']
        self.portal = self.layer['portal']
        self.qi_tool = getToolByName(self.portal, 'portal_quickinstaller')

    def _pid_is_installed(self, pid):
        """ Validate that the GS profile has been run and the product installed
        """
        installed = [p['id'] for p in self.qi_tool.listInstalledProducts()]
        self.assertTrue(
            pid in installed,
            'package %s appears not to have been installed' % pid
        )

    def test_interaktiv_basetilespolicy_is_installed(self):
        self._pid_is_installed('interaktiv.basetilespolicy')

    def test_dependencies_are_installed(self):
        dependencies = [
            'interaktiv.framework',
            'interaktiv.basetiles',
            'interaktiv.tile_image',
            'interaktiv.tile_subheadline',
            'interaktiv.tile_text',
        ]
        for dependency in dependencies:
            self._pid_is_installed(dependency)
