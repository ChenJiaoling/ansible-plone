# -*- coding: utf-8 -*-
from plone.dexterity.utils import createContentInContainer
from zope.site.hooks import getSite
from plone import api


def create_content(parent_obj, obj_id, title, portal_type):
    # create tile and set title
    obj = createContentInContainer(
        parent_obj,
        portal_type,
        id=obj_id,
        title=title
    )
    obj.reindexObject()
    return obj


def set_default_page(container, obj):
    container.setDefaultPage(obj.id)


def importVarious(context):
    """ Miscellanous steps import handle """
    portal = getSite()

    delete_plone_content = ['front-page', 'news', 'events', 'Members']
    for plone_id in delete_plone_content:
        if plone_id in portal:
            api.content.delete(obj=portal[plone_id])

    if 'startseite' not in portal:
        obj = create_content(
            portal,
            'startseite',
            'Startseite',
            'TilesContainerCT'
        )
        set_default_page(portal, obj)
