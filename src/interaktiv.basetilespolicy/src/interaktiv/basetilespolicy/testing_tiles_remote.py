# -*- coding: utf-8 -*-
from plone.app.robotframework.remote import RemoteLibrary
from plone.app.robotframework.utils import disableCSRFProtection
from plone.dexterity.utils import createContentInContainer
# from plone.app.textfield.value import RichTextValue
# from plone.namedfile.file import NamedBlobImage
import transaction
from zope.site.hooks import getSite


class TilesContent(RemoteLibrary):

    def create_test_tiles_container(self):
        disableCSRFProtection()
        portal = getSite()
        createContentInContainer(
            portal,
            'TilesContainerCT',
            id='tiles_container_a',
        )
        portal.tiles_container_a.title = 'Tiles Container A'
        portal.tiles_container_a.reindexObject()
        transaction.commit()
