import os

from Products.CMFCore.utils import getToolByName
from plone.app.robotframework.autologin import AutoLogin
from plone.app.robotframework.content import Content
from plone.app.robotframework.genericsetup import GenericSetup
from plone.app.robotframework.i18n import I18N
from plone.app.robotframework.mailhost import MockMailHost
from plone.app.robotframework.quickinstaller import QuickInstaller
from plone.app.robotframework.remote import RemoteLibraryLayer
from plone.app.robotframework.server import Zope2ServerRemote
from plone.app.robotframework.testing import REMOTE_LIBRARY_ROBOT_TESTING
from plone.app.robotframework.users import Users
from plone.app.testing import FunctionalTesting
from plone.app.testing import IntegrationTesting
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import PloneSandboxLayer
from plone.app.testing import TEST_USER_ID
from plone.app.testing import applyProfile
from plone.app.testing import setRoles
from interaktiv.basetilespolicy.testing_tiles_remote import TilesContent
from zope.configuration import xmlconfig
import importlib

USER_NAME = 'john_doe'
USER_ID = 'john_doe'
USER_EMAIL = 'john@doe.test'
USER_PASSWORD = 'test'
USER_ROLES = ('Site Administrator',)

BUILDOUT_DIR = os.environ.get('BUILDOUT_DIR', '')
INSTANCE_HOME = os.environ.get('INSTANCE_HOME', '') + '/..'
DATA_FOLDER = '/src/interaktiv-tiles/interaktiv.basetilespolicy\
               src/interaktiv/basetilespolicy/tests/data'

PATH_TO_TEST_FILES = BUILDOUT_DIR + DATA_FOLDER if BUILDOUT_DIR\
    else INSTANCE_HOME + DATA_FOLDER


class INTERAKTIVbasetilespolicyLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)
    products_to_import = [
        'plone.app.contenttypes',
        'interaktiv.framework',
        'interaktiv.basetiles',
        'interaktiv.tile_image',
        'interaktiv.tile_subheadline',
        'interaktiv.tile_text',
        'interaktiv.basetilespolicy',
    ]

    def setUpZope(self, app, configurationContext):
        for product_name in self.products_to_import:
            module = importlib.import_module(product_name)
            xmlconfig.file(
                'configure.zcml',
                module,
                context=configurationContext
            )

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'plone.app.contenttypes:default')
        applyProfile(portal, 'interaktiv.basetilespolicy:default')
        setRoles(portal, TEST_USER_ID, ['Manager'])

        # Add member
        mtool = getToolByName(portal, "portal_membership")
        mtool.addMember(
            USER_NAME,
            USER_PASSWORD,
            USER_ROLES,
            [],
            properties={
                'username': USER_ID,
                'fullname': USER_NAME,
                'email': USER_EMAIL,
            }
        )


INTERAKTIV_BASETILESPOLICY_FIXTURE = INTERAKTIVbasetilespolicyLayer()

INTERAKTIV_BASETILESPOLICY_INTEGRATION_TESTING = IntegrationTesting(
    bases=(INTERAKTIV_BASETILESPOLICY_FIXTURE,),
    name="INTERAKTIVbasetilespolicyLayer:Integration"
)

INTERAKTIV_BASETILESPOLICY_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(INTERAKTIV_BASETILESPOLICY_FIXTURE,),
    name="INTERAKTIVbasetilespolicyLayer:Functional"
)

#########################################################################

INTERAKTIV_BASETILESPOLICY_ROBOT_TILES_REMOTE_BUNDLE = RemoteLibraryLayer(
    libraries=(
        AutoLogin, QuickInstaller, GenericSetup,
        Content, Users, I18N, MockMailHost, Zope2ServerRemote,
        TilesContent,
    ),
    name="INTERAKTIVbasetilespolicyLayer:InteraktivTilesRobotRemote"
)

INTERAKTIV_BASETILESPOLICY_ROBOT_TESTING = FunctionalTesting(
    bases=(
        INTERAKTIV_BASETILESPOLICY_FIXTURE,
        REMOTE_LIBRARY_ROBOT_TESTING,
        INTERAKTIV_BASETILESPOLICY_ROBOT_TILES_REMOTE_BUNDLE,
    ),
    name="INTERAKTIVbasetilespolicyLayer:Robot"
)
