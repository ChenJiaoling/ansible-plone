setuptools
Products.CMFPlone
plone.app.theming
plone.app.portlets
plone.app.caching
plone.behavior
plone.app.contenttypes
z3c.jbot

[test]
plone.app.testing
plone.app.robotframework[debug]
lxml
requests
