# -*- coding: utf-8 -*-
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.CMFCore.utils import getToolByName
from plone import api
from zope.site.hooks import getSite
from plone.app.layout.viewlets.common import ViewletBase


class NavigationViewlet(ViewletBase):
    """ Viewlet: Navigation """

    index = ViewPageTemplateFile('templates/navigation.pt')
    max_level = 3

    def __call__(self):
        self.current_url = self.context.absolute_url()

    @staticmethod
    def get_portal_url():
        return api.portal.get().absolute_url()

    def get_navigation(self):
        return self.get_navigation(
            level1_portal_type=['Folder'],
            portal_types=['Document']
        )

    def get_navigation(self, level1_portal_type=None,
                       portal_types=None):
        """ Get Navigation with brains filtered by query
        :return list - sections:
        """
        if portal_types is None:
            portal_types = ["Document"]
        if level1_portal_type is None:
            level1_portal_type = ['Folder']
        nav_root_obj = getSite()

        path = "/".join(nav_root_obj.getPhysicalPath())
        query = {
            "portal_type": level1_portal_type,
            "path": {'query': path, 'depth': 1},
            "sort_on": "getObjPositionInParent",
            "getExcludeFromNav": False,
            "is_default_page": False
        }

        sections = self._get_sections(
            query=query,
            portal_types=portal_types
        )
        return sections

    def _get_sections(self, query, portal_types, current_level=1):
        """ Get Sections recursively
        :param dict - query:
        :param int - current_level:
        :return list - sections:
        """
        sections = list()
        catalog = getToolByName(self.context, "portal_catalog")

        # Customize Query
        if current_level > 1:
            query["portal_type"] = portal_types
        brains = catalog(query)
        for brain in brains:

            if brain.exclude_from_nav:
                continue

            # Get Subsections
            if current_level < self.max_level:
                query['path'] = {
                    "query": brain.getPath(),
                    "depth": 1
                }
                subsections = self._get_sections(
                    query=query,
                    current_level=current_level + 1,
                    portal_types=portal_types
                )
            else:
                subsections = list()

            obj = brain.getObject()

            if getattr(obj, 'is_workingcopy', False):
                continue

            sections.append({
                "title": brain.Title,
                "short_title": self._get_short_title(obj),
                "id": brain.getId,
                "url": brain.getURL(),
                "css_class": self._get_css_class(
                    brain.getPath(), current_level
                ),
                "obj": obj,
                "subsections": subsections
            })

        return sections

    @staticmethod
    def _get_short_title(obj):
        """ Return object attribute "short_title"
        :param object - obj:
        :return string - (short title):
        """
        if hasattr(obj, "short_title"):
            return obj.short_title
        return str()

    def _get_css_class(self, section_path, current_level):
        """ Evaluate correct css class and return it
        :param: string - section_path
        :param: int - current_level
        :return: string - css_class
        """
        css_class = str()
        # Add Section Level
        css_class += " section-" + str(current_level)
        if '/site' in section_path:
            section_path = section_path.replace('/site', '')

        # Add flag for subsections
        if section_path in self.context.absolute_url():
            css_class += " open-subsections"
            # Add "selected" for active section
            if section_path == self.context.absolute_url():
                css_class += " selected"
        return css_class