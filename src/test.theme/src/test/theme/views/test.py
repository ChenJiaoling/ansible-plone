from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile


class TestView(BrowserView):
    """ View: Extend Base Tile View with Tile """

    template = ViewPageTemplateFile("templates/test.pt")

    def __call__(self):
        """ Call Login View Template
        :return: ViewPageTemplateFile - (template)
        """
        return self.template()
