# -*- coding: UTF-8 -*-
from plone.app.portlets.interfaces import IColumn
from zope.interface import Interface


class ITestThemeLayer(Interface):
    """Marker interface that defines a ZTK browser layer. We can reference
    this in the 'layer' attribute of ZCML <browser:* /> directives to ensure
    the relevant registration only takes effect when this theme is installed.

    The browser layer is installed via the browserlayer.xml GenericSetup
    import step.
    """
